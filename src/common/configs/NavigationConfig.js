import {
  HomeOutlined,
  SnippetsOutlined,
  DashboardOutlined
} from '@ant-design/icons';
import {APP_PREFIX_PATH} from './AppConfig'

const dashBoardNavTree = [{
  key: 'home',
  path: `${APP_PREFIX_PATH}`,
  title: 'sidenav.home',
  icon: HomeOutlined,
  breadcrumb: false,
  submenu: []
}]

const appsNavTree = [{
  key: 'terms-of-use',
  path: `${APP_PREFIX_PATH}/terms-of-use`,
  title: 'sidenav.term',
  icon: SnippetsOutlined,
  breadcrumb: false,
  submenu: []
}]

const navigationConfig = [
  ...dashBoardNavTree,
  ...appsNavTree,
]

export default navigationConfig;
