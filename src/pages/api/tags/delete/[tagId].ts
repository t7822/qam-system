import {auth, connectDB, validate, errorHandler} from "server/middleware";
import {tagController} from "server/controllers";
import middlewares from "server";
import {runMiddleware} from "server/utils/middleware-handler";
import {deleteTag} from "server/validations/tag.validation";
import {withIronSessionApiRoute} from "iron-session/next";
import {ironOptions} from "server/config";
import {NextApiHandler, NextApiRequest, NextApiResponse} from "next";

const handler: NextApiHandler = async (req: NextApiRequest, res: NextApiResponse) => {
  await middlewares(req, res);
  await runMiddleware(req, res, auth(req, res, 'MANAGE_ALL_TAG'));
  await runMiddleware(req, res, validate(req, res, deleteTag));

  if (req.method === "POST") {
    try {
      await tagController.deleteTag(req, res);
    } catch (err) {
      errorHandler(err, req, res);
    }
  }
};

export default connectDB(withIronSessionApiRoute(handler, ironOptions));
