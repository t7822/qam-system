import {sendEmailFromMailGun} from "server/utils/email-server";
import {MailData} from "server/interfaces/types";
import {userService} from "server/services";

export const sendNotificationEmail = async (content: string, filter: any) => {
  let users = await userService.getListUsers(filter);
  console.log(users)
  await sendEmail({
    sendTo: "",
    subject: "[IDDE] Notification Email",
    contentHTML: content
  }, users);
}

export const sendEmail = async (mailData: MailData, users: any[]) => {
  if (!mailData.sendTo) {
    for (const user of users) {
      mailData.sendTo = user.email
      await sendEmailFromMailGun(mailData);
    }
  } else await sendEmailFromMailGun(mailData);
  return "A request was sent to your email";
}
