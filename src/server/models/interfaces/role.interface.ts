import {Document, Model} from 'mongoose';

export interface RoleInterface extends Document {
  name: string,
  slug: string,
  permissions: Array<string>,
  deleted: boolean,
}

export interface IRole extends RoleInterface {
  // for methods
}

export interface RoleModel extends Model<IRole> {
  // for static functions
  paginate(filter: object, options: object): any
}