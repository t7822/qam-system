import {attachmentService} from 'server/services';
import {ExtendedRequest, ExtendedResponse} from 'server/interfaces/types';
import pick from "server/utils/pick";

export const addAttachments = async (req: ExtendedRequest, res: ExtendedResponse) => {
  Object.assign(req.body, { uploader: req.session.user?._id });
  const result = await attachmentService.addAttachments(req.body, req.files);
  res.json(result);
}

export const removeAttachment = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const hasAuth = req.session.user?.role.permissions.includes('MANAGE_ALL_ATTACHMENT') || req.session.user?.role.permissions.includes('DELETE_ALL_ATTACHMENT');
  const result = await attachmentService.removeAttachment(req.body, req.session.user?._id, hasAuth);
  res.json(result);
}

export const removeAllAttachments = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const hasAuth = req.session.user?.role.permissions.includes('MANAGE_ALL_ATTACHMENT') || req.session.user?.role.permissions.includes('DELETE_ALL_ATTACHMENT');
  const result = await attachmentService.removeAllAttachments(req.body, req.session.user?._id, hasAuth);
  res.json(result);
}