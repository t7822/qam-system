import httpStatus from 'http-status';
import {Idea, IdeaTag, Tag, Topic, UserIdea, View, Attachment, Department} from 'server/models';
import {topicService} from "server/services";
import ApiError from 'server/utils/api-error';
import {IIdea} from "server/models/interfaces/idea.interface";
import {Schema, Types, PipelineStage} from "mongoose";
import {IUser} from "server/models/interfaces/user.interface";
import {genRandomUser} from "server/utils/randomString";
import {dateFormat} from "server/utils/datetime";
import csvWriter from "csv-write-stream";
import {compressAttachments, deleteAttachments} from "./attachment.service";

export const createIdea = async (ideaBody: any, attachments: Array<any>, department: string): Promise<IIdea> => {
  console.log("Test", ideaBody)
  const topic = await topicService.getTopicByFilter({_id: ideaBody.topic});
  const userDepartment = await Department.findOne({_id: department});
  if (userDepartment._id.toString() !== topic.department._id.toString()) {
    throw new ApiError(httpStatus.BAD_REQUEST, "You cannot submit new idea to other department");
  }
  if (new Date(topic.firstClosureDate).getTime() < new Date().getTime()) {
    deleteAttachments(attachments);
    throw new ApiError(httpStatus.BAD_REQUEST, "You cannot submit new idea for this topic once reaching the first closure date");
  }
  ideaBody.createdAt = Date.now();
  let idea = await Idea.create(ideaBody);

  let tags = ideaBody.tags ? (typeof ideaBody.tags === "string" ? [ideaBody.tags] : ideaBody.tags) : null;

  if (tags) {
    let exec = tags.map(t => {
      console.log('1', t);
      return {
        insertOne: {
          document: {
            idea: idea._id,
            tag: t
          }
        }
      }
    });
    await IdeaTag.bulkWrite(exec);
  }
  if (attachments && attachments.length > 0) {
    let exec = attachments.map(a => {
      return {
        insertOne: {
          document: {
            idea: idea._id,
            uploader: ideaBody.author,
            fileName: a.fileName,
            originalName: a.originalName,
            filePath: a.filePath,
            fileSize: a.size,
            mimeType: a.mimeType,
          }
        }
      }
    });
    console.log(exec)
    await Attachment.bulkWrite(exec);
  }
  await idea.populate([
    {path: "author", model: "User", select: "username displayName avatar"},
    {path: "tags", model: "IdeaTag", select: "tag", populate: {path: "tag", model: "Tag", select: "title slug"}},
    {path: "topic", model: "Topic", select: "-author -id"},
  ]);
  Object.assign(idea, {tags: idea.tags.map(data => data.tag)});
  return idea;
};

export const getIdeasByFilter = async (filter: any, options: any, userId?: Schema.Types.ObjectId, hasPermission: boolean = false): Promise<any> => {
  const objectIdFields = ["author"];
  const excludeFields = ["_id", "deleted", "topic", "createdAt"];
  for (let field in filter) {
    if (objectIdFields.includes(field) || (filter[field]._id && typeof filter[field]._id !== "object")) filter[field] = new Types.ObjectId(filter[field]._id);
    else if (excludeFields.includes(field)) continue;
    else filter[field] = {$regex: filter[field], $options: 'g'}
  }
  const pipeline: Array<PipelineStage> = [
    {
      $match: {
        ...filter
      }
    },
    {
      $lookup: {
        from: 'users',
        localField: 'author',
        foreignField: '_id',
        as: 'author',
      }
    },
    {
      $unwind: "$author"
    },
    {
      $lookup: {
        from: 'ideatags',
        localField: '_id',
        foreignField: 'idea',
        as: 'ideatags',
      }
    },
    {
      $unwind: {
        path: "$ideatags",
        preserveNullAndEmptyArrays: true
      }
    },
    {
      $lookup: {
        from: 'topics',
        localField: 'topic',
        foreignField: '_id',
        as: 'topic',
      }
    },
    {
      $unwind: {
        path: "$topic",
      }
    },
    {
      $lookup: {
        from: 'departments',
        localField: 'topic.department',
        foreignField: '_id',
        as: 'department',
      }
    },
    {
      $unwind: {
        path: "$department",
        preserveNullAndEmptyArrays: true
      }
    },
    {
      $lookup: {
        from: 'tags',
        localField: 'ideatags.tag',
        foreignField: '_id',
        as: 'tags',
      }
    },
    {
      $unwind: {
        path: "$tags",
        preserveNullAndEmptyArrays: true
      }
    },
    {
      $lookup: {
        from: 'attachments',
        localField: '_id',
        foreignField: 'idea',
        as: 'attachments',
      }
    },
    {
      $unwind: {
        path: "$attachments",
        preserveNullAndEmptyArrays: true
      }
    },
    {
      $lookup: {
        from: 'comments',
        localField: '_id',
        foreignField: 'category',
        as: 'comments',
      }
    },
    {
      $lookup: {
        from: 'reactions',
        let: {ideaId: "$_id"},
        pipeline: [
          {
            $match: {
              $expr: {
                $and: [
                  {$eq: ["$target", "$$ideaId"]},
                  {
                    $eq: ["$active", true],
                  },
                ],
              },
            }
          }
        ],
        as: 'reactions',
      }
    },
    {
      $lookup: {
        from: 'votes',
        let: {ideaId: "$_id"},
        pipeline: [
          {
            $match: {
              $expr: {
                $and: [
                  {$eq: ["$idea", "$$ideaId"]},
                  {
                    $eq: ["$active", true],
                  },
                ],
              },
            }
          }
        ],
        as: 'votes',
      }
    },
    {
      $lookup: {
        from: 'views',
        localField: '_id',
        foreignField: 'idea',
        as: 'views',
      }
    },
    {
      $project: {
        "author._id": 1,
        "author.username": 1,
        "author.fullName": 1,
        "author.displayName": 1,
        "author.avatar": 1,
        "title": 1,
        "slug": 1,
        "privacy": 1,
        "content": 1,
        "state": 1,
        "reactions": 1,
        "votes": 1,
        "createdAt": 1,
        "editedAt": 1,
        "editedTimes": 1,
        "comments": 1,
        "tags._id": 1,
        "tags.title": 1,
        "tags.slug": 1,
        "topic._id": 1,
        "topic.firstClosureDate": 1,
        "topic.finalClosureDate": 1,
        "topic.title": 1,
        "topic.slug": 1,
        "department._id": 1,
        "department.name": 1,
        "department.slug": 1,
        "attachments._id": 1,
        "attachments.fileName": 1,
        "attachments.originalName": 1,
        "attachments.fileSize": 1,
        "attachments.mimeType": 1,
        "reactionCount": {$size: "$reactions"},
        "voteCount": {$size: "$votes"},
        "commentCount": {$size: "$comments"},
        "viewCount": {$size: "$views"},
      }
    },
    {
      $group: {
        _id: "$_id",
        title: {$first: "$title"},
        content: {$first: "$content"},
        author: {$first: "$author"},
        slug: {$first: "$slug"},
        privacy: {$first: "$privacy"},
        topic: {$first: "$topic"},
        department: {$first: "$department"},
        viewCount: {$first: "$viewCount"},
        voteCount: {$first: "$voteCount"},
        createdAt: {$first: "$createdAt"},
        reactionCount: {$first: "$reactionCount"},
        commentCount: {$first: "$commentCount"},
        reactions: {$first: "$reactions"},
        votes: {$first: "$votes"},
        tags: {
          $addToSet: "$tags"
        },
        attachments: {
          $addToSet: "$attachments"
        }
      }
    },
    {
      $sort: options.sort || {reactions: -1}
    },
    {
      $skip: options.skip || 0
    },
  ];
  if (options.limit) pipeline.push({
    $limit: options.limit
  });
  let ideas = await Idea.aggregate(pipeline);
  for (const data of ideas) {
    data.createdAt = new Date(data.createdAt).getTime();
    let reactions = [...data.reactions];
    data.reactions = reactions.reduce((previousValue, currentValue) => {
      return previousValue[currentValue.type] ? ++previousValue[currentValue.type] : previousValue[currentValue.type] = 1, previousValue;
    }, {});
    let userReaction = userId ? reactions.find(r => r.user.toString() === userId.toString()) : null;
    data.userReaction = userReaction ? userReaction.type : false;
    let votes = [...data.votes];
    data.votes = votes.reduce((previousValue, currentValue) => {
      return previousValue[currentValue.type] ? ++previousValue[currentValue.type] : previousValue[currentValue.type] = 1, previousValue;
    }, {total: votes.length, upvote: 0, downvote: 0});
    let userVote = userId ? votes.find(r => r.user.toString() === userId.toString()) : null;
    data.userVote = userVote ? userVote.type : false;

    if (data.privacy === "anonymous" && userId?.toString() !== data.author.toString() && !hasPermission) data.author = genRandomUser();
  }
  return ideas;
};

export const queryIdeas = async (filter: any, options: any, user: IUser | undefined): Promise<any> => {
  const hasPermission = user?.role?.permissions.includes("MANAGE_ALL_IDEA");
  if (filter.tag) {
    if (await Tag.countDocuments({slug: filter.tag}) <= 0) throw new ApiError(httpStatus.NOT_FOUND, `Cannot find tag with name ${filter.tag}`);
    const tag = await Tag.findOne({slug: filter.tag});
    Object.assign(filter, {
      _id: {
        $in: (await IdeaTag.find({tag: tag?._id})).map((data: any) => data.idea)
      }
    })
    delete filter.tag
  }
  if (filter.topic) {
    const topic = await Topic.findOne({slug: filter.topic});
    if (!topic) throw new ApiError(httpStatus.NOT_FOUND, `Cannot find topic with name ${filter.topic}`);
    Object.assign(filter, {topic: topic?._id});
  }

  if (filter.saved) {
    Object.assign(filter, {
      _id: {
        $in: (await UserIdea.find({user: user?._id})).map((data: any) => data.idea)
      }
    });
    delete filter.saved
  }

  options.sort = {};
  options.limit = options.limit && parseInt(options.limit, 10) > 0 ? parseInt(options.limit, 10) : 9;
  options.page = options.page && parseInt(options.page, 10) > 0 ? parseInt(options.page, 10) : 1;
  if (options.sortBy) {
    if (options.sortBy.includes("-")) options.sort[`${options.sortBy.split("-")[1]}`] = -1;
    else options.sort[`${options.sortBy}`] = -1;
  }
  if (Object.keys(options.sort).length < 1) options.sort = null;
  Object.assign(options, {skip: (options.page - 1) * options.limit});
  return Promise.all([getIdeasByFilter(filter, options, user?._id, hasPermission), Idea.countDocuments({...filter})]).then((values) => {
    let [ideas, totalResults] = values;
    const totalPages = Math.ceil(totalResults / options.limit);
    const result = {
      results: ideas,
      page: options.page,
      limit: options.limit,
      totalPages: totalPages,
      totalResults: totalResults,
    };
    return Promise.resolve(result);
  })
};

export const getIdeaByFilter = async (filter: any, userId?: Schema.Types.ObjectId, newView?: boolean): Promise<IIdea> => {
  Object.assign(filter, {deleted: {$ne: true}});
  let idea = await getIdeasByFilter(filter, {skip: 0, limit: 1}, userId);
  if (idea.length <= 0) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Idea not found');
  }
  const result = idea[0];
  if (newView) {
    const viewDoc = {
      idea: result._id,
      viewedAt: Date.now()
    }
    if (userId) Object.assign(viewDoc, {user: userId});
    await View.create(viewDoc);
    result.viewCount++;
  }
  return result;
}

export const saveIdea = async (slug: string, userId: Schema.Types.ObjectId) => {
  const idea = await Idea.findOne({slug, deleted: {$ne: true}}).populate({path: "author", model: "User"});
  if (!idea || idea.deleted) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Idea not found');
  }
  const savedIdea = await UserIdea.findOne({idea: idea._id})
  let message = "Saved idea successfully";
  if (savedIdea) {
    await savedIdea.deleteOne({});
    message = "Unsaved idea successfully"
  } else {
    await UserIdea.create({
      idea: idea._id,
      user: userId
    });
  }
  return {idea, message};
}

export const updateIdea = async (slug: string, updateBody: any, hasPermission: boolean, userId: Schema.Types.ObjectId): Promise<any> => {
  const idea = await Idea.findOne({slug, deleted: {$ne: true}}).populate({path: "author", model: "User"});
  if (!idea) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Idea not found');
  }
  if (!hasPermission && idea.author._id.toString() !== userId.toString()) {
    throw new ApiError(httpStatus.FORBIDDEN, 'Forbidden');
  }
  if (updateBody.tags && updateBody.tags.length > 0) {
    let relations = await IdeaTag.find({idea: idea._id}).lean();
    let tags: Array<string> = relations.map((r: any) => r.tag.toString());
    let added: Array<string> = updateBody.tags.filter((t: string) => !tags.includes(t));
    let removed: Array<string> = tags.filter(t => !updateBody.tags.includes(t));
    let exec = [];
    for (let tag of added) {
      exec.push({
        insertOne: {
          document: {
            idea: idea._id,
            tag: tag
          }
        }
      })
    }
    for (let tag of removed) {
      exec.push({
        deleteOne: {
          filter: {
            idea: idea._id,
            tag: tag
          }
        }
      })
    }
    await IdeaTag.bulkWrite(exec);
  }
  Object.assign(idea, updateBody);
  Object.assign(idea, {editedAt: Date.now(), editedTimes: idea.editedTimes ? ++idea.editedTimes : 1});
  await idea.save();
  return idea;
};

export const deleteIdea = async (slug: string, hasPermission: boolean, userId: Schema.Types.ObjectId): Promise<any> => {
  const idea = await Idea.findOne({slug, deleted: {$ne: true}}).populate({path: "author", model: "User"});
  if (!idea || idea.deleted) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Idea not found');
  }

  if (!hasPermission && idea.author._id.toString() !== userId.toString()) {
    throw new ApiError(httpStatus.FORBIDDEN, 'Forbidden');
  }
  Object.assign(idea, {deleted: true});
  await idea.save();
  return idea;
}

export const restoreIdeas = async (body: any) => {
  if (!body.ideas || body.ideas.length <= 0) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Bad request');
  }
  let ideas = [...body.ideas];
  let execute = ideas.map(r => {
    return {
      updateOne: {
        filter: {
          _id: r,
        },
        update: {
          deleted: false,
        }
      }
    }
  })
  return Idea.bulkWrite(execute);
}

export const deleteIdeasPermanently = async (body: any) => {
  if (!body.idea || body.ideas.length <= 0) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Bad request');
  }
  let ideas = [...body.ideas];
  let execute = ideas.map(r => {
    return {
      deleteOne: {
        filter: {
          _id: r,
        }
      }
    }
  })
  return Idea.bulkWrite(execute);
}

export const exportCSV = async (filter: any, options: any) => {
  Object.assign(filter, {deleted: {$ne: true}});
  if (filter.tag) {
    if (await Tag.countDocuments({slug: filter.tag}) <= 0) throw new ApiError(httpStatus.NOT_FOUND, `Cannot find tag with name ${filter.tag}`);
    const tag = await Tag.findOne({slug: filter.tag});
    Object.assign(filter, {
      _id: {
        $in: (await IdeaTag.find({tag: tag?._id})).map((data: any) => data.idea)
      }
    })
    delete filter.tag
  }

  if (filter.topic) {
    const topic = await Topic.findOne({slug: filter.topic});
    if (!topic) throw new ApiError(httpStatus.NOT_FOUND, `Cannot find topic with name ${filter.topic}`);
    Object.assign(filter, {topic: topic?._id});
  }
  let ideas = await getIdeasByFilter(filter, options);
  const writer = csvWriter({
    headers: [
      "Title", "Content", "Author", "Privacy", "Topic", "Tags", "Topic First Closure", "Topic Final Closure",
      "Views", "Upvote", "Downvote", "Reactions", "Attachments", "Comments", "Created At"
    ]
  });
  for (let idea of ideas) {
    writer.write([
      idea.title, idea.content, idea.author.username, idea.privacy, idea.topic.title, idea.tags.map(t => t.title).join(', '),
      dateFormat(new Date(idea.topic.firstClosureDate), "dd-MM-yyyy"), dateFormat(new Date(idea.topic.finalClosureDate), "dd-MM-yyyy"),
      idea.viewCount, idea.upvoteCount, idea.downvoteCount, idea.reactionCount, idea.attachments.length, idea.commentCount, dateFormat(new Date(idea.createdAt), "dd-MM-yyyy")
    ])
  }
  writer.end();
  return {
    filename: dateFormat(new Date(), "dd-MM-yyyy") + "-" + Math.random() + '.csv',
    stream: writer
  };
}

export const exportZip = async (slug: string) => {
  const idea: any = await Idea.findOne({slug: slug});
  if (!idea) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Idea not found');
  }
  const attachments = await Attachment.find({idea: idea._id});
  if (!attachments || attachments.length < 1) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Attachment not found');
  }
  if (idea.isAttachmentsChanged === true || idea.isAttachmentsChanged === undefined) {
    const result = await compressAttachments(idea._id.toString(), attachments);
    await Idea.updateOne({_id: idea._id}, {
      $set: {
        isAttachmentsChanged: false
      }
    })
    return {
      filename: idea.slug + '.zip',
      filepath: result.zipPath
    }
  } else {
    return {
      filename: idea.slug + '.zip',
      filepath: `./attachments/zip/${idea._id}.zip`
    }
  }
}