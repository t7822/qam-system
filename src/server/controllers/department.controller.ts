import {departmentService} from 'server/services';
import pick from "server/utils/pick";
import {ExtendedRequest, ExtendedResponse} from "server/interfaces/types";

export const addDepartment = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const department = await departmentService.createDepartment(req.body);
  res.json({
    message: "Created department successfully",
    department: department
  });
};

export const getDepartments = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const filter = pick(req.query, ['slug', 'name', 'manager', "department"]);
  Object.assign(filter, {deleted: {$ne: true}});
  const options = pick(req.query, ['sortBy', 'limit', 'page']);
  const result = await departmentService.queryDepartments(filter, options);
  res.json(result);
};

export const getDepartment = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const result = await departmentService.getDepartmentByFilter({slug: req.query.slug});
  res.json(result);
};

export const updateDepartment = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const department = await departmentService.updateDepartment({slug: req.query.slug}, req.body);
  res.json({
    message: "Updated department successfully",
    department: department
  });
};

export const deleteDepartment = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const department = await departmentService.deleteDepartment({slug: req.query.slug});
  res.json({
    message: "Deleted department successfully",
    department: department
  });
};