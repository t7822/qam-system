import React, {useState} from "react";
import {Avatar, Button, Col, Divider, notification, Row, Upload} from "antd";
import {UserOutlined, CloudUploadOutlined} from "@ant-design/icons";
import ImgCrop from "antd-img-crop";
import AccountSettingLayout from "common/layouts/account-settings-layout";
import UserForm from "common/components/manage-components/user-form";
import Flex from "common/components/shared-components/Flex";
import axios from "axios";
import {useSelector} from "react-redux";
import {RootState} from "common/redux/store";
import {withSessionSsr} from "../../server/utils/with-session";
import {auth} from "../../server/utils/auth";
import {SessionData} from "../../server/interfaces/types";


const EditProfile = () => {
  const {user} = useSelector((state: RootState) => state.auth);
  const [loading, setLoading] = useState(false);

  return (
    <AccountSettingLayout>
      <div className="update-profile">
        <h4 className="mb-4">Edit User</h4>
        <Divider dashed className="mb-4"/>
        <Row>
          <Col xs={24} sm={24} md={24} lg={8} xxl={8} className="mb-4">
            <Flex flexDirection="column" alignItems="center" mobileFlex={false} className="text-center text-md-left">
              <Avatar
                shape={"circle"}
                size={120}
                icon={<UserOutlined/>}
                src={user.avatar}
              />
              <div className="d-flex mt-4 justify-content-center">
                <ImgCrop rotate>
                  <Upload
                    showUploadList={false}
                    beforeUpload={async (file) => {
                      console.log('file', file);
                      if (loading) return;
                      setLoading(true);
                      try {
                        const formData = new FormData();
                        formData.append('file', file.originFileObj);
                        // const res =
                        await axios.post('/api/users/update-self-info', formData, {
                          headers: {
                            'Content-Type': 'multipart/form-data',
                          }
                        })
                        notification.success({message: 'Upload avatar successfully'});
                      } catch (err) {
                        console.log(err)
                        notification.error({message: 'Failed to upload avatar'})
                      }
                      setLoading(false);
                    }}
                  >
                    <Button
                      icon={<CloudUploadOutlined/>}
                      type="primary"
                      loading={loading}
                    >
                      Change Avatar
                    </Button>
                  </Upload>
                </ImgCrop>
              </div>
            </Flex>
          </Col>
          <Col xs={24} sm={24} md={24} lg={16} xxl={16}>
            <UserForm user={user} mode="EDIT"/>
          </Col>
        </Row>
      </div>
    </AccountSettingLayout>
  )
}

export const getServerSideProps = withSessionSsr(
  async function getServerSideProps({req, res}): Promise<any> {
    try {
      await auth(req.session as SessionData, []);
    } catch (err) {
      res.statusCode = err["statusCode"] || 500;
      res.setHeader('Location', '/auth/login');
      return {
        props: {}
      }
    }
    return {
      props: {
        user: req.session.user,
      }
    }
  }
);

export default EditProfile;
