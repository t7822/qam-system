import React from "react";
import dynamic from "next/dynamic"

const Editor = dynamic(() => import('../../WrapperEditor'), { ssr: false })

// eslint-disable-next-line react/display-name
const EditorWithForwardedRef = React.forwardRef((props, ref) => (
  <Editor {...props} forwardedRef={ref} />
))


export default EditorWithForwardedRef;