import React, {useEffect, useState} from "react"
import {
  Table,
  Card,
  Button,
  Tooltip,
  Modal,
  Avatar,
  Input,
  Pagination,
  Tag,
  Drawer,
  Row,
  Col, Spin, Divider, notification
} from "antd";
import {EyeOutlined, DeleteOutlined, EditOutlined, UserOutlined} from '@ant-design/icons';
import {UserAddOutlined} from '@ant-design/icons';
import {useRouter} from "next/router";
import ManageSystem from "../index";
import {withSessionSsr} from "server/utils/with-session";
import {auth} from "server/utils/auth";
import {queryUsers} from "server/services/user.service";
import {SessionData} from "server/interfaces/types";
import {IUser} from "server/models/interfaces/user.interface";
import {ApiService} from "common/services/ApiService";

const {confirm} = Modal;

const ManageUser = (props: any) => {
  const router = useRouter();
  const [visible, setVisible] = useState(false);
  const [data, setData] = useState<IUser>(props.initUser);
  const [users, setUsers] = useState(props.users || []);
  const [value, setValue] = useState("");
  console.log('value', value);
  const [page, setPage] = useState(1);
  const [loading, setLoading] = useState(false);
  const [pageInfo, setPageInfo] = useState(props.pageInfo || {});

  useEffect(() => {
    handleChange().then((_: any) => {
    })
  }, [value, page])


  const handleChange = async () => {
    setLoading(true);
    if (value === "") {
      const res = await ApiService.getUsers({
        page: page,
        limit: 10,
        sortBy: "createdAt"
      });
      setUsers(res.results)
      setPageInfo({
        totalResults: res.totalResults,
        hasNextPage: res.page < res.totalPages
      })
      setLoading(false)
    } else {
      const res = await ApiService.getUsers({
        page: page,
        limit: 10,
        sortBy: "createdAt",
        username: value
      });
      setUsers(res.results)
      setPageInfo({
        totalResults: res.totalResults,
        hasNextPage: res.page < res.totalPages
      })
      setLoading(false)
    }
  }

  const showDeleteConfirm = (username: any) => {
    return (
      confirm({
        title: 'Are you sure delete this user?',
        content: 'This action can not undo, so do you want to delete?',
        okText: 'Yes',
        okType: 'danger',
        okButtonProps: {
          disabled: false,
        },
        cancelText: 'No',
        onOk: async () => {
          const res = await ApiService.deleteUser(username);
          if (res.status) {
            return notification.error({
              message: res.data.message
            })
          }
          const newUsers = [...users].filter(user => user._id !== res.user._id)
          setUsers(newUsers)
          notification.success({
            message: res.message
          })
        },
      })
    )
  };
  const columns: any = [
    {
      title: "Avatar",
      dataIndex: "avatar",
      align: "center",
      key: "avatar",
      render: (record: any) => (
        <Avatar src={record} icon={<UserOutlined/>}/>
      )
    }, {
      title: "Full Name",
      align: "center",
      dataIndex: "fullName",
      key: "fullName",
    }, {
      title: "Username",
      dataIndex: "username",
      key: "useName",
    }, {
      title: "Role",
      dataIndex: "role",
      align: "center",
      key: "role",
      render: (role: any) => {
        return (
          <span>{role?.name}</span>)
      }
    },{
      title: "Department",
      dataIndex: "department",
      align: "center",
      key: "department",
      render: (department: any) => {
        return (
          <Tag>{department?.name}</Tag>)
      }
    }, {
      title: "Action",
      dataIndex: "action",
      align: "center",
      key: "action",
      render: (_: any, record: IUser) => (
        <div className="text-right d-flex justify-content-center">
          <Tooltip title="View">
            <Button type="primary" className="mr-2" icon={<EyeOutlined/>} size="small" onClick={() => {
              setVisible(true)
              setData(record)
            }}/>
          </Tooltip>
          <Tooltip title="Update">
            <Button type="default" className="mr-2" icon={<EditOutlined/>} size="small" onClick={async () => {
              await router.push(`/manage-system/manage-users/edit-user/${record.username}`)
            }}/>
          </Tooltip>
          <Tooltip title="Delete">
            <Button type="default" danger icon={<DeleteOutlined/>} onClick={() => showDeleteConfirm(record.username)}
                    size="small"/>
          </Tooltip>
        </div>
      )
    }
  ];

  return (
    <ManageSystem>
      <div>
        <Row gutter={16} className={"mb-3"} style={{
          display: "flex",
          direction: "inherit",
          justifyContent: "space-between"
        }}>
          <Col lg={16} md={14} sm={8} xs={6} >
            <Button type="primary" icon={<UserAddOutlined/>} onClick={async () => {
              await router.push(`/manage-system/manage-users/add-user`)
            }}>Add User</Button>
          </Col>
          <Col lg={8} md={8} sm={10} xs={14} >
            <Input.Search style={{width: "100%"}} allowClear enterButton onSearch={(value) => {
              setValue(value);
              setPage(1);
            }}/>
          </Col>
        </Row>
        <Card>
          <div className="table-responsive">
            <Spin spinning={loading} tip="Loading....">
              <Table columns={columns} dataSource={users} rowKey={record => record.id} pagination={false}
                     footer={() => {
                       return (
                         <Pagination
                           current={page}
                           showQuickJumper
                           defaultCurrent={1}
                           total={pageInfo.totalResults}
                           onChange={(page) => setPage(page)}
                         />
                       )
                     }}/>
            </Spin>
            <div>
              <Drawer
                visible={visible}
                title="User Profile"
                onClose={() => setVisible(false)}
                width={700}
                footer={[
                  <Button key="back" onClick={() => {
                    setVisible(false)
                  }}>
                    Close
                  </Button>
                ]}
              >
                <h3>Requirement Information</h3>
                <Row gutter={24}>
                  <Col span={12} style={{marginTop: 24, marginBottom: 12}}>
                    <p>Full Name: {data?.fullName}</p>
                  </Col>
                  <Col span={12} style={{marginTop: 24, marginBottom: 12}}>
                    <p>Email: {data?.email}</p>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col span={12}>
                    <p>Role: {data?.role?.name.toUpperCase()}</p>
                  </Col>
                  <Col span={12}>
                    <p>Department: {data?.department?.name}</p>
                  </Col>
                </Row>
                <Divider/>
                <h3>Personal Information</h3>

                <Row gutter={24}>
                  <Col span={12} style={{marginTop: 24, marginBottom: 12}}>
                    <p>{`Date of Birth: ${new Date(parseInt(data?.dob)).toLocaleDateString()}`}</p>
                  </Col>
                  <Col span={12} style={{marginTop: 24, marginBottom: 12}}>
                    <p>Gender: {data?.gender === '0' ? 'Male' : data?.gender === '1' ? 'Female' : 'Unassigned'}</p>
                  </Col>
                </Row>

                <Row gutter={24}>
                  <Col span={12} style={{marginBottom: 12}}>
                    <p>Address: {data?.address}</p>
                  </Col>
                  <Col span={12} style={{marginBottom: 12}}>
                    <p>Phone: {data?.contact} </p>
                  </Col>
                </Row>
              </Drawer>
            </div>
          </div>
        </Card>
      </div>
    </ManageSystem>
  )
}

export const getServerSideProps = withSessionSsr(
  async function getServerSideProps({req, res}): Promise<any> {
    try {
      await auth(req.session as SessionData, ["MANAGE_ALL_USER"]);
      const users = await queryUsers({}, {page: 1, limit: 10, sortBy: "createdAt"});
      return {
        props: {
          initUser: {} as IUser,
          users: JSON.parse(JSON.stringify(users?.results)),
          pageInfo: {
            totalResults: users?.totalResults,
            hasNextPage: users?.page < users?.totalPages
          }
        }
      }
    } catch (err) {
      res.statusCode = err["statusCode"] || 500;
      return {
        redirect: {
          permanent: false,
          destination: (err.statusCode === 401) ? '/auth/login' : '/',
        },
      }
    }
  }
);

export default ManageUser
