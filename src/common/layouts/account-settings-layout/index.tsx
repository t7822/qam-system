import React from "react";
import {Menu} from "antd";
import {LockOutlined, UserOutlined, FacebookOutlined} from "@ant-design/icons";
import InnerAppLayout from "../inner-app-layout";
import Link from "next/link"
import {useRouter} from "next/router";

const SettingOption = () => {
  const router = useRouter();
  return (
    <Menu
      defaultSelectedKeys={[router.pathname ? '' : '/account/info']}
      mode="inline"
      selectedKeys={[router.pathname ? '' : '/account/info']}
    >
      <Menu.Item key={`/account/info`}>
        <Link href={'/account/info'}>
          <a style={{
            display: 'block',
            height: '100%'
          }}>
            <UserOutlined/>
            <span>Edit Profile</span>
          </a>
        </Link>
      </Menu.Item>
      <Menu.Item key={`/account/security`}>
        <Link href={'/account/security'}>
          <a style={{
            display: 'block',
            height: '100%'
          }}>
            <LockOutlined/>
            <span>Change Password</span>
          </a>
        </Link>
      </Menu.Item>
    </Menu>
  );
};

const AccountSettingLayout = (props: any) => {
  return (
    <InnerAppLayout
      sideContentWidth={320}
      sideContent={<SettingOption {...props}/>}
      mainContent={props.children}
    />
  )
}
export default AccountSettingLayout
