import UserForm from "common/components/user-components/user-form";
import {withSessionSsr} from "server/utils/with-session";
import {auth} from "server/utils/auth";
import {SessionData} from "server/interfaces/types";
import {queryRoles} from "server/services/role.service";
import {getUserByFilter} from "server/services/user.service";
import {queryDepartments} from "server/services/department.service";

const EditUser = (props: any) => {
  return (
    <UserForm mode="EDIT" roles={props.roles} user={props.user} departments={props.departments}/>
  )
}

export const getServerSideProps = withSessionSsr(
  async function getServerSideProps(context) : Promise<any> {
    const {req, res} = context;
    const {username} = context.query;
    try {
      await auth(req.session as SessionData, ["MANAGE_ALL_USER", "ADD_ALL_USER"]);
      const roles = await queryRoles({deleted: {$ne: true}}, {page: 1, limit: 10});
      const user = await getUserByFilter({username: username});
      const departments = await queryDepartments({deleted: {$ne: true}}, {page: 1, limit: 10, sortBy: "-createdAt"});
      return {
        props: {
          departments: JSON.parse(JSON.stringify(departments?.results)),
          roles: JSON.parse(JSON.stringify(roles?.results)),
          user: JSON.parse(JSON.stringify(user)),
        }
      }
    } catch (err) {
      res.statusCode = err["statusCode"] || 500;
      return {
        redirect: {
          permanent: false,
          destination: (err.statusCode === 401) ? '/auth/login' : '/',
        },
      }
    }
  }
)

export default EditUser
