const authorities = {
  staff: ['EXPORT_ZIP',],
  coordinator: [
    'MANAGE_ALL_COMMENT',
    'EXPORT_ZIP',
  ],
  manager: [
    'MANAGE_ALL_COMMENT',
    'GET_STATISTIC',
    'EXPORT_ZIP',
  ],
  admin: [
    'MANAGE_ALL_USER',
    'MANAGE_ALL_ROLE',
    'MANAGE_ALL_COMMENT',
    'MANAGE_ALL_IDEA',
    'MANAGE_ALL_TAG',
    'MANAGE_ALL_TOPIC',
    'MANAGE_ALL_ATTACHMENT',
    'MANAGE_ALL_DEPARTMENT',
    'EXPORT_IDEA',
    'EXPORT_ZIP',
    'GET_STATISTIC',
  ]
}

const roleConfig = Object.keys(authorities);

const AUTHORITIES = new Map(Object.entries(authorities));
const PERMISSIONS = [
  // user permission
  'MANAGE_ALL_USER',
  'GET_ALL_USER',
  'ADD_ALL_USER',
  'UPDATE_ALL_USER',
  'DELETE_ALL_USER',

  // role permission
  'MANAGE_ALL_ROLE',
  'GET_ALL_ROLE',
  'ADD_ALL_ROLE',
  'UPDATE_ALL_ROLE',
  'DELETE_ALL_ROLE',

  // comments permission
  'MANAGE_ALL_COMMENT',
  'GET_ALL_COMMENT',
  'ADD_ALL_COMMENT',
  'UPDATE_ALL_COMMENT',
  'DELETE_ALL_COMMENT',

  // tags permission
  'MANAGE_ALL_TAG',
  'GET_ALL_TAG',
  'ADD_ALL_TAG',
  'UPDATE_ALL_TAG',
  'DELETE_ALL_TAG',

  // topic permission
  'MANAGE_ALL_TOPIC',
  'GET_ALL_TOPIC',
  'ADD_ALL_TOPIC',
  'UPDATE_ALL_TOPIC',
  'DELETE_ALL_TOPIC',

  // idea permission
  'EXPORT_IDEA',
  'EXPORT_ZIP',
  'MANAGE_ALL_IDEA',
  'GET_ALL_IDEA',
  'ADD_ALL_IDEA',
  'UPDATE_ALL_IDEA',
  'DELETE_ALL_IDEA',

  'GET_STATISTIC',

  // attachment permission
  'MANAGE_ALL_ATTACHMENT',
  'DELETE_ALL_ATTACHMENT',

  // department permission
  'MANAGE_ALL_DEPARTMENT',
  'GET_ALL_DEPARTMENT',
  'ADD_ALL_DEPARTMENT',
  'UPDATE_ALL_DEPARTMENT',
  'DELETE_ALL_DEPARTMENT',
]

export default {
  ROLES: roleConfig,
  AUTHORITIES,
  PERMISSIONS,
  authorities,
}
