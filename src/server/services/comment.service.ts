import httpStatus from 'http-status';
import {Comment, Idea} from 'server/models';
import ApiError from 'server/utils/api-error';
import {Schema, Types} from "mongoose";
import {IComment} from "server/models/interfaces/comment.interface";
import {genRandomUser} from "../utils/randomString";
import {IUser} from "../models/interfaces/user.interface";

export const addComment = async (commentBody: any, authorId: Schema.Types.ObjectId): Promise<IComment> => {
  const category = await Idea.findOne({_id: commentBody.category}).populate({path: "topic", model: "Topic"});
  if (!category) throw new ApiError(httpStatus.NOT_FOUND, "Idea not found");

  if (new Date(category.topic.finalClosureDate).getTime() < new Date().getTime()) throw new ApiError(httpStatus.BAD_REQUEST, "You cannot add new comment to this idea once reaching the final closure date\"");

  Object.assign(commentBody, {author: authorId});
  const comment = await Comment.create(commentBody);
  await comment.populate([
    {path: "author", model: "User", select: "username displayName avatar fullName"},
    {path: "category", model: commentBody.model}
  ]);
  return comment;
};

export const getCommentsByFilter = async (filter: any, options: any, userId?: Schema.Types.ObjectId, hasPermission: boolean = false) => {
  if (filter._id) filter._id = new Types.ObjectId(filter._id);
  if (filter.replyFor) filter.replyFor = new Types.ObjectId(filter.replyFor);
  if (filter.category) filter.category = new Types.ObjectId(filter.category);

  let comments = await Comment.aggregate([
    {
      $match: {
        ...filter
      }
    },
    {
      $lookup: {
        from: 'users',
        localField: 'author',
        foreignField: '_id',
        as: 'author',
      }
    },
    {$unwind: "$author"},
    {
      $lookup: {
        from: 'comments',
        localField: '_id',
        foreignField: 'replyFor',
        as: 'subComments',
      }
    },
    {
      $lookup: {
        from: 'reactions',
        let: { cmtId: "$_id" },
        pipeline: [
          {
            $match: {
              $expr: {
                $and: [
                  { $eq: ["$target", "$$cmtId"] },
                  {
                    $eq: ["$active", true],
                  },
                ],
              },
            }
          }
        ],
        as: 'reactions',
      }
    },
    {
      $project: {
        "author._id": 1,
        "author.username": 1,
        "author.email": 1,
        "author.fullName": 1,
        "author.displayName": 1,
        "author.avatar": 1,
        "_id": 1,
        "content": 1,
        "privacy": 1,
        "replyFor": 1,
        "level": 1,
        "category": 1,
        "editedAt": 1,
        "createdAt": 1,
        "editedTimes": 1,
        "subCommentCount": {$size: "$subComments"},
        "reactions": 1,
        "reactionCount": {$size: "$reactions"},
      }
    },
    {
      $sort: options.sort || {createdAt: -1}
    },
    {
      $skip: options.skip || 0
    },
    {
      $limit: options.limit || 10
    },
  ]);
  for (let data of comments) {
    data.createdAt = new Date(data.createdAt).getTime();
    let reactions = [...data.reactions];
    data.reactions = reactions.reduce((previousValue, currentValue) => {
      return previousValue[currentValue.type] ? ++previousValue[currentValue.type] : previousValue[currentValue.type] = 1, previousValue;
    }, {})
    let userReaction = userId ? reactions.find(r => r.user.toString() === userId.toString()) : null;
    data.userReaction = userReaction ? userReaction.type : false;
    if (data.privacy === "anonymous" && userId?.toString() !== data.author._id.toString() && !hasPermission) data.author = genRandomUser();
  }
  return comments;
};

export const queryComments = async (filter: any, options: any, user: IUser | undefined) => {
  const hasPermission = user?.role.permissions.includes("MANAGE_ALL_COMMENT" || "MANAGE_ALL_IDEA");
  options.sort = {};
  options.limit = options.limit && parseInt(options.limit, 10) > 0 ? parseInt(options.limit, 10) : 9;
  options.page = options.page && parseInt(options.page, 10) > 0 ? parseInt(options.page, 10) : 1;
  if (options.sortBy) {
    if (options.sortBy.includes("-")) options.sort[`${options.sortBy.split("-")[1]}`] = -1;
    else options.sort[`${options.sortBy}`] = 1;
  }
  if (Object.keys(options.sort).length < 1) options.sort = null;
  Object.assign(options, {skip: (options.page - 1) * options.limit});

  return Promise.all([getCommentsByFilter(filter, options, user?._id, hasPermission), Comment.count({...filter})]).then((values) => {
    let [questions, totalResults] = values;
    const totalPages = Math.ceil(totalResults / options.limit);
    const result = {
      results: questions,
      page: options.page,
      limit: options.limit,
      totalPages: totalPages,
      totalResults: totalResults,
    };
    return Promise.resolve(result);
  })
};

export const getCommentByFilter = async (filter: any, userId?: Schema.Types.ObjectId) => {
  const comment = await getCommentsByFilter(filter, {skip: 0, limit: 1}, userId);
  if (!comment) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Comment not found');
  }
  return comment[0];
};

export const updateComment = async (commentId: Types.ObjectId, updateBody: any, hasPermission: boolean) => {
  const {author} = updateBody;
  const comment = await Comment.findOne({_id: commentId});
  if (!comment || comment.deleted) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Comment not found');
  }
  if (!hasPermission && comment.author._id.toString() !== author.toString()) {
    throw new ApiError(httpStatus.FORBIDDEN, 'Forbidden');
  }
  Object.assign(comment, updateBody);
  Object.assign(comment, {editedAt: Date.now(), editedTimes: comment.editedTimes ? ++comment.editedTimes : 1});
  await comment.save();
  await comment.populate([
    {path: "author", model: "User", select: "username displayName avatar fullName"},
    {path: "category", model: updateBody.model}
  ]);
  return comment;
};

export const deleteComment = async (deleteData: any, hasPermission: boolean) => {
  const {commentId, author} = deleteData;
  const comment = await Comment.findOne({_id: commentId});
  if (!comment) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Comment not found');
  }
  if (!hasPermission && comment.author._id.toString() !== author.toString()) {
    throw new ApiError(httpStatus.FORBIDDEN, 'Forbidden');
  }

  await comment.deleteOne();
  return comment;
}