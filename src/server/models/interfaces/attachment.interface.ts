import {Document, Model} from 'mongoose';
import {IIdea} from "./idea.interface";
import {IUser} from "./user.interface";

export interface AttachmentInterface extends Document {
  idea: IIdea["_id"],
  uploader: IUser["_id"],
  fileName: string,
  originalName: string,
  filePath: string,
  fileSize: number,
  mimeType: string,
}

// for methods
export interface IAttachment extends AttachmentInterface {
}

// for static methods
export interface AttachmentModel extends Model<IAttachment> {
  paginate(filter: object, options: object): any,
}