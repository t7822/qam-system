export const terms: any = [
  {
    "rule": 1,
    "content": "Remember the human. Q&A system is a place for creating community and belonging, not for attacking marginalized or vulnerable groups of people. Everyone has a right to use Q&A system free of harassment, bullying, and threats of violence. Communities and users that incite violence or that promote hate based on identity or vulnerability will be banned."
  }, {
    "rule": 2,
    "content": "Abide by community rules. Post authentic content into communities where you have a personal interest, and do not cheat or engage in content manipulation (including spamming, vote manipulation, ban evasion, or subscriber fraud) or otherwise interfere with or disrupt Q&A system communities."
  }, {
    "rule": 3,
    "content": "Respect the privacy of others. Instigating harassment, for example by revealing someone’s personal or confidential information, is not allowed. Never post or threaten to post intimate or sexually-explicit media of someone without their consent."
  }, {
    "rule": 4,
    "content": "Do not post or encourage the posting of sexual or suggestive content involving minors."
  }, {
    "rule": 5,
    "content": "You don’t have to use your real name to use Q&A system, but don’t impersonate an individual or an entity in a misleading or deceptive manner."
  }, {
    "rule": 6,
    "content": "Ensure people have predictable experiences on Q&A system by properly labeling content and communities, particularly content that is graphic, sexually-explicit, or offensive."
  }, {
    "rule": 7,
    "content": "Keep it legal, and avoid posting illegal content or soliciting or facilitating illegal or prohibited transactions."
  }, {
    "rule": 8,
    "content": "Don’t break the site or do anything that interferes with normal use of Q&A system."
  },
]