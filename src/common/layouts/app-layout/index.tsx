import React from 'react';
import {useSelector} from 'react-redux';
import SideNav from 'common/components/layout-components/SideNav';
import TopNav from 'common/components/layout-components/TopNav';
import Loading from 'common/components/shared-components/Loading';
import MobileNav from 'common/components/layout-components/MobileNav'
import HeaderNav from 'common/components/layout-components/HeaderNav';
import PageHeader from 'common/components/layout-components/PageHeader';
import Footer from 'common/components/layout-components/Footer';
import {ThemeSwitcherProvider} from "react-css-theme-switcher";
import {THEME_CONFIG} from "common/configs/AppConfig";

import {
  Layout,
  Grid, ConfigProvider,
} from "antd";

import navigationConfig from "common/configs/NavigationConfig";
import {
  SIDE_NAV_WIDTH,
  SIDE_NAV_COLLAPSED_WIDTH,
  NAV_TYPE_SIDE,
  NAV_TYPE_TOP,
  DIR_RTL,
  DIR_LTR
} from 'common/constants/ThemeConstant';
import utils from 'common/utils';
import {useThemeSwitcher} from "react-css-theme-switcher";
import {useRouter} from "next/router";
import {RootState} from "common/redux/store";
import {ConfigProviderProps} from "antd/lib/config-provider";
import {IntlProvider} from "react-intl";
import AppLocale from "common/lang";

const {Content} = Layout;
const {useBreakpoint} = Grid;


const themes = {
  dark: `/css/dark-theme.css`,
  light: `/css/light-theme.css`,
};


interface AppLayoutProps {
  direction?: string;
  children?: any
}

interface RouteInfo {
  breadcrumb?: any,
  title?: any
}

const AppLayoutInner = ({children}: AppLayoutProps) => {
  const {navCollapsed, navType} = useSelector((state: RootState) => state.theme);
  const {locale, direction} = useSelector((state: RootState) => state.theme);
  // @ts-ignore
  const currentAppLocale = AppLocale[locale];

  const router = useRouter();
  const currentRouteInfo = utils.getRouteInfo(navigationConfig, router.pathname) as RouteInfo;

  const screens = utils.getBreakPoint(useBreakpoint());
  const isMobile = screens.length === 0 ? false : !screens.includes('lg')
  const isNavSide = navType === NAV_TYPE_SIDE;
  const isNavTop = navType === NAV_TYPE_TOP;

  const getLayoutGutter = () => {
    if (isNavTop || isMobile) {
      return 0
    }
    return navCollapsed ? SIDE_NAV_COLLAPSED_WIDTH : SIDE_NAV_WIDTH
  }

  const {status} = useThemeSwitcher();

  if (status === 'loading') {
    return <Loading cover="page"/>;
  }

  const getLayoutDirectionGutter = () => {
    if (direction === DIR_LTR) {
      return {paddingLeft: getLayoutGutter()}
    }
    if (direction === DIR_RTL) {
      return {paddingRight: getLayoutGutter()}
    }
    return {paddingLeft: getLayoutGutter()}
  }

  return (
    <IntlProvider
      locale={currentAppLocale.locale}
      messages={currentAppLocale.messages}>
      <ConfigProvider locale={currentAppLocale.antd} direction={direction as ConfigProviderProps['direction']}>
        <Layout>
          <HeaderNav isMobile={isMobile}/>
          {(isNavTop && !isMobile) ? <TopNav/> : null}
          <Layout className="app-container">
            {(isNavSide && !isMobile) ? <SideNav routeInfo={currentRouteInfo}/> : null}
            <Layout className="app-layout" style={getLayoutDirectionGutter()}>
              <div className={`app-content ${isNavTop ? 'layout-top-nav' : ''}`}>
                <PageHeader display={currentRouteInfo?.breadcrumb} title={currentRouteInfo?.title}/>
                <Content>
                  {children}
                </Content>
              </div>
              <Footer/>
            </Layout>
          </Layout>
          {isMobile && <MobileNav/>}
        </Layout>
      </ConfigProvider>
    </IntlProvider>
  )
}

const AppLayout = ({children}: AppLayoutProps) => {
  return (
    <ThemeSwitcherProvider
      themeMap={themes}
      defaultTheme={THEME_CONFIG.currentTheme}
      insertionPoint="styles-insertion-point"
    >
      <AppLayoutInner>
        {children}
      </AppLayoutInner>
    </ThemeSwitcherProvider>
  )
}

export default React.memo(AppLayout);
