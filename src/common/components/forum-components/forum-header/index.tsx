import React from "react";
import {Card, Col, Row} from "antd";
import {useSelector} from "react-redux";
import {RootState} from "../../../redux/store";

const ForumHeader = (props: any) => {
  const {user} = useSelector((state: RootState) => state.auth)
  return (
    <Row>
      <Col span={24}>
        <Card className={"forum-header"}>
          <div className={"forum-header-content"}>
            {/*<div className={"category"}>*/}
            {/*  General*/}
            {/*</div>*/}
            <div className={"title"}>
              {
                props?.topic ? props?.topic?.title.toUpperCase() : props?.department ? props?.department.name : "Welcome to IDDE"
              }
            </div>
            <div className={"author"}>
              <img src={props?.topic ? props?.topic?.author?.avatar: user.avatar } alt={"avatar"}/>
              {props?.topic ? props?.topic?.author?.fullName : user.fullName}
            </div>
          </div>
        </Card>
      </Col>
    </Row>
  )
};

export default ForumHeader;
