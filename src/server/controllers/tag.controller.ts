import {tagService} from 'server/services';
import pick from "server/utils/pick";
import {ExtendedRequest, ExtendedResponse} from "server/interfaces/types";

export const addTag = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const tag = await tagService.createTag(req.body);
  res.json({
    message: "Created Tag successfully",
    tag
  });
};

export const getTags = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const filter = pick(req.query, ['title', 'slug']);
  const options = pick(req.query, ['sortBy', 'limit', 'page']);
  const result = await tagService.queryTags(filter, options);
  res.json(result);
};

export const getTag = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const result = await tagService.getTagByFilter({_id: req.query.tagId});
  res.json(result);
};


export const deleteTag = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const tag = await tagService.deleteTag({_id: req.query.tagId});
  res.json({
    message: "Deleted Tag successfully",
    tag
  });
};