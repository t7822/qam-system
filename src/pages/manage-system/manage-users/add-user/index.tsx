import UserForm from "common/components/user-components/user-form";
import {SessionData} from "server/interfaces/types";
import {withSessionSsr} from "server/utils/with-session";
import {auth} from "server/utils/auth";
import {queryRoles} from "server/services/role.service";
import {queryDepartments} from "server/services/department.service";

const AddUser = (props: any) => {
  return (
      <UserForm mode="ADD" roles={props.roles} departments={props.departments}/>
  )
}

export const getServerSideProps = withSessionSsr(
  async function getServerSideProps({req, res}) : Promise<any> {
    try {
      await auth(req.session as SessionData, ["MANAGE_ALL_USER", "ADD_ALL_USER"]);
      const departments = await queryDepartments({deleted: {$ne: true}}, {page: 1, limit: 10, sortBy: "-createdAt"});
      const roles = await queryRoles({deleted: {$ne: true}}, {page: 1, limit: 10, sortBy: "-createdAt"});
      return {
        props: {
          departments: JSON.parse(JSON.stringify(departments?.results)),
          roles: JSON.parse(JSON.stringify(roles?.results))
        }
      }
    } catch (err) {
      res.statusCode = err["statusCode"] || 500;
      return {
        redirect: {
          permanent: false,
          destination: (err.statusCode === 401) ? '/auth/login' : '/',
        },
      }
    }
  }
)

export default AddUser
