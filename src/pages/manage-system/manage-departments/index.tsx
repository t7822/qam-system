import {Button, Tooltip, Modal, Card, Table, Form, Input, notification, Pagination, Spin} from "antd";
import {DeleteOutlined, EditOutlined, FolderAddOutlined} from "@ant-design/icons";
import React, {useEffect, useState} from "react";
import ManageSystem from "../index";
import {ApiService} from "common/services/ApiService";
import {withSessionSsr} from "server/utils/with-session";
import {auth} from "server/utils/auth";
import {SessionData} from "server/interfaces/types";
import {queryDepartments} from "server/services/department.service";

const {confirm} = Modal;

const RoleList = (props: any) => {
  const [form] = Form.useForm();
  const [visible, setVisible] = useState(false);
  const [mode, setMode] = useState("");
  const [slug, setSlug] = useState("");
  const [departments, setDepartments] = useState(props.departments || []);
  const [loading, setLoading] = useState(false);
  const [modalLoading, setModalLoading] = useState(false);
  const [pageInfo, setPageInfo] = useState(props.pageInfo || {});

  useEffect(() => {
    if (mode === "edit") {
      if (slug !== "") {
        (async () => {
          await getDepartment()
        })()
      }
    } else {
      form.resetFields
    }
  }, [mode, slug]);

  const getDepartment = async () => {
    setModalLoading(true)
    try {
      const res = await ApiService.getDepartment(slug);
      form.setFieldsValue({
        name: res?.name
      })
      setModalLoading(false);
    } catch (err) {
      notification.error({
        message: err
      })
    }
  }

  const handleChange = async (value: any) => {
    const res = await ApiService.getDepartments({
      page: value,
      limit: 10,
      sortBy: "createdAt"
    });
    setDepartments(res.results)
    setPageInfo({
      totalResults: res.totalResults,
      hasNextPage: res?.page < res?.totalPages
    })
  }

  const columns: any = [
    {
      title: "Name",
      align: "center",
      dataIndex: "name",
      key: "name",
    },
    {
      title: "Slug",
      align: "center",
      dataIndex: "slug",
      key: "slug",
    },
    {
      title: "Action",
      dataIndex: "action",
      align: "center",
      key: "action",
      render: (_: any, record: any) => (
        <div className="text-right d-flex justify-content-center">
          <Tooltip title="Update">
            <Button type="default" className="mr-2" icon={<EditOutlined/>} onClick={() => {
              setMode("edit");
              setSlug(record.slug);
              setVisible(true);
            }} size="small"/>
          </Tooltip>
          <Tooltip title="Delete">
            <Button danger icon={<DeleteOutlined/>} onClick={() => showDeleteConfirm(record.slug)}
                    size="small"/>
          </Tooltip>
        </div>
      )
    }
  ];

  const submitDepartment = async () => {
    const values = form.getFieldsValue();
    setLoading(true);
    if (mode === "add") {
      try {
        const res = await ApiService.addDepartment(values);
        if (res.status) {
          notification.error({
            message: res.data.message
          })
          return setLoading(false)
        }
        const newList = [...departments, res.department]
        setDepartments(newList);
        setLoading(false)
        setVisible(false);
        form.resetFields();
        notification.success({
          message: res.message
        })
      } catch (err) {
        notification.error({message: err})
        setLoading(false)
      }
    } else if (mode === "edit") {
      try {
        const res = await ApiService.updateDepartment(slug, values);
        if (res.status) {
          notification.error({
            message: res.data.message
          })
          return setLoading(false)
        }
        const newList = departments.map((department: any) => {
          if (res?.department?._id === department._id) {
            return res.department
          } else {
            return department
          }
        });
        setDepartments(newList);
        setLoading(false);
        setSlug("");
        setMode("");
        setVisible(false);
        form.resetFields();
        notification.success({
          message: res.message
        })
      } catch (err) {
        notification.error({message: err})
        setLoading(false);
      }
    }

  }

  const showDeleteConfirm = (slug: any) => {
    return (
      confirm({
        title: 'Are you sure delete this department?',
        content: 'This action can not undo, so do you want to delete?',
        okText: 'Yes',
        okType: 'danger',
        cancelText: 'No',
        onOk: async () => {
          const res = await ApiService.deleteDepartment(slug);
          if (res.status) {
            return notification.error({
              message: res.data.message
            })
          }
          const newList = [...departments].filter(department => department._id !== res.department._id)
          setDepartments(newList)
          notification.success({
            message: res.message
          })
        },
      })
    )
  }

  return (
    <ManageSystem>
      <div>
        <div className="mb-3" style={{
          display: "flex",
          justifyContent: "space-between"
        }}>
          <Button type="primary" icon={<FolderAddOutlined/>} onClick={() => {
            setVisible(true);
            setMode("add");
          }}>
            Add Department
          </Button>
        </div>
        <Card>
          <div className="table-responsive">
            <Table
              rowKey={(record) => record.slug}
              columns={columns}
              dataSource={departments}
              pagination={false}
              footer={() => {
                return (
                  <Pagination
                    showQuickJumper
                    defaultCurrent={1}
                    total={pageInfo.totalResults}
                    onChange={handleChange}
                  />
                )
              }}
            />
          </div>
        </Card>
      </div>
      <Modal
        visible={visible}
        title={mode === "add" ? "Add Department" : "Edit Department"}
        onCancel={() => {
          setVisible(false);
          setMode("");
          setSlug("");
          form.resetFields();
        }}
        okButtonProps={{form: 'department-form-submit', htmlType: 'submit'}}
        confirmLoading={loading}
        okText={mode === "add" ? "Submit" : "Save"}
      >
        <Spin spinning={modalLoading} tip={"Loading...."}>
          <Form
            onFinish={submitDepartment}
            id="department-form-submit"
            form={form}
            layout="vertical"
          >
            <Form.Item
              label="Name"
              name="name"
              rules={[{required: true, message: 'Please input Department name!'}]}
            >
              <Input placeholder="Input Department name..."/>
            </Form.Item>
          </Form>
        </Spin>
      </Modal>
    </ManageSystem>
  )
}

export const getServerSideProps = withSessionSsr(
  async function getServerSideProps({req, res}): Promise<any> {
    try {
      await auth(req.session as SessionData, ["MANAGE_ALL_DEPARTMENT"]);
      const departments = await queryDepartments({deleted: {$ne: true}}, {page: 1, limit: 10, sortBy: "-createdAt"});
      return {
        props: {
          departments: JSON.parse(JSON.stringify(departments?.results)),
          pageInfo: {
            totalResults: departments?.totalResults,
            hasNextPage: departments?.page < departments?.totalPages
          }
        }
      }
    } catch (err) {
      res.statusCode = err["statusCode"] || 500;
      return {
        redirect: {
          permanent: false,
          destination: (err.statusCode === 401) ? '/auth/login' : '/',
        },
      }
    }
  }
);

export default RoleList
