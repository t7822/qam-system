import {Document, Model} from 'mongoose';
import {ITopic} from "./topic.interface";
import {IUser} from "./user.interface";
import {IIdeaTag} from "./idea-tag.interface";

export interface IdeaInterface extends Document {
  author: IUser["_id"],
  title: string,
  slug: string,
  topic: ITopic["_id"],
  content: string,
  privacy: string,
  editedAt: string,
  editedTimes: number,
  deleted: boolean,
  type: string,
  isAttachmentsChanged: boolean,
  createdAt: number,
  updatedAt: number,
}

// for methods
export interface IIdea extends IdeaInterface {
  tags: [IIdeaTag]
}

// for static methods
export interface IdeaModel extends Model<IIdea> {
  paginate(filter: object, options: object): any,

  slugGenerator(title: string): Promise<string>,
}