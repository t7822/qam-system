import errorHandler from 'server/middleware/error.middleware';
import auth from 'server/middleware/auth.middleware';
import connectDB from 'server/middleware/mongodb.middleware';
import validate from 'server/middleware/validate.middleware';
import upload from 'server/middleware/upload.middleware';
import recordUserActivity from "server/middleware/user-activity.middleware";

export {
  errorHandler,
  auth,
  connectDB,
  validate,
  upload,
  recordUserActivity
};
