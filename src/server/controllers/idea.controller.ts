import {ideaService, userActivityService} from 'server/services';
import {ExtendedRequest, ExtendedResponse} from 'server/interfaces/types';
import pick from "server/utils/pick";
import {sendNotificationEmail} from "server/services/email.service";
import {notificationEmailContent, urls} from "server/config/email.config";
import fs from 'fs';
import {Role} from "../models";

export const addIdea = async (req: ExtendedRequest, res: ExtendedResponse) => {
  Object.assign(req.body, {author: req.session.user?._id});
  const idea = await ideaService.createIdea(req.body, req.files, req.session.user?.department._id.toString());
  res.json({
    message: "Created Idea successfully",
    idea
  });

  const roles = (await Role.find({name: {$in: ["admin", "manager", "coordinator"]}})).map(data => data._id);

  await sendNotificationEmail(notificationEmailContent(urls.redirectUrl + idea.slug, `${req.session.user?.fullName} added a new idea!`), {role: {$in: roles}});
  await userActivityService.createUserActivity({
    message: `has added a new idea`,
    detail: idea.title,
    target: idea._id,
    model: "Idea",
    user: req.session.user?._id
  });
};

export const getIdeas = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const filter = pick(req.query, ['title', 'slug', "author", "tag", "topic", "closureDate"]);
  const options = pick(req.query, ['sortBy', 'limit', 'page']);
  Object.assign(filter, {deleted: {$ne: true}});
  const result = await ideaService.queryIdeas(filter, options, req.session.user);
  res.json(result);
};

export const exportCSV = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const filter = pick(req.query, ['title', 'slug', "author", "tag", "topic", "closureDate"]);
  const options = pick(req.query, ['sortBy', 'limit', 'page']);
  Object.assign(filter, {deleted: {$ne: true}});
  const result = await ideaService.exportCSV(filter, options);
  res.setHeader('Content-Type', 'application/csv');
  res.setHeader('Content-Disposition', 'attachment; filename=' + result.filename);
  res.status(200).send(result.stream);
};

export const exportZip = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const result = await ideaService.exportZip(req.query.slug.toString());
  res.setHeader('Content-Type', 'application/zip');
  res.setHeader('Content-Disposition', 'attachment; filename=' + result.filename);
  res.setHeader('Content-Length', fs.statSync(result.filepath).size);
  const readStream = fs.createReadStream(result.filepath);
  readStream.pipe(res);
};

export const getSavedIdea = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const filter = {saved: true, deleted: {$ne: true}};
  const options = pick(req.query, ['sortBy', 'limit', 'page']);
  const result = await ideaService.queryIdeas(filter, options, req.session.user);
  res.json(result);
};

export const getDeletedIdeas = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const filter = pick(req.query, ['title', 'slug']);
  const options = pick(req.query, ['sortBy', 'limit', 'page']);
  Object.assign(filter, {deleted: true});
  const result = await ideaService.queryIdeas(filter, options, req.session.user);
  res.json(result);
};

export const getIdea = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const userId = req.session.user ? req.session.user?._id : null;
  const slug: string = req.query.slug.toString();
  let newView: boolean = false;
  if (!req.session.views) {
    req.session.views = [];
    await req.session.save();
  }
  if (!req.session.views.includes(slug)) newView = true;
  const result = await ideaService.getIdeaByFilter({slug: slug}, userId, newView);
  if (result && newView) {
    req.session.views.push(slug);
    await req.session.save();
  }
  res.json(result);
};

export const updateIdea = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const idea = await ideaService.updateIdea(req.query.slug as string, req.body, req.session.user?.role.permissions.includes('MANAGE_ALL_IDEA' || 'UPDATE_ALL_IDEA'), req.session.user?._id);
  res.json({
    message: "Updated idea successfully",
    idea
  });
  await userActivityService.createUserActivity({
    message: `has updated a forum idea`,
    detail: idea.title,
    target: idea._id,
    model: "Idea",
    user: req.session.user?._id
  });
};

export const saveIdea = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const {idea, message} = await ideaService.saveIdea(req.query.slug as string, req.session.user?._id);
  res.json({
    message: message,
    idea
  });
  await userActivityService.createUserActivity({
    message: (message.includes("Unsaved")) ? `has unsaved a forum idea` : `has saved a forum idea`,
    detail: idea.title,
    target: idea._id,
    model: "Idea",
    user: req.session.user?._id
  });
};

export const deleteIdea = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const idea = await ideaService.deleteIdea(req.query.slug as string, req.session.user?.role.permissions.includes('MANAGE_ALL_IDEA' || 'DELETE_ALL_IDEA'), req.session.user?._id);
  res.json({
    message: "Deleted idea successfully",
    idea
  });
  await userActivityService.createUserActivity({
    message: `has deleted a forum idea`,
    detail: idea.title,
    target: idea._id,
    model: "Idea",
    user: req.session.user?._id
  });
};

export const restoreIdeas = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const result = await ideaService.restoreIdeas(req.body);
  res.json({
    message: "Restore Ideas successfully",
    result
  });
};

export const deleteIdeasPermanently = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const result = await ideaService.deleteIdeasPermanently(req.body);
  res.json({
    message: "Delete ideas successfully",
    result
  });
};