import {connectDB, errorHandler} from "server/middleware";
import {authController} from "server/controllers";
import middlewares from "server";
import {NextApiHandler, NextApiRequest, NextApiResponse} from "next";
import { withIronSessionApiRoute } from "iron-session/next";
import { ironOptions } from "server/config";

const handler : NextApiHandler = async (req : NextApiRequest, res : NextApiResponse) => {
  await middlewares(req, res);

  if (req.method === "GET" || req.method === "POST") {
    try {
      await authController.getSessionData(req, res);
    } catch (err) {
      errorHandler(err, req, res);
    }
  }
};

export default connectDB(withIronSessionApiRoute(handler, ironOptions));
