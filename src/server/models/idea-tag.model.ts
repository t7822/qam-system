import {Schema, model, models} from 'mongoose';
import {toJSON, paginate} from 'server/models/plugins';
import {IIdeaTag, IdeaTagModel} from "./interfaces/idea-tag.interface";

const itSchema = new Schema<IIdeaTag>({
  idea: {
    type: Schema.Types.ObjectId,
    ref: "Idea",
  },
  tag: {
    type: Schema.Types.ObjectId,
    ref: "Tag"
  },
}, {
  collection: "ideatags",
  timestamps: true,
});

// add plugin that converts mongoose to json
itSchema.plugin(toJSON);
itSchema.plugin(paginate);

const IdeaTag = models.IdeaTag || model<IIdeaTag, IdeaTagModel>('IdeaTag', itSchema);

export default IdeaTag;
