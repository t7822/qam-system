import {Schema, model, models} from 'mongoose';
import {toJSON, paginate} from 'server/models/plugins';
import {IDepartment, DepartmentModel} from "./interfaces/department.interface";
import {slugify} from "../utils/slugify";

const departmentSchema = new Schema<IDepartment>({
  manager: {
    type: Schema.Types.ObjectId,
    ref: "User",
  },
  name: {
    type: String,
    required: true
  },
  slug: {
    type: String,
  },
  deleted: {
    type: Boolean,
    default: false,
  },
  createdAt: {
    type: Number,
    default: Date.now(),
  }
}, {
  collection: "departments",
});

// add plugin that converts mongoose to json
departmentSchema.plugin(toJSON);
departmentSchema.plugin(paginate);

departmentSchema.statics.slugGenerator = async function (name: string): Promise<string> {
  let newSlug = slugify(name);
  let count = 0;
  while (await this.exists({slug: newSlug})) {
    newSlug = `${slugify(name)}_${++count}`;
  }
  return newSlug;
};

departmentSchema.pre('save', async function (next) {
  const department = this;
  if (department.isModified("name")) {
    department.slug = await Department.slugGenerator(department.name);
  }
  next();
});

const Department = models.Department || model<IDepartment, DepartmentModel>('Department', departmentSchema);

export default Department;
