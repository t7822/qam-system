import Joi from 'joi';
import {ObjectId} from "server/validations/custom.validations";

export const getReactions: {params: object} = {
  params: Joi.object().keys({
    targetId: Joi.string().custom(ObjectId),
  }),
};

export const updateReaction: {params: object, body: object} = {
  params: Joi.object().keys({
    targetId: Joi.required().custom(ObjectId),
  }),
  body: Joi.object()
    .keys({
      type: Joi.string(),
      active: Joi.string(),
      model: Joi.string().required(),
      reactTo: Joi.string(),
      delete: Joi.boolean()
    })
    .min(1),
};