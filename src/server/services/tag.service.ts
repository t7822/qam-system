import httpStatus from 'http-status';
import {Tag} from 'server/models';
import ApiError from 'server/utils/api-error';
import {ITag} from "server/models/interfaces/tag.interface";

export const createTag = async (tagBody: any): Promise<ITag> => {
  return Tag.create(tagBody);
};

export const queryTags = async (filter: any, options: any): Promise<any> => {
  Object.assign(options, {lean: true});
  return Tag.paginate(filter, options);
};

export const getTagByFilter = async (filter: any): Promise<ITag> => {
  const tag = await Tag.findOne({...filter});
  if (!tag) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Tag not found');
  }
  return tag;
};

export const deleteTag = async (filter: any): Promise<ITag> => {
  const tag = await Tag.findOne(filter);
  if (!tag) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Tag not found');
  }
  await tag.deleteOne();
  return tag;
}
