import React from "react";
import InnerAppLayout from "common/layouts/inner-app-layout";
import {Menu} from "antd";
import Link from "next/link";
import {
  SolutionOutlined,
  UserOutlined,
  BookOutlined,
  ReadOutlined,
  SnippetsOutlined,
  UsergroupAddOutlined
} from "@ant-design/icons";
import {useRouter} from "next/router";
import {useSelector} from "react-redux";
import {RootState} from "../../common/redux/store";


const ManageSystem = (props: any) => {
  const {user} = useSelector((state: RootState) => state.auth)
  const router = useRouter();

  const ManageSystemMenu = () => (
    <div className="w-100">
      <Menu
        defaultSelectedKeys={[user.role.name === "admin" ? '/manage-system/manage-users' : '/manage-system/tags']}
        selectedKeys={[router.asPath]}
        mode={"vertical"}
        theme={"light"}
      >
        {
          user?.role?.name === "admin" && (
            <>
              <Menu.Item key={'/manage-system/manage-users'}>
                <Link href={`/manage-system/manage-users`}>
                  <a>
                    <UserOutlined/>
                    <span>Manage User</span>
                  </a>
                </Link>
              </Menu.Item>
              <Menu.Item key={'/manage-system/manage-roles'}>
                <Link href={`/manage-system/manage-roles`}>
                  <a>
                    <SolutionOutlined/>
                    <span>Manage Role</span>
                  </a>
                </Link>
              </Menu.Item>
              <Menu.Item key={'/manage-system/manage-departments'}>
                <Link href={`/manage-system/manage-departments`}>
                  <a>
                    <UsergroupAddOutlined/>
                    <span>Manage Departments</span>
                  </a>
                </Link>
              </Menu.Item>

              <Menu.Item key={'/manage-system/manage-campaigns'}>
                <Link href={`/manage-system/manage-campaigns`}>
                  <a>
                    <SnippetsOutlined/>
                    <span>Manage Campaigns</span>
                  </a>
                </Link>
              </Menu.Item>
            </>
          )
        }
        {
          user?.role?.name === "manager" && (
            <>
              <Menu.Item key={'/manage-system/manage-tags'}>
                <Link href={`/manage-system/manage-tags`}>
                  <a>
                    <BookOutlined/>
                    <span>Manage Tag</span>
                  </a>
                </Link>
              </Menu.Item>
              <Menu.Item key={'/manage-system/manage-ideas'}>
                <Link href={`/manage-system/manage-ideas`}>
                  <a>
                    <ReadOutlined/>
                    <span>Manage Idea</span>
                  </a>
                </Link>
              </Menu.Item></>
          )
        }

      </Menu>
    </div>
  )

  return (
    <InnerAppLayout
      sideContent={<ManageSystemMenu {...props}/>}
      mainContent={props.children}
      border
    />
  )
}
export default ManageSystem
