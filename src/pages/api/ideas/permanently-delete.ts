import {auth, connectDB, validate, errorHandler} from "server/middleware";
import {ideaController} from "server/controllers";
import middlewares from "server";
import {runMiddleware} from "server/utils/middleware-handler";
import {deleteIdeasPermanently} from "server/validations/idea.validation";
import {withIronSessionApiRoute} from "iron-session/next";
import {ironOptions} from "server/config";
import {NextApiHandler, NextApiRequest, NextApiResponse} from "next";

const handler: NextApiHandler = async (req: NextApiRequest, res: NextApiResponse) => {
  await middlewares(req, res);
  await runMiddleware(req, res, auth(req, res, "MANAGE_ALL_COMMUNITY_QUESTION"));
  await runMiddleware(req, res, validate(req, res, deleteIdeasPermanently));

  if (req.method === "POST") {
    try {
      await ideaController.deleteIdeasPermanently(req, res);
    } catch (err) {
      errorHandler(err, req, res);
    }
  }
};

export default connectDB(withIronSessionApiRoute(handler, ironOptions));
