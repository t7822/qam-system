import httpStatus from 'http-status';
import {userService} from 'server/services/index';
import ApiError from 'server/utils/api-error';
import {IUser} from "server/models/interfaces/user.interface";

export const login = async (loginData: any): Promise<IUser> => {
  const {username, password} = loginData;

  const user = await userService.getUserByFilter({username});

  if (!user) {
    throw new ApiError(httpStatus.UNAUTHORIZED, 'User not found');
  }

  if (!(await user.isPasswordMatch(password))) {
    throw new ApiError(httpStatus.UNAUTHORIZED, 'Provided password is not correct');
  }
  return user;
};
