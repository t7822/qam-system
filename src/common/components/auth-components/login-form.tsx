import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {Alert, Button, Form, Input} from "antd";
import {MailOutlined, LockOutlined} from '@ant-design/icons';
import {useRouter} from "next/router";
import {RootState} from "../../redux/store";
import {doLogin, ILoginData, setShowAuthMessage} from "../../redux/slices/auth.slice";
import {motion} from "framer-motion"


interface LoginFormProps {
  otherSignIn?: boolean,
  showForgetPassword?: boolean,
  extra?: string | Element,
  isPopup?: boolean
}

export const LoginForm = (props: LoginFormProps) => {
  const {authLoading, showAuthMessage, authMessage, isLoggedIn} = useSelector((state: RootState) => state.auth);
  const dispatch = useDispatch();
  const router = useRouter();
  const {
    extra,
  } = props;

  const onSignIn = (values: ILoginData) => {
    dispatch(doLogin(values));
  };

  useEffect(() => {
    if (isLoggedIn) {
      router.replace("/").then(() => {
        //
      });
    }
  }, [isLoggedIn, router]);

  useEffect(() => {
    if (showAuthMessage) {
      setTimeout(() => {
        dispatch(setShowAuthMessage(false));
      }, 3000);
    }
  }, [showAuthMessage]);

  return (
    <>
      <motion.div
        initial={{opacity: 0, marginBottom: 0}}
        animate={{
          opacity: showAuthMessage ? 1 : 0,
          marginBottom: showAuthMessage ? 20 : 0
        }}>
        <Alert type="error" showIcon message={authMessage}/>
      </motion.div>
      <Form
        layout="vertical"
        name="login-form"
        onFinish={onSignIn}
      >
        <Form.Item
          name="username"
          label="Username"
          rules={[
            {
              required: true,
              message: 'Please input your username',
            },
          ]}>
          <Input prefix={<MailOutlined className="text-primary"/>}/>
        </Form.Item>
        <Form.Item
          className='mb-2'
          label="Password"
          name="password"
          rules={[{
            required: true,
            message: 'Please input your password',
          }]}
        >
          <Input.Password prefix={<LockOutlined className="text-primary"/>}/>
        </Form.Item>
        <Form.Item
          className='mb-2'
        >
          <Button
            className='float-right p-0 text-muted font-size-base font-weight-normal under-line'
            type='link'
            onClick={() => router.push("/auth/forgot-password")}
          >
            Forgot password?
          </Button>
        </Form.Item>
        <Form.Item>
          <Button type="primary" htmlType="submit" block loading={authLoading}>
            Sign In
          </Button>
        </Form.Item>
        {extra}
        {/*{*/}
        {/*  !isPopup && <div className='d-flex justify-content-center'>*/}
        {/*    <Button*/}
        {/*      className='p-0 text-gray'*/}
        {/*      // icon={<ArrowLeftOutlined />}*/}
        {/*      type='link'*/}
        {/*      onClick={() => {*/}
        {/*        router.push('/')*/}
        {/*      }}*/}
        {/*    >*/}
        {/*      <span className='under-line'>*/}
        {/*        Back to home page*/}
        {/*      </span>*/}
        {/*    </Button>*/}
        {/*  </div>*/}
        {/*}*/}
      </Form>
    </>
  )
}

export default LoginForm
