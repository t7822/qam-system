import httpStatus from 'http-status';
import {Vote, Idea} from 'server/models';
import ApiError from 'server/utils/api-error';
import {Schema, Types} from "mongoose";

export const updateVote = async (ideaSlug: string, userId: Schema.Types.ObjectId, body: any) => {
  let idea = await Idea.findOne({slug: ideaSlug}).populate({path: "author", model: "User"});
  if (!idea || idea.deleted) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Idea not found');
  }
  let vote = await Vote.findOne({user: userId, idea: idea._id});
  let message = `You have given a vote to a idea`;
  if (!vote) {
    vote = await Vote.create({
      user: userId,
      idea: idea._id,
      active: body.active ?? true,
      type: body.type ?? 'upvote',
    });
  } else {
    vote.active = body.active ?? vote.type === body.type ? !vote.active : true;
    vote.type = body.type;
    if (!vote.active) message = 'You have un-voted an idea';
    await vote.save();
  }
  const result = await getVotes(idea.slug);
  return {
    message: message,
    type: body.type,
    active: vote.active,
    idea: idea,
    votes: result,
  }
}

export const getVotes = async (ideaSlug: string) => {
  let idea = await Idea.findOne({slug: ideaSlug}).populate({path: "author", model: "User"});
  if (!idea || idea.deleted) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Idea not found');
  }
  let votes = await Vote.find({idea: idea, active: true}).populate({
    path: "user",
    model: "User",
    select: { avatar: 1, displayName: 1, fullName: 1, username: 1}
  }).lean();
  let result: any = {};
  let sum = 0;
  for (let vote of votes) {
    if (vote.type === "upvote") sum++;
    else sum--;
    if (!result[vote.type]) {
      result[vote.type] = {
        users: [vote.user],
        count: 1,
      }
    } else {
      result[vote.type].users.push(vote.user);
      result[vote.type].count++;
    }
  }
  result.total = sum;
  return result;
};
