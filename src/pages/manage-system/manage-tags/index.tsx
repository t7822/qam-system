import {Button, Tooltip, Modal, Card, Table, Form, Input, notification, Pagination} from "antd";
import {DeleteOutlined, FolderAddOutlined} from "@ant-design/icons";
import React, {useState} from "react";
import ManageSystem from "../index";
import {ApiService} from "common/services/ApiService";
import {withSessionSsr} from "server/utils/with-session";
import {auth} from "server/utils/auth";
import {SessionData} from "server/interfaces/types";
import {queryTags} from "server/services/tag.service";

const {confirm} = Modal;

const RoleList = (props: any) => {
  const [form] = Form.useForm();
  const [visible, setVisible] = useState(false);
  const [tags, setTags] = useState(props.tags || []);
  const [loading, setLoading] = useState(false);
  const [pageInfo, setPageInfo] = useState(props.pageInfo || {});

  const handleChange = async (value: any) => {
    const res = await ApiService.getTags({
      page: value,
      limit: 10,
      sortBy: "-createdAt"
    });
    setTags(res.results)
    setPageInfo({
      totalResults: res.totalResults,
      hasNextPage: res?.page < res?.totalPages
    })
  }

  const columns: any = [
    {
      title: "Title",
      align: "center",
      dataIndex: "title",
      key: "title",
    },
    {
      title: "Slug",
      align: "center",
      dataIndex: "slug",
      key: "slug",
    },
    {
      title: "Action",
      dataIndex: "action",
      align: "center",
      key: "action",
      render: (_: any, record: any) => (
        <div className="text-right d-flex justify-content-center">
          <Tooltip title="Delete">
            <Button type="default" danger icon={<DeleteOutlined/>} onClick={() => showDeleteConfirm(record._id)}
                    size="small"/>
          </Tooltip>
        </div>
      )
    }
  ];

  const handleTag = async () => {
    const values = form.getFieldsValue();
    setLoading(true);
    try {
      const res = await ApiService.addTag(values);
      if (res.status) {
        notification.error({
          message: res.data.message
        })
        return setLoading(false)
      }
      const newTags = [...tags, res.tag]
      setTags(newTags);
      setLoading(false)
      setVisible(false);
      form.resetFields();
      notification.success({
        message: res.message
      })
    } catch (err) {
      notification.error({message: err})
      setLoading(false)
    }
  }

  const showDeleteConfirm = (tagId: any) => {
    return (
      confirm({
        title: 'Are you sure delete this category?',
        content: 'This action can not undo, so do you want to delete?',
        okText: 'Yes',
        okType: 'danger',
        cancelText: 'No',
        onOk: async () => {
          const res = await ApiService.deleteTag(tagId);
          if (res.status) {
            return notification.error({
              message: res.data.message
            })
          }
          const newTags = [...tags].filter(tag => tag._id !== res.tag._id)
          setTags(newTags)
          notification.success({
            message: res.message
          })
        },
      })
    )
  }

  return (
    <ManageSystem>
      <div>
        <div className="mb-3" style={{
          display: "flex",
          justifyContent: "space-between"
        }}>
          <Button type="primary" icon={<FolderAddOutlined/>} onClick={() => {
            setVisible(true);
          }}>
            Add Tag
          </Button>
        </div>
        <Card>
          <div className="table-responsive">
            <Table
              rowKey={(record) => record.slug}
              columns={columns}
              dataSource={tags}
              pagination={false}
              footer={() => {
                return (
                  <Pagination
                    showQuickJumper
                    defaultCurrent={1}
                    total={pageInfo.totalResults}
                    onChange={handleChange}
                  />
                )
              }}
            />
          </div>
        </Card>
      </div>
      <Modal
        visible={visible}
        title="Add Tag"
        onCancel={() => {
          setVisible(false)
          form.resetFields();
        }}
        okButtonProps={{form: 'tag-form-submit', htmlType: 'submit'}}
        confirmLoading={loading}
        okText={"Submit"}
      >
        <Form
          id='tag-form-submit'
          form={form}
          layout="vertical"
          onFinish={handleTag}
        >
          <Form.Item label="Tag Title" name="title" rules={[{required: true, message: 'Please input title!'}]}>
            <Input placeholder="Input Title Tag..."/>
          </Form.Item>
        </Form>
      </Modal>
    </ManageSystem>
  )
}

export const getServerSideProps = withSessionSsr(
  async function getServerSideProps({req, res}): Promise<any> {
    try {
      await auth(req.session as SessionData, ["MANAGE_ALL_TAG"]);
      const tags = await queryTags({deleted: {$ne: true}}, {page: 1, limit: 10, sortBy: "-createdAt"});
      return {
        props: {
          tags: JSON.parse(JSON.stringify(tags?.results)),
          pageInfo: {
            totalResults: tags?.totalResults,
            hasNextPage: tags?.page < tags?.totalPages
          }
        }
      }
    } catch (err) {
      res.statusCode = err["statusCode"] || 500;
      return {
        redirect: {
          permanent: false,
          destination: (err.statusCode === 401) ? '/auth/login' : '/',
        },
      }
    }
  }
);

export default RoleList
