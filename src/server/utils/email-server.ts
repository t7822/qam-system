import ApiError from "server/utils/api-error";
import httpStatus from "http-status";
import mailgun from "mailgun-js";
import config from "server/config";
import logger from "server/config/logger.config";
import { MailData } from "server/interfaces/types";

export const sendEmailFromMailGun = async (mailData: MailData): Promise<object> => {
  const mg = mailgun({
    apiKey: config.mailgun.apiKey, domain: config.mailgun.domain
  });

  try {
    mg.messages().send({
      from: `IDDE <postmaster@idde.me>`,
      to: mailData.sendTo,
      subject: mailData.subject,
      html: mailData.contentHTML,
    });
    if (config.env !== "production") logger.info(`Email sent to ${mailData.sendTo}`);
    return {
      message: 'A request was sent to your email'
    };
  } catch (err) {
    if (config.env !== "production") logger.error(err);
    throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, "Cannot send email");
  }
}