import React from "react";
import {Menu, Dropdown, Avatar} from "antd";
import {
  UserOutlined,
  LogoutOutlined, EditOutlined
} from '@ant-design/icons';
import Icon from 'common/components/util-components/Icon';
import {doSignOut, UserData} from "../../redux/slices/auth.slice";
import {useDispatch} from "react-redux";

const menuItem = [
  {
    title: "Profile",
    icon: UserOutlined,
    path: "/profile/me"
  },
  {
    title: "Account Settings",
    icon: EditOutlined,
    path: "/account/info"
  },
]

export const NavProfile = ({user}: { user: UserData }) => {
  const profileImg = user.avatar;
  const dispatch = useDispatch();
  const profileMenu = (
    <div className="nav-profile nav-dropdown">
      <div className="nav-profile-header">
        <div className="d-flex">
          <Avatar size={45} src={profileImg}/>
          <div className="pl-3">
            <h4 className="mb-0">{user.fullName}</h4>
            <span className="text-muted">{user.id}</span>
          </div>
        </div>
      </div>
      <div className="nav-profile-body">
        <Menu>
          {menuItem.map((el, i) => {
            return (
              <Menu.Item key={i}>
                <a href={el.path}>
                  <Icon type={el.icon}/>
                  <span className="font-weight-normal">{el.title}</span>
                </a>
              </Menu.Item>
            );
          })}
          <Menu.Item key={menuItem.length + 1} onClick={() => {
            dispatch(doSignOut());
          }}>
            <span>
              <LogoutOutlined/>
              <span className="font-weight-normal">Sign Out</span>
            </span>
          </Menu.Item>
        </Menu>
      </div>
    </div>
  );
  return (
    <Dropdown placement="bottomRight" overlay={profileMenu} trigger={["click"]}>
      <Menu className="d-flex align-item-center" mode="horizontal">
        <Menu.Item key="profile">
          <Avatar src={profileImg}/>
        </Menu.Item>
      </Menu>
    </Dropdown>
  );
}

export default NavProfile;
