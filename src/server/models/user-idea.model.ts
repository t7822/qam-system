import {Schema, models, model} from 'mongoose';
import {paginate, toJSON} from "server/models/plugins";
import {IUserIdea, UserIdeaModel} from "server/models/interfaces/user-idea.interface";

const uIdeaSchema = new Schema<IUserIdea>({
  idea: {
    type: Schema.Types.ObjectId,
    required: true
  },
  user: {
    type: Schema.Types.ObjectId,
    ref: "User",
    required: true
  },
}, {
  collection: 'userideas',
  timestamps: true
});

uIdeaSchema.plugin(paginate);
uIdeaSchema.plugin(toJSON);


const UserIdea = models.UserIdea as UserIdeaModel || model<IUserIdea, UserIdeaModel>("UserIdea", uIdeaSchema);

export default UserIdea;