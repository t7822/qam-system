import {auth, connectDB, validate, errorHandler} from "server/middleware";
import {statisticController} from "server/controllers";
import middlewares from "server";
import {runMiddleware} from "server/utils/middleware-handler";
import {getTopTopicIdeasStatistic} from "server/validations/statistic.validation";
import {withIronSessionApiRoute} from "iron-session/next";
import {ironOptions} from "server/config";
import {NextApiHandler, NextApiRequest, NextApiResponse} from "next";

const handler: NextApiHandler = async (req: NextApiRequest, res: NextApiResponse) => {
  await middlewares(req, res);
  await runMiddleware(req, res, auth(req, res, "GET_STATISTIC"));
  await runMiddleware(req, res, validate(req, res, getTopTopicIdeasStatistic));

  if (req.method === "GET") {
    try {
      await statisticController.getTopTopicIdeasStatistic(req, res);
    } catch (err) {
      errorHandler(err, req, res);
    }
  }
};

export default connectDB(withIronSessionApiRoute(handler, ironOptions));