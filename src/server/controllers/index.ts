import {IUser} from "server/models/interfaces/user.interface";

declare module "iron-session" {
  interface IronSessionData {
    isLoggedIn?: boolean,
    user?: IUser,
    views?: Array<string>,
  }
}

export * as authController from 'server/controllers/auth.controller';
export * as userController from 'server/controllers/user.controller';
export * as commentController from 'server/controllers/comment.controller';
export * as ideaController from 'server/controllers/idea.controller';
export * as tagController from 'server/controllers/tag.controller';
export * as reactionController from 'server/controllers/reaction.controller';
export * as userActivityController from 'server/controllers/user-activity.controller';
export * as roleController from 'server/controllers/role.controller';
export * as topicController from 'server/controllers/topic.controller';
export * as statisticController from 'server/controllers/statistic.controller';
export * as attachmentController from 'server/controllers/attachment.controller';
export * as voteController from 'server/controllers/vote.controller';
export * as departmentController from 'server/controllers/department.controller';