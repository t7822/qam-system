import {auth, connectDB, validate, errorHandler, upload} from "server/middleware";
import {ideaController} from "server/controllers";
import middlewares from "server";
import {runMiddleware} from "server/utils/middleware-handler";
import {createIdea} from "server/validations/idea.validation";
import {withIronSessionApiRoute} from "iron-session/next";
import {ironOptions} from "server/config";
import {NextApiHandler, NextApiRequest, NextApiResponse} from "next";

export const config = {
  api: {
    bodyParser: false,
  },
};

const handler: NextApiHandler = async (req: NextApiRequest, res: NextApiResponse) => {
  await middlewares(req, res);
  await runMiddleware(req, res, auth(req, res));
  await runMiddleware(req, res, validate(req, res, createIdea));
  await runMiddleware(req, res, upload(req, res));
  if (req.method === "POST") {
    try {
      await ideaController.addIdea(req, res);
    } catch (err) {
      errorHandler(err, req, res);
    }
  }
};

export default connectDB(withIronSessionApiRoute(handler, ironOptions));
