import {Document, Model, Schema} from 'mongoose';
import {IUser} from "./user.interface";

export interface UserActivityInterface extends Document {
  message: string,
  detail: string,
  target: Schema.Types.ObjectId,
  model: string,
  user: IUser["_id"],
}

// for methods
export interface IUserActivity extends UserActivityInterface {

}

// for static methods
export interface UserActivityModel extends Model<IUserActivity> {
  paginate(filter: object, options: object): any
}