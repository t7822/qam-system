import {Button, Tooltip, Modal, Card, Table, Tag, notification, Pagination, Spin} from "antd";
import {DeleteOutlined, EyeOutlined, ArrowLeftOutlined} from "@ant-design/icons";
import React, {useEffect, useState} from "react";
import ManageSystem from "../../index";
import {PageHeaderAlt} from "common/components/layout-components/PageHeaderAlt";
import Flex from "common/components/shared-components/Flex";
import {useRouter} from "next/router";
import {withSessionSsr} from "server/utils/with-session";
import {auth} from "server/utils/auth";
import {SessionData} from "server/interfaces/types";
import {getIdeaByFilter} from "server/services/idea.service";
import {queryComments} from "server/services/comment.service";
import moment from "moment";
import {ApiService} from "common/services/ApiService";

const {confirm} = Modal;

const CommentList = (props: any) => {
  const {idea} = props;
  const router = useRouter()
  const [comments, setComments] = useState(props.comments || []);
  const [pageInfo, setPageInfo] = useState(props.pageInfo || {});
  const [page, setPage] = useState(1);
  const [loading, setLoading] = useState(false)

  useEffect(() => {
    (async () => {
      await getComments(page, 10)
    })()
  }, [page])

  const getComments = async (currentPage: any, limit: any) => {
    setLoading(true)
    try {
      const res = await ApiService.getComments({
        category: idea._id,
        page: currentPage,
        limit: limit,
        sortBy: "-createdAt"
      });
      setComments(res?.results)
      setPageInfo({
        totalResults: res?.totalResults,
        hasNextPage: res?.page < res?.totalPages
      });
      setLoading(false)
    } catch (err) {
      notification.error({
        message: err ?? "Something errored"
      })
      setLoading(false)
    }
  }

  const columns: any = [
    {
      title: "Content",
      align: "center",
      dataIndex: "content",
      key: "content",
      width: 450,
      ellipsis: true
}, {
      title: "Author",
      align: "center",
      dataIndex: "author",
      key: "author",
      render: (_: any, record: any) => (
        <Tag>{record.author.fullName}</Tag>
      )
    }, {
      title: "Created At",
      align: "center",
      dataIndex: "createdAt",
      key: "createdAt",
      render: (record: any) => {
        return (
          <span>{moment(record).format("DD/MM/YYYY")}</span>
        )
      }
    }, {
      title: "Privacy",
      align: "center",
      dataIndex: "privacy",
      key: "privacy",
    }, {
      title: "Action",
      dataIndex: "action",
      align: "center",
      key: "action",
      render: (_: any, record: any) => (
        <div className="text-right d-flex justify-content-center">
          <Tooltip title="View">
            <Button type="primary" className="mr-2" icon={<EyeOutlined/>} size="small"/>
          </Tooltip>
          <Tooltip title="Delete">
            <Button type="default" danger icon={<DeleteOutlined/>}
                    onClick={() => showDeleteConfirm(record._id)}
                    size="small"/>
          </Tooltip>
        </div>
      )
    }
  ];

  const showDeleteConfirm = (id: any) => {
    return (
      confirm({
        title: 'Are you sure delete this comment?',
        content: 'This action can not undo, so do you want to delete?',
        okText: 'Yes',
        okType: 'danger',
        cancelText: 'No',
        onOk: async () => {
          try {
            const res = await ApiService.deleteComment(id, {model: "Comment"});
            if (res.status) {
              return notification.error({
                message: res.data.message
              })
            }
            const newLists = [...comments].filter(comment => comment._id !== res.comment._id);
            setComments(newLists);
            notification.success({
              message: res.message
            });
          } catch (err) {
            notification.error({message: err ? err : "Some error"})
          }
        },

      })
    )
  }

  return (
    <ManageSystem>
      <PageHeaderAlt className="border-bottom" navType={undefined}>
        <Flex className="py-2" mobileFlex={false} justifyContent="between" alignItems="center">
          <div className="nav-close d-flex align-items-center mb-3">
            <div className="font-size-md" onClick={async () => {
              await router.replace("/manage-system/manage-ideas");
            }}>
              <ArrowLeftOutlined className="mail-detail-action-icon font-size-md ml-0"/>
            </div>
            <h2 className="mt-2" style={{
              width: "100%",
              whiteSpace: 'nowrap',
              overflow: 'hidden',
              textOverflow: 'ellipsis'
            }}>Comments In {idea.title}</h2>
          </div>
        </Flex>
      </PageHeaderAlt>
      <Card>
        <div className="table-responsive">
          <Spin spinning={loading} tip={"Loading...."}>
            <Table
              rowKey={(record) => record._id}
              columns={columns}
              scroll={{x: 1000}}
              dataSource={comments}
              pagination={false}
              footer={() => {
                return (
                  <Pagination
                    showQuickJumper
                    current={page}
                    defaultCurrent={1}
                    total={pageInfo.totalResults}
                    onChange={(page) => {
                      setPage(page);
                    }}
                  />
                )
              }}
            />
          </Spin>
        </div>
      </Card>
    </ManageSystem>
  )
}

export const getServerSideProps = withSessionSsr(
  async function getServerSideProps(context): Promise<any> {
    const {req, res} = context;
    const {slug} = context.query;
    try {
      const user = await auth(req.session as SessionData, ["MANAGE_ALL_COMMENT"]);
      const idea = await getIdeaByFilter({slug: slug, deleted: {$ne: true}}, user._id);
      const comments = await queryComments({category: idea._id, level: 1}, {
        page: 1,
        limit: 10,
        sortBy: "-createdAt"
      }, user);

      return {
        props: {
          idea: JSON.parse(JSON.stringify(idea)),
          comments: JSON.parse(JSON.stringify(comments?.results)),
          pageInfo: {
            totalResults: comments?.totalResults,
            hasNextPage: comments?.page < comments?.totalPages
          }
        }
      }
    } catch (err) {
      console.log('err', err);
      res.statusCode = err["statusCode"] || 500;
      return {
        redirect: {
          permanent: false,
          destination: '/',
        },
      }
    }
  }
)

export default CommentList
