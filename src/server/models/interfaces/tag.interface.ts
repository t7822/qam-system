import {Document, Model} from 'mongoose';

export interface TagInterface extends Document {
  title: string,
  slug: string,
}

// for methods
export interface ITag extends TagInterface {

}

// for static methods
export interface TagModel extends Model<ITag> {
  paginate(filter: object, options: object): any,
  slugGenerator(tagTitle: string): Promise<string>,
}