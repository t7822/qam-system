import Joi from 'joi';
import { ObjectId } from "server/validations/custom.validations";

export const createDepartment: { body: object } = {
  body: Joi.object().keys({
    name: Joi.string().required(),
    manager: Joi.string().custom(ObjectId)
  }),
};

export const restoreDepartments: { body: object } = {
  body: Joi.object().keys({
    departments: Joi.array().items(Joi.string().custom(ObjectId)),
  }),
};

export const getDepartments: { query: object } = {
  query: Joi.object().keys({
    slug: Joi.string(),
    name: Joi.string(),
    manager: Joi.string(),
    sortBy: Joi.string(),
    limit: Joi.number().integer(),
    page: Joi.number().integer(),
  }),
};

export const getDepartment: { params: object } = {
  params: Joi.object().keys({
    slug: Joi.string(),
    manager: Joi.string(),
  }),
};

export const updateDepartment: { params: object, body: object } = {
  params: Joi.object().keys({
    slug: Joi.string(),
  }),
  body: Joi.object()
    .keys({
      name: Joi.string().required(),
      manager: Joi.string().custom(ObjectId)
    })
    .min(1),
};

export const deleteDepartment: { params: object } = {
  params: Joi.object().keys({
    slug: Joi.string(),
  }),
};

export const deleteDepartmentsPermanently: { body: object } = {
  body: Joi.object().keys({
    departments: Joi.array().items(Joi.string().custom(ObjectId)),
  }),
};