import ForumLayout from "common/layouts/forum-layout";
import ForumHeader from "common/components/forum-components/forum-header";
import {Typography, Row, Col, Input, Card, List, Tag, Pagination, Select, Spin, Button} from "antd";
import {
  SearchOutlined,
  PlayCircleFilled, PlusCircleOutlined
} from '@ant-design/icons';
import Link from "next/link"
import {withSessionSsr} from "server/utils/with-session";
import {auth} from "server/utils/auth";
import {SessionData} from "server/interfaces/types";
import {queryTopics} from "server/services/topic.service";
import {useEffect, useState} from "react";
import {ApiService} from "common/services/ApiService";
import {queryIdeas} from "server/services/idea.service";
import {queryDepartments} from "server/services/department.service";
import {useSelector} from "react-redux";
import {RootState} from "../common/redux/store";


const Home = (props: any) => {
  const {user} = useSelector((state: RootState) => state.auth)
  const {recentIdeas, recentTopics} = props;
  const [departments, setDepartments] = useState(props.departments || []);
  const [pageInfo, setPageInfo] = useState(props.pageInfo || {});
  const [loading, setLoading] = useState(false);
  const [content, setContent] = useState("");
  const [page, setPage] = useState(1);

  const getDepartments = async () => {
    setLoading(true)
    if (content !== "") {
      const res = await ApiService.getDepartments({
        name: content,
        page: page,
        limit: 5,
        sortBy: "-createdAt"
      });
      setDepartments(res.results)
      setPageInfo({
        totalResults: res.totalResults,
        hasNextPage: res?.page < res?.totalPages
      })
    } else {
      const res = await ApiService.getDepartments({
        page: page,
        limit: 5,
        sortBy: "-createdAt"
      });
      setDepartments(res.results)
      setPageInfo({
        totalResults: res.totalResults,
        hasNextPage: res?.page < res?.totalPages
      })
    }
    setLoading(false)
  }

  useEffect(() => {
    getDepartments().then((_: any) => {
    })
  }, [content, page])

  return (
    <ForumLayout>
      <ForumHeader/>
      <Row gutter={16}>
        <Col lg={8} md={14} sm={24} xs={24} className={"mb-2"}>
          <Input.Search onSearch={(value) => {
            setContent(value);
            setPage(1);
          }} prefix={<SearchOutlined/>} allowClear size={"small"}
                        placeholder={"Search for department..."}/>
        </Col>
        <Col md={0} xs={16} className={'mt-2'}>
          <h5>Department</h5>
        </Col>
        {
          user.role.name === "staff" && (
            <Col lg={16} md={10} sm={8} xs={8}
                 style={{display: "flex", justifyContent: "flex-end"}}
                 className="mb-2"
            >
              <Button href="/idea/new" icon={<PlusCircleOutlined/>} size={"small"} type={"primary"}>
                New ideas
              </Button>
            </Col>
          )
        }
      </Row>

      <Row gutter={32}>
        <Col xs={24} md={18}>
          <Spin spinning={loading} tip={"Loading...."}>
            <List
              size="large"
              split={false}
              dataSource={departments}
              renderItem={(item: any) => <Card bordered={false}>
                <div className={"forum-list-item"}>
                  <div className={"item-main-content"}>
                    <div className={"list-item-topic"}>
                      <div className={"list-item-topic-outer"}>
                        💬
                      </div>
                    </div>
                    <div className={"list-item-meta"}>
                      <Link href={`/department/${item.slug}`}>
                        <a className={"title"}>
                          <strong>
                            <Typography.Text>
                              {item.name}
                            </Typography.Text>
                          </strong>
                        </a>
                      </Link>
                      <div className={"description"}>
                        Department
                      </div>
                    </div>
                  </div>
                </div>
              </Card>}
            />
          </Spin>
          {
            departments?.length > 0 && (
              <Pagination
                current={page}
                defaultCurrent={1}
                total={pageInfo.totalResults}
                pageSize={5}
                onChange={(page) => setPage(page)}
              />
            )
          }
        </Col>
        <Col md={6} xs={0}>
          <Card
            title={
              <div style={{textAlign: "center"}}>Recent Campaigns</div>
            }
            size="small"
            bordered={false}
          >
            {
              recentTopics?.map((topic: any) => {
                return (
                  <div key={topic.slug} className="ml-4">
                    <Link href={`/topic/${topic.slug}`}>
                      <a>
                        <p style={{fontWeight: "bold"}}><PlayCircleFilled/> {topic.title}</p>
                      </a>
                    </Link>
                  </div>
                )
              })
            }
          </Card>
          <Card
            className={"mt-2"}
            size="small"
            title={
              <div style={{textAlign: "center"}}>Recent Ideas</div>
            }
            bordered={false}>
            {
              recentIdeas?.map((idea: any) => {
                return (
                  <div key={idea.slug} className="ml-4">
                    <Link href={`/idea/${idea.slug}`}>
                      <a>
                        <p style={{fontWeight: "bold"}}><PlayCircleFilled/> {idea.title}</p>
                      </a>
                    </Link>
                  </div>
                )
              })
            }
          </Card>
        </Col>
      </Row>
    </ForumLayout>
  )
}

export const getServerSideProps = withSessionSsr(
  async function getServerSideProps({req, res}): Promise<any> {
    try {
      const user = await auth(req.session as SessionData, []);
      const departments = await queryDepartments({deleted: {$ne: true}}, {page: 1, limit: 5, sortBy: "-createdAt"});
      const recentIdeas = await queryIdeas({deleted: {$ne: true}}, {page: 1, limit: 3, sortBy: "-createdAt"}, user);
      const recentTopics = await queryTopics({deleted: {$ne: true}}, {page: 1, limit: 3, sortBy: "-createdAt"});
      return {
        props: {
          departments: JSON.parse(JSON.stringify(departments?.results)),
          recentIdeas: JSON.parse(JSON.stringify(recentIdeas?.results)),
          recentTopics: JSON.parse(JSON.stringify(recentTopics?.results)),
          pageInfo: {
            totalResults: departments?.totalResults,
            hasNextPage: departments?.page < departments?.totalPages
          },
        }
      }
    } catch (err) {
      console.log("error", err);
      res.statusCode = err["statusCode"] || 500;
      return {
        redirect: {
          permanent: false,
          destination: (err.statusCode === 401) ? '/auth/login' : '/',
        },
      }
    }
  }
);

export default Home;
