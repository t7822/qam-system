import {Document, Model, Schema} from "mongoose";
import {IUser} from "./user.interface";
import {IDepartment} from "./department.interface";

export interface TopicInterface extends Document {
  author: IUser["_id"],
  department: IDepartment["_id"],
  firstClosureDate: number,
  finalClosureDate: number,
  title: string,
  slug: string,
  deleted: boolean
}

export interface ITopic extends TopicInterface {
  isFieldTaken(filter: any, excludedId: Schema.Types.ObjectId): Promise<boolean>
}

// for static methods
export interface TopicModel extends Model<ITopic> {
  paginate(filter: object, options: object): any
}