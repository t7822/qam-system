import {Document, Model} from 'mongoose';
import {IUser} from "./user.interface";
import {IIdea} from "./idea.interface";

export interface ViewInterface extends Document {
  user: IUser["_id"],
  idea: IIdea["_id"],
  viewedAt: string,
}

export interface IView extends ViewInterface {
  // for methods
}

export interface ViewModel extends Model<IView> {
  // for static functions
}