import httpStatus from 'http-status';
import {View, Idea, Comment, User, Topic} from 'server/models';
import ApiError from 'server/utils/api-error';
import { getIdeasByFilter } from 'server/services/idea.service';
import {Schema, PipelineStage} from "mongoose";

const dayNames = ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday'];

const getTimes = (datetime: Date) => {
  const startYear: number = (new Date(datetime.getFullYear(), 0, 1, 0, 0, 0)).getTime();
  const endYear: number = (new Date(datetime.getFullYear(), 11, 31, 23, 59, 59)).getTime();
  const startMonth: number = (new Date(datetime.getFullYear(), datetime.getMonth(), 1, 0, 0, 0)).getTime();
  const endMonth: number = (new Date(datetime.getFullYear(), datetime.getMonth() + 1, 0, 23, 59, 59)).getTime();
  const temp = new Date(datetime.getFullYear(), datetime.getMonth(), datetime.getDate(), 0, 0, 0);
  const firstDayWeek = temp.getDate() - temp.getDay();
  const lastDayWeek = firstDayWeek + 6;
  const startWeek: number = new Date(temp).setDate(firstDayWeek);
  const endWeek: number = (new Date(temp.getFullYear(), temp.getMonth(), lastDayWeek, 23, 59, 59)).getTime();
  const startDay: number = (new Date(datetime.getFullYear(), datetime.getMonth(), datetime.getDate(), 0, 0, 0)).getTime();
  const endDay: number = (new Date(datetime.getFullYear(), datetime.getMonth(), datetime.getDate(), 23, 59, 59)).getTime();

  return {
    startYear,
    endYear,
    startMonth,
    endMonth,
    startWeek,
    endWeek,
    startDay,
    endDay,
  }
}

export const ideaViews = async (filter: any) => {
  if (filter.user) {
    const user = await User.findOne({username: filter.user});
    if (!user) throw new ApiError(httpStatus.NOT_FOUND, `Cannot find user with username ${filter.user}`);
    filter.user = user._id;
  }
  if (filter.idea) {
    const idea = await Idea.findOne({slug: filter.idea});
    if (!idea) throw new ApiError(httpStatus.NOT_FOUND, `Cannot find idea with title ${filter.idea}`);
    filter.idea = idea._id;
  }
  const result: any = {};
  const datetime: Date = filter.date ? new Date(+filter.date) : new Date();
  const views = await View.find({}).lean();
  result.total = { views: views.length };
  const { startYear, endYear, startMonth, endMonth, startWeek, endWeek, startDay, endDay } = getTimes(datetime);
  const yearViews = views.filter(v => parseInt(v.viewedAt) >= startYear && parseInt(v.viewedAt) <= endYear);
  result.year = { views: yearViews.length };
  
  const monthViews = yearViews.filter(v => parseInt(v.viewedAt) >= startMonth && parseInt(v.viewedAt) <= endMonth);
  const weekViews = views.filter(v => parseInt(v.viewedAt) >= startWeek && parseInt(v.viewedAt) <= endWeek);
  const dayViews = weekViews.filter(v => parseInt(v.viewedAt) >= startDay && parseInt(v.viewedAt) <= endDay);
  let currDay = datetime.getDay();
  Object.assign(result, {
    month: {
      views: monthViews.length,
    },
    week: {
      views: weekViews.length,
      days: []
    },
    day: {
      views: dayViews.length,
    }
  })
  const temp = new Date(datetime.getFullYear(), datetime.getMonth(), datetime.getDate(), 0, 0, 0);
  for (let i = 0; i <= 6; i++) {
    const day = {
      name: dayNames[i],
      views: 0
    }
    if (i == currDay) {
      day.views = dayViews.length;
    } else {
      let startWeekDay: number = (new Date(temp)).setDate(i - currDay + temp.getDate());
      let endWeekDay: number = (new Date(temp.getFullYear(), temp.getMonth(), (i - currDay + temp.getDate()), 23, 59, 59)).getTime();
      const weekDayViews = weekViews.filter(v => parseInt(v.viewedAt) >= startWeekDay && parseInt(v.viewedAt) <= endWeekDay);
      day.views = weekDayViews.length;
    }
    result.week.days.push(day);
  }
  return result;
};

export const topicIdeas = async (filter: any) => {
  if (filter.author) {
    const user = await User.findOne({username: filter.author});
    if (!user) throw new ApiError(httpStatus.NOT_FOUND, `Cannot find user with username ${filter.author}`);
    filter.author = user._id;
  }
  if (filter.topic) {
    const topic = await Topic.findOne({slug: filter.topic});
    if (!topic) throw new ApiError(httpStatus.NOT_FOUND, `Cannot find idea with title ${filter.topic}`);
    filter.topic = topic._id;
  }
  const datetime: Date = filter.date ? new Date(+filter.date) : new Date();
  const ideas: any[] = await Idea.find({}).lean();
  const { startYear, endYear, startMonth, endMonth, startWeek, endWeek, startDay, endDay } = getTimes(datetime);
  const yearCreated = ideas.filter(v => parseInt(v.createdAt) >= startYear && parseInt(v.createdAt) <= endYear);
  const monthCreated = yearCreated.filter(v => parseInt(v.createdAt) >= startMonth && parseInt(v.createdAt) <= endMonth);
  const weekCreated = yearCreated.filter(v => parseInt(v.createdAt) >= startWeek && parseInt(v.createdAt) <= endWeek);
  const dayCreated = weekCreated.filter(v => parseInt(v.createdAt) >= startDay && parseInt(v.createdAt) <= endDay);
  const temp = new Date(datetime.getFullYear(), datetime.getMonth(), datetime.getDate(), 0, 0, 0);
  let currDay = datetime.getDay();
  const result = {
    total: {
      created: ideas.length,
      authors: [...new Set(ideas.map(i => i.author.toString()))].length,
    },
    year: {
      created: yearCreated.length,
      authors: [...new Set(yearCreated.map(i => i.author.toString()))].length,
    },
    month: {
      created: monthCreated.length,
      authors: [...new Set(monthCreated.map(i => i.author.toString()))].length,
    },
    week: {
      created: weekCreated.length,
      authors: [...new Set(weekCreated.map(i => i.author.toString()))].length,
      days: [],
    },
    day: {
      created: dayCreated.length,
      authors: [...new Set(dayCreated.map(i => i.author.toString()))].length,
    }
  }
  for (let i = 0; i <= 6; i++) {
    const day = {
      name: dayNames[i],
      created: 0,
      authors: 0,
    }
    if (i == currDay) {
      day.created = result.day.created;
      day.authors = result.day.authors;
    } else {
      let startWeekDay: number = (new Date(temp)).setDate(i - currDay + temp.getDate());
      let endWeekDay: number = (new Date(temp.getFullYear(), temp.getMonth(), (i - currDay + temp.getDate()), 23, 59, 59)).getTime();
      const weekDayCreated = weekCreated.filter(v => parseInt(v.createdAt) >= startWeekDay && parseInt(v.createdAt) <= endWeekDay);
      day.created = weekDayCreated.length;
      day.authors = [...new Set(weekDayCreated.map(i => i.author.toString()))].length;
    }
    result.week.days.push(day);
  }
  return result;
};

export const topViewsIdeas = async (filter: any, options: any, userId: Schema.Types.ObjectId, hasPermission: boolean) => {
  if (filter.author) {
    const user = await User.findOne({username: filter.author});
    if (!user) throw new ApiError(httpStatus.NOT_FOUND, `Cannot find user with username ${filter.author}`);
    filter.author = user._id;
  }
  if (filter.topic) {
    const topic = await Topic.findOne({slug: filter.topic});
    if (!topic) throw new ApiError(httpStatus.NOT_FOUND, `Cannot find idea with title ${filter.topic}`);
    filter.topic = topic._id;
  }
  const datetime: Date = filter.date ? new Date(+filter.date) : new Date();
  const ideas = await getIdeasByFilter(filter, {
    sort: { viewCount: -1 }
  }, userId, hasPermission);
  const { startYear, endYear, startMonth, endMonth, startWeek, endWeek, startDay, endDay } = getTimes(datetime);
  const total = [...ideas];
  const limit = options.limit ? +options.limit : 5;
  if (total.length > limit) total.length = limit;
  const topYear = ideas.filter(i => i.createdAt >= startYear && i.createdAt <= endYear);
  const topMonth = topYear.filter(i => i.createdAt >= startMonth && i.createdAt <= endMonth);
  const topWeek = ideas.filter(i => i.createdAt >= startWeek && i.createdAt <= endWeek);
  const topDay = ideas.filter(i => i.createdAt >= startDay && i.createdAt <= endDay);
  const result = {
    total: {
      ideas: total
    },
    year: {
      ideas: topYear
    },
    month: {
      ideas: topMonth
    },
    week: {
      ideas: topWeek
    },
    day: {
      ideas: topDay
    }
  }
  return result;
}

export const topTopicIdeas = async (filter: any, options: any) => {
  const pipeline: Array<PipelineStage> = [
    {
      $match: {
        ...filter
      }
    },
    {
      $lookup: {
        from: 'users',
        localField: 'author',
        foreignField: '_id',
        as: 'author',
      }
    },
    {
      $unwind: "$author"
    },
    {
      $lookup: {
        from: 'ideas',
        localField: '_id',
        foreignField: 'topic',
        as: 'ideas',
      }
    },
    {
      $project: {
        "author._id": 1,
        "author.username": 1,
        "author.fullName": 1,
        "author.displayName": 1,
        "author.avatar": 1,
        "title": 1,
        "slug": 1,
        "firstClosureDate": 1,
        "finalClosureDate": 1,
        "createdAt": 1,
        "ideas._id": 1,
        "ideas.title": 1,
        "ideas.slug": 1,
        "ideaCount": {$size: "$ideas"},
      }
    },
    {
      $group: {
        _id: "$_id",
        title: {$first: "$title"},
        author: {$first: "$author"},
        slug: {$first: "$slug"},
        firstClosureDate: {$first: "$firstClosureDate"},
        finalClosureDate: {$first: "$finalClosureDate"},
        ideaCount: {$first: "$ideaCount"},
        createdAt: {$first: "$createdAt"},
      }
    },
    {
      $sort: {ideaCount: -1}
    },
    {
      $skip: options.skip ?? 0
    },
    {
      $limit: options.limit ?? 5
    }
  ];
  const topics = await Topic.aggregate(pipeline);
  return topics;
}