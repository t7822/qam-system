import Joi from 'joi';
import {ObjectId} from "server/validations/custom.validations";

export const createComment: {body: object} = {
  body: Joi.object().keys({
    content: Joi.string().required(),
    replyFor: Joi.string().custom(ObjectId),
    level: Joi.number(),
    privacy: Joi.string(),
    tag: Joi.string().custom(ObjectId),
    category: Joi.string().custom(ObjectId).required(),
    model: Joi.string(),
  }),
};

export const getComments: {query: object} = {
  query: Joi.object().keys({
    category: Joi.string().custom(ObjectId),
    author: Joi.string().custom(ObjectId),
    replyFor: Joi.string().custom(ObjectId),
    level: Joi.number().integer(),
    sortBy: Joi.string(),
    limit: Joi.number().integer(),
    page: Joi.number().integer(),
  }),
};

export const getComment: {params: object} = {
  params: Joi.object().keys({
    commentId: Joi.string().custom(ObjectId),
  }),
};

export const updateComment: {params: object, body: object} = {
  params: Joi.object().keys({
    commentId: Joi.required().custom(ObjectId),
  }),
  body: Joi.object()
    .keys({
      content: Joi.string().required(),
      model: Joi.string(),
    })
    .min(1),
};

export const deleteComment: {params: object, body: object} = {
  params: Joi.object().keys({
    commentId: Joi.string().custom(ObjectId),
  }),
  body: Joi.object().keys({
    model: Joi.string()
  })
};

export const restoreComments: {body: object} = {
  body: Joi.object().keys({
    comments: Joi.array().items(Joi.string().custom(ObjectId)),
  }),
};

export const deleteCommentsPermanently: {body: object} = {
  body: Joi.object().keys({
    comments: Joi.array().items(Joi.string().custom(ObjectId)),
  }),
};
