import React from 'react'
import { Editor } from '@toast-ui/react-editor';
import '@toast-ui/editor/dist/toastui-editor.css';
import PropTypes from "prop-types";

const WrappedEditor = (props: any) => {
  const { forwardedRef } = props;

  return <Editor
    {...props}
    ref={forwardedRef}
    useCommandShortcut={true}
    height={props.height ? props.height : "200px"}
    toolbarItems={[
      ['heading', 'bold', 'italic', 'strike'],
      ['hr', 'quote'],
      ['ul', 'ol', 'task', 'indent', 'outdent'],
      ['table', 'image', 'link'],
      ['code', 'codeblock'],
    ]}
    initialEditType="wysiwyg"
    usageStatistics={false}
  />
}

WrappedEditor.propTypes = {
  props: PropTypes.object,
  forwardedRef: PropTypes.shape({
    current: PropTypes.instanceOf(Element)
  }).isRequired
}

export default WrappedEditor;