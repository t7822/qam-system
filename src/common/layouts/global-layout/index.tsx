import React, {useEffect} from "react";
import {ApiService} from "../../services/ApiService";
import {useDispatch} from "react-redux";
import {setSessionData} from "../../redux/slices/auth.slice";

const GlobalLayout = ({children}: { children: any }) => {
  const dispatch = useDispatch();
  useEffect(() => {
    ApiService.getSession().then(async sessionData => {
      dispatch(setSessionData(sessionData))
    });
  }, []);

  return (
    <>
      {children}
    </>
  )
};

export default GlobalLayout;
