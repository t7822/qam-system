import {userActivityService} from 'server/services';
import {ExtendedRequest, ExtendedResponse} from "server/interfaces/types";
import pick from "server/utils/pick";

export const addUserActivity = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const userActivity = await userActivityService.createUserActivity(req.body);
  res.json({
    message: "Created activity successfully",
    userActivity
  });
};

export const getUserActivities = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const filter = pick(req.query, [""]);
  const options = pick(req.query, ['sortBy', 'limit', 'page']);
  const result = await userActivityService.queryUserActivities(req.session.user, filter, options);
  res.json(result);
};

export const deleteUserActivity = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const userActivity = await userActivityService.deleteUserActivity({_id: req.query.activityId});
  res.json({
    message: "Deleted userActivity successfully",
    userActivity
  });
};
