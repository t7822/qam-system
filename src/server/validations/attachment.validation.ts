import Joi from 'joi';
import { ObjectId } from "server/validations/custom.validations";

export const addAttachments: { body: object } = {
  body: Joi.object().keys({
    idea: Joi.string().required()
  }),
};

export const removeAttachment: { body: object } = {
  body: Joi.object().keys({
    attachment: Joi.string().custom(ObjectId),
  }),
};

export const removeAllAttachments: { body: object } = {
  body: Joi.object().keys({
    idea: Joi.string().required(),
  }),
};