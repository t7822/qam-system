import {Schema, isValidObjectId, model, models} from 'mongoose';
import validator from 'validator';
import bcrypt from 'bcryptjs';
import {toJSON, paginate} from 'server/models/plugins';
import Role from "server/models/role.model";
import httpStatus from "http-status";
import ApiError from "server/utils/api-error";
import defaultAvatar from "server/config/upload.config"
import {IUser, UserModel} from "server/models/interfaces/user.interface";

const userSchema = new Schema<IUser>({
  username: {
    type: String,
    required: true,
    lowercase: true,
    trim: true,
  },
  id: {
    type: Number
  },
  email: {
    type: String,
    required: true,
    trim: true,
    lowercase: true,
    validate(value: string) {
      if (!validator.isEmail(value)) {
        throw new Error('Invalid email');
      }
    },
  },
  password: {
    type: String,
    required: true,
    trim: true,
    minlength: 8,
    validate(value: string) {
      if (!value.match(/\d/) || !value.match(/[a-zA-Z]/)) {
        throw new Error('Password must contain at least one letter and one number');
      }
    },
    private: true, // used by the toJSON plugin
  },
  role: {
    type: Schema.Types.ObjectId,
    ref: "Role"
  },
  department: {
    type: Schema.Types.ObjectId,
    ref: "Department"
  },
  dob: {
    type: String,
  },
  gender: {
    type: String,
  },
  fullName: {
    type: String,
  },
  address: {
    type: String,
  },
  contact: {
    type: String,
  },
  avatar: {
    type: String,
    default: defaultAvatar.defaultData.url
  },
  deleted: {
    type: Boolean,
    default: false,
    private: true
  },
}, {
  collection: "users",
  timestamps: true,
  toJSON: {virtuals: true},
  toObject: {virtuals: true},
});

userSchema.virtual("activities", {
  ref: "UserActivity",
  localField: "_id",
  foreignField: "user"
});

// add plugin that converts mongoose to json
userSchema.plugin(toJSON);
userSchema.plugin(paginate);

userSchema.statics.isFieldTaken = async function (filter: object, excludeUserId: Schema.Types.ObjectId): Promise<boolean> {
  const user = await this.findOne({...filter, _id: {$ne: excludeUserId}});
  return !!user;
};

userSchema.statics.handleRole = async function (data: any): Promise<any> {
  const role = await Role.findOne((typeof data === "object") ? data : (isValidObjectId(data) ? {_id: data} : {$or: [{slug: data}, {name: data}]}));
  if (!role) throw new ApiError(httpStatus.NOT_FOUND, "Role not found");
  return role._id;
};

userSchema.methods.isPasswordMatch = async function (password: string): Promise<boolean> {
  const user = this;
  return bcrypt.compare(password, user.password);
};

userSchema.pre('save', async function (next) {
  const user = this;
  if (!user.id) {
    const checkUser = await User.findOne({}).sort({id: -1});
    let padding = Math.floor(Math.random() * 6) + 1;
    let defaultID = 10000000;
    user.id = (checkUser ? (checkUser.id || defaultID) : defaultID) + padding;
  }

  if (user.isModified('password')) {
    user.password = await bcrypt.hash(user.password, 8);
  }

  if (!user.fullName) {
    user.fullName = this.username;
  }

  if (!user.role) {
    // @ts-ignore
    user.role = (await Role.findOne({name: "user"}).lean())._id;
  }
  next();
});

const User = models.User as UserModel || model<IUser, UserModel>('User', userSchema);

export default User;
