import '@toast-ui/editor/dist/toastui-editor.css';
import 'common/styles/globals.scss';
import 'antd-css-utilities/utility.min.css';
import {Provider} from 'react-redux';
import {store} from 'common/redux/store';
import Head from "next/head";
import AppLayout from "common/layouts/app-layout";
import AuthLayout from "common/layouts/auth-layout";
import {useRouter} from "next/router";
import GlobalLayout from "../common/layouts/global-layout";

function MyApp({Component, pageProps}: any) {
  const router = useRouter();
  let Layout : any = AppLayout;
  if (router.pathname.startsWith("/auth/")) {
    Layout = AuthLayout;
  }

  return (
    <div className="App">
      <Head>
        <title>idde :: Share your idea</title>
      </Head>
      <Provider store={store}>
        <GlobalLayout>
          <Layout>
            <Component {...pageProps} />
          </Layout>
        </GlobalLayout>
      </Provider>
    </div>
  );
}

export default MyApp
