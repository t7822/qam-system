import {Document, Model} from 'mongoose';
import {IUser} from "./user.interface";
import {IIdea} from "./idea.interface";
import {IComment} from "./comment.interface";

export interface ReactionInterface extends Document {
  user: IUser["_id"],
  target: IIdea["_id"] | IComment["_id"],
  type: string,
  active: boolean,
  activatedAt: number,
}

export interface IReaction extends ReactionInterface {
  // for methods
}

export interface ReactionModel extends Model<IReaction> {
  // for static functions
}