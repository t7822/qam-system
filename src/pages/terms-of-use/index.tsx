import {Card, Typography, Col, Row} from "antd";
import ForumLayout from "common/layouts/forum-layout";
import {terms} from "common/constants/TermConfig";

const Terms = () => {
  return (
    <ForumLayout>
      <Row>
        <Col className={"container"}>
          <div className={"d-flex justify-content-center"}>
            <Card>
              <h1 className={"d-flex justify-content-center"}>Terms and Policies</h1>
              <p>
                <Typography.Text>
                  Q&A system system system system system is a vast network of communities that are created, run, and
                  populated by you, the Q&A system system system system system
                  users.
                  Through these communities, you can post, comment, vote, discuss, learn, debate, support, and connect
                  with
                  people who share your interests, and we encourage you to find—or even create—your home on Q&A system
                  system system system system.
                </Typography.Text>
              </p>
              <p>
                <Typography.Text>
                  While not every community may be for you (and you may find some unrelatable or even offensive), no
                  community should be used as a weapon. Communities should create a sense of belonging for their
                  members,
                  not try to diminish it for others. Likewise, everyone on Q&A system system system system system should
                  have an expectation of
                  privacy
                  and
                  safety, so please respect the privacy and safety of others.
                </Typography.Text>
              </p>
              <p>
                <Typography.Text>
                  Every community on Q&A system is defined by its users. Some of these users help manage the community
                  as
                  moderators. The culture of each community is shaped explicitly, by the community rules enforced by
                  moderators, and implicitly, by the upvotes, downvotes, and discussions of its community members.
                  Please
                  abide by the rules of communities in which you participate and do not interfere with those in which
                  you
                  are not a member.

                  Below the rules governing each community are the platform-wide rules that apply to everyone on Q&A
                  system.
                  These rules are enforced by us, the admins.
                </Typography.Text>
              </p>
              <h1 className={"d-flex justify-content-center mt-4"}>
                Rules
              </h1>
              {
                terms.map((term: any) => {
                  return (
                    <div key={term.rule}>
                      <h3>Rule {term.rule}</h3>
                      <p>
                        <Typography.Text>
                          {term.content}
                        </Typography.Text>
                      </p>
                    </div>
                  )
                })
              }
            </Card>
          </div>
        </Col>
      </Row>
    </ForumLayout>
  )
}

export default Terms