import {
  Button,
  Card, Col,
  Input,
  Modal,
  notification,
  Pagination, Row,
  Select,
  Spin,
  Table,
  Tag,
  Tooltip,
} from "antd";
import {
  CommentOutlined,
  DeleteOutlined,
  EyeOutlined,
} from "@ant-design/icons";
import React, {useEffect, useState} from "react";
import ManageSystem from "../index";
import {useRouter} from "next/router";
import {withSessionSsr} from "server/utils/with-session";
import {auth} from "server/utils/auth";
import {SessionData} from "server/interfaces/types";
import {queryIdeas} from "server/services/idea.service";
import moment from "moment";
import {ApiService} from "common/services/ApiService";

const {confirm} = Modal;

const ManageDiscussion = (props: any) => {
  const router = useRouter();
  const [ideas, setIdeas] = useState(props.ideas || []);
  const [filter, setFilter] = useState("title");
  const [pageInfo, setPageInfo] = useState(props.pageInfo || {});
  const [loading, setLoading] = useState(false);
  const [page, setPage] = useState(1);
  const [value, setValue] = useState("");

  const getIdeas = async () => {
    setLoading(true)
    let params = {
      page: page,
      limit: 10,
      sortBy: "-createdAt",
    }
    if (value !== "" && filter === "title") {
      params = Object.assign({title: value})
    }
    if (value !== "" && filter === "slug") {
      params = Object.assign({slug: value})
    }
    if (value !== "" && filter === "tag") {
      params = Object.assign({tag: value})
    }
    if (value !== "" && filter === "topic") {
      params = Object.assign({topic: value})
    }
    const res = await ApiService.getIdeas(params);
    setIdeas(res.results)
    setPageInfo({
      totalResults: res.totalResults,
      hasNextPage: res?.page < res?.totalPages
    })
    setLoading(false)
  }

  useEffect(() => {
    getIdeas().then((_: any) => {
    })
  }, [value, page])

  const getStatus = (open: any) => {
    const listTopics = ideas?.map((idea: any) => {
      return idea.topic.finalClosureDate < new Date().getTime();
    });
    if (open !== "") {
      return listTopics?.filter((value: any) => !value).length
    } else {
      return listTopics?.filter((value: any) => value).length
    }
  }

  const columns: any = [
    {
      title: "Title",
      align: "center",
      dataIndex: "title",
      key: "title",
    }, {
      title: "Author",
      align: "center",
      dataIndex: "author",
      key: "author",
      render: (record: any) => {
        return (
          <Tag>{record.fullName}</Tag>
        )
      }
    }, {
      title: "Created At",
      align: "center",
      dataIndex: "createdAt",
      key: "createdAt",
      render: (record: any) => {
        return (
          <span>{moment(record).format("DD/MM/YYYY")}</span>
        )
      }
    }, {
      title: "Campaign",
      align: "center",
      dataIndex: "topic",
      key: "topic",
      render: ((record: any) => {
        return (
          <Tag
            className="edit-tag"
            closable={false}
            style={{
              borderRadius: 50
            }}
          >
            {record.title}
          </Tag>
        )
      })
    }, {
      title: "Tags",
      align: "center",
      dataIndex: "tags",
      key: "tags",
      responsive: ['md'],
      render: ((record: any) => {
        return (
          record.map((tag: any) => {
            return (
              <Tag
                className="edit-tag mb-1"
                closable={false}
                key={tag._id}
                style={{
                  borderRadius: 50
                }}
              >
                {tag.title}
              </Tag>
            )
          })
        )
      })
    }, {
      title: "Status",
      align: "center",
      dataIndex: "topic",
      key: "status",
      responsive: ['md'],
      render: (record: any) => {
        return (
          <Tag className="edit-tag"
               closable={false}
               style={{
                 borderRadius: 50
               }}
               color={record.finalClosureDate < new Date().getTime() ? "#ed5e4e" : "#71eba1"}
          >
            {record.finalClosureDate < new Date().getTime() ? "CLOSE" : "OPEN"}
          </Tag>
        )
      }
    }, {
      title: "Action",
      dataIndex: "action",
      align: "center",
      key: "action",
      render: (_: any, record: any) => (
        <div className="text-right d-flex justify-content-center">
          <Tooltip title="View">
            <Button type="primary" className="mr-2" icon={<EyeOutlined/>} size="small" onClick={async () => {
              await router.push(`/idea/${record.slug}`)
            }}/>
          </Tooltip>
          <Tooltip title="View Comments">
            <Button type="default" className="mr-2" icon={<CommentOutlined/>} size="small" onClick={async () => {
              await router.push(`/manage-system/manage-ideas/comments/${record.slug}`);
            }}/>
          </Tooltip>
          <Tooltip title="Delete">
            <Button type="primary" danger icon={<DeleteOutlined/>} onClick={() => showDeleteConfirm(record.slug)}
                    size="small"/>
          </Tooltip>
        </div>
      )
    }
  ];

  const showDeleteConfirm = (slug: any) => {
    return (
      confirm({
        title: 'Are you sure delete this idea?',
        content: 'This action can not undo, so do you want to delete?',
        okText: 'Yes',
        okType: 'danger',
        cancelText: 'No',
        onOk: async () => {
          try {
            const res = await ApiService.deleteIdea(slug);
            if (res.status) {
              return notification.error({
                message: res.data.message
              })
            }
            const newLists = [...ideas].filter(idea => idea._id !== res.idea._id);
            setIdeas(newLists);
            notification.success({
              message: res.message
            });
          } catch (err) {
            notification.error({message: err ? err : "Some error"})
          }
        },

      })
    )
  }

  return (
    <ManageSystem>
      <div>
        <Row gutter={16} className={"mb-3"}>
          <Col xs={24} sm={12} md={14} lg={16}>
            <Tag color="success"
                 className="mb-2">{`Total ${getStatus("open")} open idea(s)`}</Tag>
            <Tag color="error"
                 className="mb-2">{`Total ${getStatus("")} close idea(s)`}</Tag>
          </Col>
          <Col xs={24} sm={24} md={10} lg={8} style={{display: "flex", justifyContent: "flex-end"}}>
            <Select value={filter} className={"mr-3"} onChange={(value) => {
              setFilter(value)
            }}>
              <Select.Option value="title">All ideas</Select.Option>
              <Select.Option value="slug">Slug</Select.Option>
              <Select.Option value="tag">Tag</Select.Option>
              <Select.Option value="topic">Campaign</Select.Option>
            </Select>
            <Input.Search
              placeholder="input search text"
              allowClear
              size="middle"
              onSearch={(value) => {
                setValue(value);
                setPage(1);
              }}
            />
          </Col>
        </Row>
        <Card>
          <div className="table-responsive">
            <Spin spinning={loading} tip="Loading....">
              <Table
                rowKey={(record) => record.slug}
                columns={columns}
                dataSource={ideas}
                pagination={false}
                footer={() => {
                  return (
                    <Pagination
                      current={page}
                      showQuickJumper
                      defaultCurrent={1}
                      total={pageInfo.totalResults}
                      onChange={(page) => setPage(page)}
                    />
                  )
                }}
              />
            </Spin>
          </div>
        </Card>
      </div>
    </ManageSystem>
  )
}

export const getServerSideProps = withSessionSsr(
  async function getServerSideProps({req, res}): Promise<any> {
    try {
      const user = await auth(req.session as SessionData, ["MANAGE_ALL_IDEA"]);
      const ideas = await queryIdeas({deleted: {$ne: true}}, {page: 1, limit: 10, sortBy: "-createdAt"}, user);
      return {
        props: {
          ideas: JSON.parse(JSON.stringify(ideas?.results)),
          pageInfo: {
            totalResults: ideas?.totalResults,
            hasNextPage: ideas?.page < ideas?.totalPages
          }
        }
      }
    } catch (err) {
      res.statusCode = err["statusCode"] || 500;
      return {
        redirect: {
          permanent: false,
          destination: (err.statusCode === 401) ? '/auth/login' : '/',
        },
      }
    }
  }
);

export default ManageDiscussion
