module.exports = {
  reactStrictMode: true,
  typescript: {
    // !! WARN !!
    ignoreBuildErrors: true,
  },
}
