import {withSessionSsr} from "server/utils/with-session";
import {SessionData} from "server/interfaces/types";
import {getPermissions, getRoleByFilter} from "server/services/role.service";
import {auth} from "server/utils/auth";
import {useRouter} from "next/router";
import {Button, Card, Checkbox, Col, Form, Input, notification, Row} from "antd";
import {useEffect, useState} from "react";
import {ApiService} from "common/services/ApiService";
import {PageHeaderAlt} from "common/components/layout-components/PageHeaderAlt";
import Flex from "common/components/shared-components/Flex";

const EditRole = (props: any) => {
  const router = useRouter();
  const {role, permissions} = props;
  const [form] = Form.useForm();
  const [value, setValue] = useState("");
  const [loading, setLoading] = useState(false);

  useEffect(() => {

    form.setFieldsValue({...role})
  }, [form, role])

  const onFinish = async () => {
    setLoading(true);
    try {
      const values = form.getFieldsValue();
      const res = await ApiService.updateRole(role.slug, values);
      if (res.status) {
        notification.error({
          message: res.data.message
        })
        return setLoading(false)
      }
      setLoading(false)
      notification.success({
        message: res.message
      })
    } catch (err) {
      console.log('err');
      setLoading(false)
      notification.error({message: err});
    }
  }

  const getNamePermission = (slug: any) => {
    const arr = slug.split('-');
    if (arr.length > 1) {
      return `${arr[0].charAt(0).toUpperCase() + arr[0].slice(1)} ${arr[1]}`;
    } else {
      return `${arr[0].charAt(0).toUpperCase() + arr[0].slice(1)}`;
    }
  }

  return (
    <div>
      <Form
        form={form}
        layout="vertical"
        onFinish={onFinish}
      >
        <PageHeaderAlt className="border-bottom" navType={undefined}>
          <div className="container">
            <Flex className="py-2" mobileFlex={false} justifyContent="between" alignItems="center">
              <h2>Edit Role Information</h2>
              <div className="mb-3">
                <Button type="default" className="mr-3" onClick={async () => await router.back()}>Cancel</Button>
                <Button type="primary" htmlType="submit" loading={loading}>
                  Save
                </Button>
              </div>
            </Flex>
          </div>
        </PageHeaderAlt>
        <div className="mt-4">
          <Card>
            <Row gutter={16}>
              <Col xs={24} lg={12}>
                <Form.Item
                  name="name"
                  label="Name"
                  rules={[{required: true, message: "Role name is required!"}]}
                >
                  <Input placeholder="Input role name..." onChange={(e) => setValue(e.target.value)}/>
                </Form.Item>
              </Col>
              <Col xs={24} lg={12}>
                <Form.Item
                  name="slug"
                  label="Slug"
                >
                  <Input disabled={true} value={value}/>
                </Form.Item>
              </Col>
            </Row>
            <Form.Item
              name="permissions"
            >
              <Checkbox.Group>
                {Object.keys(permissions).map((per, index) => (
                  <div key={per + index} className="w-100 mb-3">
                      <span
                        className="d-block mb-2 text-capitalize font-weight-semibold">{getNamePermission(per)} permission</span>
                    <Row gutter={16}>
                      {permissions[per].map((p: any, i: any) => (
                        <Col xs={24} md={12} lg={12} xl={8} key={p + i}>
                          <Checkbox value={p} key={p + i}>
                            <span className="text-capitalize">{p.split("_").join(" ")}</span>
                          </Checkbox>
                        </Col>
                      ))}
                    </Row>
                  </div>
                ))}
              </Checkbox.Group>
            </Form.Item>
          </Card>
        </div>
      </Form>
    </div>
  )
}

export const getServerSideProps = withSessionSsr(
  async function getServerSideProps(context): Promise<any> {
    const {req, res} = context;
    const {slug} = context.query;
    let permissions = {
      user: [],
      role: [],
      comment: [],
      tag: [],
      topic: [],
      idea: [],
      zip: [],
      department: [],
      attachment: [],
    };
    try {
      await auth(req.session as SessionData, ["MANAGE_ALL_ROLE"]);
      const responses = getPermissions();
      const role = await getRoleByFilter({slug: slug});
      if (responses && responses.length > 0) {
        responses.forEach((per: string) => {
          if (per.includes("USER")) {
            // @ts-ignore
            permissions.user.push(per);
          } else if (per.includes("ROLE")) {
            // @ts-ignore
            permissions.role.push(per);
          } else if (per.includes("COMMENT")) {
            // @ts-ignore
            permissions.comment.push(per);
          } else if (per.includes("IDEA")) {
            // @ts-ignore
            permissions.idea.push(per);
          } else if (per.includes("TAG")) {
            // @ts-ignore
            permissions.tag.push(per);
          } else if (per.includes("TOPIC")) {
            // @ts-ignore
            permissions.topic.push(per);
          } else if (per.includes("DEPARTMENT")) {
            // @ts-ignore
            permissions.department.push(per);
          } else if (per.includes("ATTACHMENT")) {
            // @ts-ignore
            permissions.attachment.push(per);
          }else if (per.includes("ZIP")) {
            // @ts-ignore
            permissions.zip.push(per);
          } else {
            console.log("else")
          }
        })
      }
      if (JSON.stringify(role) === "{}") return {props: {}}
      return {
        props: {
          permissions,
          role: JSON.parse(JSON.stringify(role))
        }
      }
    } catch (err) {
      res.statusCode = err["statusCode"] || 500;
      return {
        redirect: {
          permanent: false,
          destination: (err.statusCode === 401) ? '/auth/login' : '/',
        },
      }
    }
  }
)

export default EditRole
