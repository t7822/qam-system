import httpStatus from 'http-status';
import {Attachment, Idea, User} from 'server/models';
import ApiError from 'server/utils/api-error';
import { Types } from 'mongoose';
import fs from "fs";
import AdmZip from 'adm-zip';

const inProgress = new Set();

export const deleteAttachments = (attachments) => {
  if (!attachments || attachments.length < 1) return;
  for (let att of attachments) {
    fs.unlink(att.filePath, (err) => {
      if (err) console.log(err);
    })
  }
}

export const compressAttachments = async (ideaId, attachments: any[]) => {
  if (!attachments || attachments.length < 1) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Attachment not found');
  }
  if (inProgress.has(ideaId.toString())) throw new ApiError(httpStatus.BAD_REQUEST, 'Processing. Please try again later.');
  inProgress.add(ideaId);
  try {
    const zip = new AdmZip();
    const zipPath = `./attachments/zip/${ideaId}.zip`;
    for (let entry of attachments) {
      if (fs.existsSync(entry.filePath)) zip.addLocalFile(entry.filePath, '', entry.originalName);
    }
    if (!fs.existsSync('./attachments/zip')) fs.mkdirSync('./attachments/zip', { recursive: true});
    zip.writeZip(zipPath);
    inProgress.delete(ideaId);
    return {
      zipPath: zipPath
    }
  } catch (err) {
    console.log(err)
    inProgress.delete(ideaId);
    throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, 'An error occurred. Please try again later.');
  }
}

export const addAttachments = async (attBody: any, attachments: Array<any>) => {
  if (!attachments || attachments.length < 1) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Bad request');
  }
  const idea = await Idea.findOne({slug: attBody.idea, deleted: {$ne: true}}).populate({path: "author", model: "User"});
  if (!idea) {
    deleteAttachments(attachments);
    throw new ApiError(httpStatus.NOT_FOUND, 'Idea not found');
  }
  if (attBody.uploader.toString() !== idea.author._id.toString() || attachments.length < 1) {
    deleteAttachments(attachments);
    throw new ApiError(httpStatus.BAD_REQUEST, 'Bad request');
  }
  const documents = attachments.map(a => {
    return {
      idea: idea._id,
      uploader: idea.author._id,
      fileName: a.fileName,
      originalName: a.originalName,
      filePath: a.filePath,
      fileSize: a.size,
      mimeType: a.mimeType,
    }
  })
  const result = await Attachment.insertMany(documents);
  idea.isAttachmentsChanged = true;
  await idea.save();
  return result;
};

export const removeAttachment = async (attBody: any, userId: Types.ObjectId, hasPermission: boolean) => {
  if (!attBody.attachment) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Bad request');
  }
  const attachment = await Attachment.findOne({_id: attBody.attachment})
    .populate({path: "uploader", model: "User"})
    .populate({path: "idea", model: "Idea"});
  if (!attachment) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Attachment not found');
  }
  if (userId.toString() !== attachment.uploader._id.toString() && !hasPermission) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Bad request');
  }
  attachment.deleteOne();
  await Idea.updateOne({_id: attachment.idea._id}, {
    $set: {
      isAttachmentChanged: true
    }
  });
  deleteAttachments([attachment]);
  return attachment;
};

export const removeAllAttachments = async (attBody: any, userId: Types.ObjectId, hasPermission: boolean) => {
  const idea = await Idea.findOne({slug: attBody.idea, deleted: {$ne: true}}).populate({path: "author", model: "User"});
  if (!idea) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Idea not found');
  }
  if (userId.toString() !== idea.author._id.toString() && !hasPermission) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Bad request');
  }
  const attachments = await Attachment.find({
    idea: idea._id
  });
  if (attachments.length < 1) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Attachment not found');
  }
  const result = await Attachment.deleteMany({
    idea: idea._id,
    uploader: userId,
  })
  idea.isAttachmentsChanged = true;
  await idea.save();
  deleteAttachments(attachments);
  return result;
};