import Joi from 'joi';
import {ObjectId} from "server/validations/custom.validations";

export const getVotes: {params: object} = {
  params: Joi.object().keys({
    idea: Joi.string().required(),
  }),
};

export const updateVote: {params: object, body: object} = {
  params: Joi.object().keys({
    idea: Joi.string().required(),
  }),
  body: Joi.object()
    .keys({
      type: Joi.string(),
      active: Joi.string(),
      delete: Joi.boolean()
    })
    .min(1),
};