import React from 'react'
import Loading from 'common/components/shared-components/Loading';
import {ThemeSwitcherProvider, useThemeSwitcher} from "react-css-theme-switcher";
import {THEME_CONFIG} from "common/configs/AppConfig";

const themes = {
	dark: `/css/dark-theme.css`,
	light: `/css/light-theme.css`,
};

interface AuthLayoutProps {
	children: any
}

const AuthLayoutInner = ({children} : AuthLayoutProps) => {
	const { status } = useThemeSwitcher();
  if (status === 'loading') {
    return <Loading cover="page" />;
  }

	return (
		<div className="auth-container">
			{children}
		</div>
	)
}

const AuthLayout = ({children} : AuthLayoutProps) => {
	return (
		<ThemeSwitcherProvider
			themeMap={themes}
			defaultTheme={THEME_CONFIG.currentTheme}
			insertionPoint="styles-insertion-point"
		>
			<AuthLayoutInner>
				{children}
			</AuthLayoutInner>
		</ThemeSwitcherProvider>
	)
}

export default AuthLayout;
