import {Schema, model, models} from 'mongoose';
import {slugify} from "server/utils/slugify";
import {toJSON, paginate} from 'server/models/plugins';
import IdeaTag from 'server/models/idea-tag.model';
import {ITag, TagModel} from "./interfaces/tag.interface";

const tagSchema = new Schema<ITag>({
  title: {
    type: String,
    required: true,
  },
  slug: {
    type: String,
  },
}, {
  collection: "tags",
  timestamps: true,
});

// add plugin that converts mongoose to json
tagSchema.plugin(toJSON);
tagSchema.plugin(paginate);


tagSchema.statics.slugGenerator = async function(tagTitle: string): Promise<string> {
  let newSlug = slugify(tagTitle);
  let count = 0;
  while (await this.exists({slug: newSlug})) {
    newSlug = `${slugify(tagTitle)}_${++count}`;
  }
  return newSlug;
};

tagSchema.pre('save', async function (next) {
  const tag = this;
  if (tag.isModified("title")) {
    tag.slug = await Tag.slugGenerator(tag.title);
  }
  next();
});

tagSchema.pre("deleteOne", async function (next) {
  const tag = this;
  await IdeaTag.deleteMany({tag: tag._id});
  next();
});


const Tag = models.Tag as TagModel || model<ITag, TagModel>('Tag', tagSchema);

export default Tag;
