import {Document, Model} from 'mongoose';
import {IIdea} from "./idea.interface";
import {ITag} from "./tag.interface";

export interface IdeaTagInterface extends Document {
  idea: IIdea["_id"],
  tag: ITag["_id"],
}

// for methods
export interface IIdeaTag extends IdeaTagInterface {
}

// for static methods
export interface IdeaTagModel extends Model<IIdeaTag> {
  paginate(filter: object, options: object): any,
}