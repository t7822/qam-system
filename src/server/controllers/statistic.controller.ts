import {statisticService} from 'server/services';
import {ExtendedRequest, ExtendedResponse} from 'server/interfaces/types';
import pick from "server/utils/pick";

export const getIdeaViewsStatistic = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const filter = pick(req.query, ['user', 'idea', 'date']);
  const result = await statisticService.ideaViews(filter);
  res.json(result);
}

export const getTopicIdeasStatistic = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const filter = pick(req.query, ['author', 'topic', 'date']);
  const result = await statisticService.topicIdeas(filter);
  res.json(result);
}

export const getTopViewsStatistic = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const filter = pick(req.query, ['topic', 'date']);
  const options = pick(req.query, ['limit']);
  Object.assign(filter, {deleted: {$ne: true}});
  const hasPermission = req.session.user.role.permissions.includes("MANAGE_ALL_IDEA");
  const result = await statisticService.topViewsIdeas(filter, options, req.session.user._id, hasPermission);
  res.json(result);
}

export const getTopTopicIdeasStatistic = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const filter = pick(req.query, []);
  const options = pick(req.query, ['limit']);
  Object.assign(filter, {deleted: {$ne: true}});
  const result = await statisticService.topTopicIdeas(filter, options);
  res.json(result);
}