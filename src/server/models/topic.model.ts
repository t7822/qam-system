import {Schema, model, models} from 'mongoose';
import {toJSON, paginate} from 'server/models/plugins';
import {ITopic, TopicModel} from "server/models/interfaces/topic.interface";
import {slugGenerator} from "server/utils/slugify";
import {ApiError} from "next/dist/server/api-utils";
import httpStatus from "http-status";

const topicSchema = new Schema<ITopic>({
  author: {
    type: Schema.Types.ObjectId,
    ref: "User",
    required: true,
  },
  department: {
    type: Schema.Types.ObjectId,
    ref: "Department"
  },
  firstClosureDate: {
    type: Number,
    required: true
  },
  finalClosureDate: {
    type: Number,
    required: true
  },
  title: {
    type: String,
    required: true
  },
  slug: {
    type: String,
  },
  deleted: {
    type: Boolean,
    default: false,
    private: true
  },
}, {
  collection: "topics",
  timestamps: true,
  toJSON: {virtuals: true},
  toObject: {virtuals: true},
});

// add plugin that converts mongoose to json
topicSchema.plugin(toJSON);
topicSchema.plugin(paginate);

topicSchema.pre('save', async function (next) {
  const doc = this;
  if (doc.isModified("title")) {
    doc.slug = await slugGenerator(doc.title, "Topic");
  }
  if (new Date(doc.firstClosureDate).getTime() > new Date(doc.finalClosureDate).getTime()) {
    throw new ApiError(httpStatus.BAD_REQUEST, "First closure date must be sooner than the Final one");
  }
  next();
});

const Topic = models.Topic as TopicModel || model<ITopic, TopicModel>('Topic', topicSchema);

export default Topic;
