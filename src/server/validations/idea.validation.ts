import Joi from 'joi';
import { ObjectId } from "server/validations/custom.validations";

export const createIdea: { body: object } = {
  body: Joi.object().keys({
    title: Joi.string().required(),
    type: Joi.string(),
    content: Joi.string().required(),
    privacy: Joi.string(),
    topic: Joi.string().custom(ObjectId).required(),
    tags: Joi.array().items(Joi.string().custom(ObjectId)),
  }),
};

export const restoreIdeas: { body: object } = {
  body: Joi.object().keys({
    ideas: Joi.array().items(Joi.string().custom(ObjectId)),
  }),
};

export const getIdeas: { query: object } = {
  query: Joi.object().keys({
    title: Joi.string(),
    slug: Joi.string(),
    topic: Joi.string(),
    tag: Joi.string(),
    privacy: Joi.string(),
    closureDate: Joi.string(),
    author: Joi.string().custom(ObjectId),
    sortBy: Joi.string(),
    limit: Joi.number().integer(),
    page: Joi.number().integer(),
  }),
};

export const getIdea: { params: object } = {
  params: Joi.object().keys({
    slug: Joi.string(),
  }),
};

export const updateIdea: { params: object, body: object } = {
  params: Joi.object().keys({
    slug: Joi.string(),
  }),
  body: Joi.object()
    .keys({
      title: Joi.string(),
      type: Joi.string(),
      content: Joi.string(),
      privacy: Joi.string(),
      tags: Joi.array().items(Joi.string().custom(ObjectId)),
    })
    .min(1),
};

export const deleteIdea: { params: object } = {
  params: Joi.object().keys({
    slug: Joi.string(),
  }),
};

export const deleteIdeasPermanently: { body: object } = {
  body: Joi.object().keys({
    ideas: Joi.array().items(Joi.string().custom(ObjectId)),
  }),
};