import React from "react";
import {RootState} from "common/redux/store";
import {useSelector} from "react-redux";
import {Col, Row} from "antd";
import Link from "next/link";
import LoginForm from "../../common/components/auth-components/login-form";


const LoginPage = () => {
  const theme = useSelector((state: RootState) => state.theme.currentTheme)
  return (
    <>
      <div className={`h-100 ${theme === 'light' ? 'bg-white' : ''}`}>
        <Row justify="center" className="align-items-stretch h-100">
          <Col xs={20} sm={20} md={24} lg={16}>
            <div className="container d-flex flex-column justify-content-center h-100 py-3">
              <Row justify="center">
                <Col xs={24} sm={24} md={20} lg={12} xl={8}>
                  <h1>Sign In</h1>
                  {/*<p>{`Don't have an account yet?`} <Link href="/auth/register">Sign Up</Link></p>*/}
                  <div className="mt-4">
                    <LoginForm />
                  </div>
                </Col>
              </Row>
            </div>
          </Col>
          <Col xs={0} sm={0} md={0} lg={8}>
            <div className="d-flex flex-column justify-content-between h-100 px-4" style={{
              backgroundImage: 'url(/img/others/img-17.jpg)',
              backgroundRepeat: 'no-repeat',
              backgroundSize: 'cover'
            }}>
              <div className="text-right">
                <img src="/img/logo-white.png" alt="logo"/>
              </div>
              <Row justify="center">
                <Col xs={0} sm={0} md={0} lg={20}>
                  <img className="img-fluid mb-5" src="/img/others/img-18.png" alt=""/>
                  <h1 className="text-white">Welcome to IDDE</h1>
                  <p className="text-white">Share your ideas.</p>
                </Col>
              </Row>
              <div className="d-flex justify-content-end pb-4">
                <div>
                  <Link href={"/"}>
                    <a className="text-white" onClick={e => e.preventDefault()}>Term & Conditions</a>
                  </Link>
                  <span className="mx-2 text-white"> | </span>
                  <Link href={"/"}>
                    <a className="text-white" onClick={e => e.preventDefault()}>Privacy & Policy</a>
                  </Link>
                </div>
              </div>
            </div>
          </Col>
        </Row>
      </div>
    </>
  )
}


export default LoginPage;
