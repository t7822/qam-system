import {roleService} from 'server/services';
import pick from "server/utils/pick";
import {NextApiResponse} from "next";
import {ExtendedRequest} from "server/interfaces/types";

export const addRole = async (req: ExtendedRequest, res: NextApiResponse) => {
  const role = await roleService.createRole(req.body);
  res.json({
    message: "Created role successfully",
    role: role
  });
};

export const getRoles = async (req: ExtendedRequest, res: NextApiResponse) => {
  const filter = pick(req.query, ['slug', 'type']);
  Object.assign(filter, {deleted: {$ne: true}});
  const options = pick(req.query, ['sortBy', 'limit', 'page']);
  const result = await roleService.queryRoles(filter, options);
  res.json(result);
};

export const getUsers = async (req: ExtendedRequest, res: NextApiResponse) => {
  const filter = pick(req.query, ['username']);
  const options = pick(req.query, ['sortBy', 'limit', 'page']);
  const result = await roleService.getUsers(req.query.permission as string, filter, options);
  res.json(result);
};

export const getRole = async (req: ExtendedRequest, res: NextApiResponse) => {
  const result = await roleService.getRoleByFilter({slug: req.query.slug});
  res.json(result);
};

export const updateRole = async (req: ExtendedRequest, res: NextApiResponse) => {
  const role = await roleService.updateRole({slug: req.query.slug}, req.body);
  res.send({
    message: "Updated role successfully",
    role: role
  });
};

export const deleteRole = async (req: ExtendedRequest, res: NextApiResponse) => {
  const role = await roleService.deleteRole({slug: req.query.slug});
  res.send({
    message: "Deleted role successfully",
    role: role
  });
};

export const getPermissions = async (_req: ExtendedRequest, res: NextApiResponse) => {
  const permissions = roleService.getPermissions();
  res.json({permissions});
};
