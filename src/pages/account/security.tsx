import React, {useState} from "react";
import {Col, Divider, Form, Row, Card, Input, Button, notification, Spin} from "antd";
import AccountSettingLayout from "common/layouts/account-settings-layout";
import {ApiService} from "common/services/ApiService";

const ChangePassword = () => {
  const [loading, setLoading] = useState(false);
  const [form] = Form.useForm();

  const handleChangePassword = async () => {
    setLoading(true);
    const {password, new_password} = form.getFieldsValue();
    const data = {password: new_password, currentPassword: password};
    try {
      const res = await ApiService.updateSelfInfo(data);
      if (res.status) {
        notification.error({
          message: res.data.message
        })
        return setLoading(false);

      } else {
        notification.success({message: 'Changed password successfully'});
        form.resetFields();
      }
    } catch (err) {
      notification.error({message: 'Failed to change password'});
    }
    setLoading(false);
  }

  return (
    <AccountSettingLayout>
      <div className="change-password">
        <h4 className="mb-4">Edit User</h4>
        <Divider dashed className="mb-4"/>
        <Row justify="center">
          <Col xs={24} sm={24} md={24} lg={14}>
            <Spin
              spinning={loading}
              tip='Changing password...'
            >
              <Card>
                <Form
                  form={form}
                  onFinish={handleChangePassword}
                  name="changePasswordForm"
                  layout="vertical"
                >
                  <Form.Item
                    label="Current Password"
                    name="password"
                    rules={[{
                      required: true,
                      message: 'Please enter your current password!'
                    }]}
                  >
                    <Input.Password/>
                  </Form.Item>
                  <Form.Item
                    label="New Password"
                    name="new_password"
                    rules={[{
                      required: true,
                      message: 'Please enter your new password!'
                    }]}
                  >
                    <Input.Password/>
                  </Form.Item>
                  <Form.Item
                    label="Confirm Password"
                    name="confirmPassword"
                    rules={
                      [
                        {
                          required: true,
                          message: 'Please confirm your password!'
                        },
                        ({getFieldValue}) => ({
                          validator(_, value) {
                            if (!value || getFieldValue('new_password') === value) {
                              return Promise.resolve();
                            }
                            return Promise.reject('Password not matched!');
                          },
                        }),
                      ]
                    }
                  >
                    <Input.Password/>
                  </Form.Item>
                  <Button type="primary" htmlType="submit" style={{float: "right"}}>
                    Change password
                  </Button>
                </Form>
              </Card>
            </Spin>
          </Col>
        </Row>
      </div>
    </AccountSettingLayout>
  )
}
export default ChangePassword;
