import httpStatus from "http-status";
import {Department, User} from "server/models"
import ApiError from "server/utils/api-error";
import {IDepartment} from "server/models/interfaces/department.interface";

export const createDepartment = async (departmentBody: any): Promise<IDepartment> => {
  const department = await Department.create(departmentBody);
  await department.populate({path: "manager", model: "User", select: "fullName username avatar"});
  return department;
}

export const queryDepartments = async (filter: any, options: any) => {
  if (filter.manager) {
    const manager = await User.findOne({username: filter.manager});
    if (!manager) throw new ApiError(httpStatus.NOT_FOUND, 'Manager not found');
    filter.manager = manager._id;
  }
  Object.assign(options, {populate: 'manager', filter: {manager: {model: "User", select: "username fullName avatar"}}});
  return Department.paginate(filter, options);
}

export const getDepartmentByFilter = async (filter: any): Promise<IDepartment> => {
  if (filter.manager) {
    const manager = await User.findOne({username: filter.manager});
    if (!manager) throw new ApiError(httpStatus.NOT_FOUND, 'Manager not found');
    filter.manager = manager._id;
  }
  let department = await Department.findOne(filter)
    .populate({path: "manager", model: "User", select: "fullName username avatar"});
  if (!department && department.deleted) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Department not found');
  }
  return department;
}

export const updateDepartment = async (filter: any, updateBody: any): Promise<IDepartment> => {
  const department = await getDepartmentByFilter(filter);
  Object.assign(department, updateBody);
  await department.save();
  return department;
};

export const deleteDepartment = async (filter: any): Promise<IDepartment> => {
  const department = await getDepartmentByFilter(filter);
  await department.updateOne({deleted: true});
  return department;
}

export const deleteDepartmentPermanently = async (filter: any,): Promise<IDepartment> => {
  const department = await getDepartmentByFilter(filter);
  await department.deleteOne();
  return department;
}