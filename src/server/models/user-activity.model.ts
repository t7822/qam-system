import {Schema,models, model} from 'mongoose';
import {paginate, toJSON} from "server/models/plugins";
import {IUserActivity, UserActivityModel} from "server/models/interfaces/user-activity.interface";

const activitySchema = new Schema<IUserActivity>({
  message: {
    type: String,
    required: true
  },
  detail: {
    type: String,
  },
  target: {
    type: Schema.Types.ObjectId,
    required: true
  },
  model: {
    type: String,
    enum: ["Blog", "Comment", "User", "Idea", "Exam", "Result", "Reaction", "Question", "ProfilePost", "Issue"],
    required: true
  },
  user: {
    type: Schema.Types.ObjectId,
    ref: "User",
    required: true
  },
}, {
  collection: 'useractivities',
  timestamps: true,
  toJSON: { virtuals: true },
  toObject: { virtuals: true },
});

activitySchema.plugin(paginate);
activitySchema.plugin(toJSON);

const UserActivity = models.UserActivity as UserActivityModel || model<IUserActivity, UserActivityModel>("UserActivity", activitySchema);

export default UserActivity;