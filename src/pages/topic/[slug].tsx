import ForumLayout from "common/layouts/forum-layout";
import ForumHeader from "common/components/forum-components/forum-header";
import {Typography, Row, Col, Input, Select, Card, Button, List, Avatar, Pagination, Spin} from "antd";
import {
  SearchOutlined,
  MessageOutlined,
  EyeOutlined,
  PlayCircleFilled,
  PlusCircleOutlined, DownloadOutlined
} from '@ant-design/icons';
import Link from "next/link"
import {withSessionSsr} from "server/utils/with-session";
import {auth} from "server/utils/auth";
import {SessionData} from "server/interfaces/types";
import {queryTopics, getTopicByFilter} from "server/services/topic.service";
import {useEffect, useState} from "react";
import moment from "moment";
import {ApiService} from "common/services/ApiService";
import {queryIdeas} from "server/services/idea.service";
import {useRouter} from "next/router";
import {useSelector} from "react-redux";
import {RootState} from "common/redux/store";

const IdeaTopic = (props: any) => {
  const router = useRouter();
  const {slug} = router.query;
  const {user} = useSelector((state: RootState) => state.auth);
  const {recentIdeas, recentTopics} = props;
  const [ideas, setIdeas] = useState(props.ideas || []);
  const [filter, setFilter] = useState('-createdAt');
  const [page, setPage] = useState(1);
  const [value, setValue] = useState("");
  const [pageInfo, setPageInfo] = useState(props.pageInfo || {});
  const [loading, setLoading] = useState(false);

  const getIdeas = async () => {
    setLoading(true)
    if (value !== "") {
      const res = await ApiService.getIdeas({
        topic: slug,
        page: page,
        limit: 5,
        sortBy: filter,
        title: value
      });
      setIdeas(res.results)
      setPageInfo({
        totalResults: res.totalResults,
        hasNextPage: res?.page < res?.totalPages
      })
    } else {
      const res = await ApiService.getIdeas({
        topic: slug,
        page: page,
        limit: 5,
        sortBy: filter
      });
      setIdeas(res.results)
      setPageInfo({
        totalResults: res.totalResults,
        hasNextPage: res?.page < res?.totalPages
      })
    }

    setLoading(false)
  }

  useEffect(() => {
    getIdeas().then((_: any) => {
    })
  }, [value, page, filter])

  const onChange = (value: any) => {
    setFilter(value)
  }

  return (
    <ForumLayout>
      <ForumHeader topic={props.topic}/>
      <Row gutter={16}>
        <Col lg={8} md={10} sm={16} xs={15} className={"mb-2"}>
          <Input.Search onSearch={(value) => {
            setValue(value)
            setPage(1)
          }} prefix={<SearchOutlined/>} allowClear size={"small"}
                        placeholder={"Search for idea..."}/>
        </Col>

        <Col lg={3} md={4} sm={8} xs={9}>
          <Select
            size={"small"}
            style={{width: "100%"}}
            defaultValue={"-createdAt"}
            onChange={onChange}
          >
            <Select.Option value="-createdAt">Newest</Select.Option>
            <Select.Option value="-viewCount">Most Views</Select.Option>
            <Select.Option value="-voteCount">Most Popular</Select.Option>
            <Select.Option value="-commentCount">Most Comments</Select.Option>
          </Select>
        </Col>
        <Col md={0} xs={16} style={{
          alignItems: "center"
        }}>
          <h5>Idea</h5>
        </Col>
        <Col lg={13} md={10} sm={8} xs={8} style={{
          display: "flex",
          justifyContent: "flex-end"
        }}>
          {
            (user?.role.name === "manager") && (
              <Button className="ml-3" href={`/api/ideas/export/csv?topic=${slug}`} icon={<DownloadOutlined/>} size={"small"} type={"default"}>
                Export CSV
              </Button>
            )
          }
        </Col>
      </Row>

      <Row style={{marginTop: 24}} gutter={32}>
        <Col xs={24} md={18}>
          <Spin spinning={loading} tip={"Loading...."}>
            <List
              size="large"
              split={false}
              dataSource={ideas}
              renderItem={(item: any) => <Card bordered={false}>
                <div className={"forum-list-item"}>
                  <div className={"item-main-content"}>
                    <div className={"list-item-vote"}>
                      <EyeOutlined/> {item.viewCount === null ? 0 : item.viewCount}
                    </div>
                    <div className={"list-item-topic"}>
                      <div className={"list-item-topic-outer"}>
                        💬
                      </div>
                    </div>
                    <div className={"list-item-meta"}>
                      <Link href={`/idea/${item.slug}`}>
                        <a className={"title"}>
                          <strong>
                            <Typography.Text>
                              {item.title}
                            </Typography.Text>
                          </strong>
                        </a>
                      </Link>
                      <div className={"description"}>
                        {`${item.author.fullName} started ${moment(item.createdAt).fromNow()} in ${item.topic.title}`}
                      </div>
                    </div>
                  </div>
                  <div className={"item-extra-content"}>
                    <div className={"mr-8"}>
                      <Avatar src={`${item.author.avatar}`} size={24}/>
                    </div>
                    <div>
                      <MessageOutlined/> {item.commentCount}
                    </div>
                  </div>
                </div>
              </Card>}
            />
          </Spin>
          {
            ideas?.length > 0 && (
              <Pagination
                current={page}
                defaultCurrent={1}
                total={pageInfo.totalResults}
                pageSize={5}
                onChange={(page) => setPage(page)}
              />
            )
          }
        </Col>
        <Col md={6} xs={0}>
          <Card
            title={
              <div style={{textAlign: "center"}}>Recent Campaigns</div>
            }
            size="small"
            bordered={false}
          >
            {
              recentTopics.map((topic: any) => {
                return (
                  <div key={topic.slug} className="ml-4">
                    <Link href={`/topic/${topic.slug}`}>
                      <a>
                        <p style={{fontWeight: "bold"}}><PlayCircleFilled/> {topic.title}</p>
                      </a>
                    </Link>
                  </div>
                )
              })
            }
          </Card>
          <Card
            className={"mt-2"}
            size="small"
            title={
              <div style={{textAlign: "center"}}>Recent Ideas</div>
            }
            bordered={false}>
            {
              recentIdeas.map((idea: any) => {
                return (
                  <div key={idea.slug} className="ml-4">
                    <Link href={`/idea/${idea.slug}`}>
                      <a>
                        <p style={{fontWeight: "bold"}}><PlayCircleFilled/> {idea.title}</p>
                      </a>
                    </Link>
                  </div>
                )
              })
            }
          </Card>
        </Col>
      </Row>
    </ForumLayout>
  )
}

export const getServerSideProps = withSessionSsr(
  async function getServerSideProps(context): Promise<any> {
    const {req, res} = context;
    const {slug} = context.query;
    try {
      const user = await auth(req.session as SessionData, []);
      const topic = await getTopicByFilter({deleted: {$ne: true}, slug: slug});
      const ideas = await queryIdeas({deleted: {$ne: true}, topic: slug}, {
        page: 1,
        limit: 5,
        sortBy: "-createdAt"
      }, user);
      const recentIdeas = await queryIdeas({deleted: {$ne: true}}, {page: 1, limit: 3, sortBy: "-createdAt"}, user);
      const recentTopics = await queryTopics({deleted: {$ne: true}}, {page: 1, limit: 3, sortBy: "-createdAt"});
      return {
        props: {
          topic: JSON.parse(JSON.stringify(topic)),
          ideas: JSON.parse(JSON.stringify(ideas?.results)),
          recentIdeas: JSON.parse(JSON.stringify(recentIdeas?.results)),
          recentTopics: JSON.parse(JSON.stringify(recentTopics?.results)),
          pageInfo: {
            totalResults: ideas?.totalResults,
            hasNextPage: ideas?.page < ideas?.totalPages
          },
        }
      }
    } catch (err) {
      res.statusCode = err["statusCode"] || 500;
      return {
        redirect: {
          permanent: false,
          destination: (err.statusCode === 401) ? '/auth/login' : '/',
        },
      }
    }
  }
);

export default IdeaTopic;
