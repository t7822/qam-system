import {NextApiRequest, NextApiResponse} from "next";
import {IronSession, IronSessionData} from "iron-session";
import {IUser} from "../models/interfaces/user.interface";

export interface ExtendedRequest extends NextApiRequest {
  files?: Array<any>,
  session: IronSession,
}

export interface SessionData extends IronSessionData {
  isLoggedIn?: boolean,
  user?: IUser,
  views?: Array<string>,
}


export interface ExtendedResponse extends NextApiResponse {
}

export interface MailData {
  sendTo: string,
  subject: string,
  contentHTML: string
}

export interface Filter {
}