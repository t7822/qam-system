import axios, {AxiosRequestConfig} from "axios";

interface ApiResponse {
  code: number;
  isLoggedIn?: boolean;
  results?: any,
  totalResults?: number,
  page?: any,
  totalPages?: any,
  hasNextPage?: boolean,
  status?: number,
  message?: string,
  data?: any
}

interface RoleApiResponse extends ApiResponse {
  role?: any,
}

interface TagApiResponse extends ApiResponse {
  tag?: any,
}

interface IdeaApiResponse extends ApiResponse {
  content: string;
  attachments?: any;
  idea?: any,
}

interface CommentApiResponse extends ApiResponse {
  content: string;
  comment?: any,
}

interface TopicApiResponse extends ApiResponse {
  department?: any;
  topic?: any,
  title?: any,
  firstClosureDate?: any,
  finalClosureDate?: any
}

interface AuthApiResponse extends ApiResponse {
  user?: any,
}

export class ApiService {
  static API_BASE = "/api"

  static request(url: string, config: AxiosRequestConfig) {
    return new Promise<ApiResponse>((resolve, reject) => {
      axios(url, config).then((response) => {
        resolve(response.data);
      }).catch(err => {
        if (err.response && err.status !== 500) {
          resolve(err.response);
        } else {
          reject(err);
        }
      });
    });
  }

  static async login(username: string, password: string): Promise<AuthApiResponse> {
    return await this.request(`${this.API_BASE}/auth/login`, {
      method: "POST",
      data: {
        username,
        password
      }
    });
  }

  static async register(formData: any) {
    return await this.request(`${this.API_BASE}/auth/register`, {
      method: "POST",
      data: formData
    });
  }

  static async signOut() {
    return await this.request(`${this.API_BASE}/auth/logout`, {
      method: "POST"
    });
  }

  // get current session data (called every full-navigation)
  static async getSession() {
    return await this.request(`${this.API_BASE}/auth/get-session`, {
      method: "GET",
    });
  }

  static async updateSelfInfo(data: any): Promise<AuthApiResponse> {
    return await this.request(`${this.API_BASE}/users/update-self-info`, {
      method: "POST",
      data: data
    })
  }

  static async getUsers(params: any): Promise<AuthApiResponse> {
    return await this.request(`${this.API_BASE}/users`, {
      method: "GET",
      params: params
    })
  }

  static async getUser(username: string): Promise<AuthApiResponse> {
    return await this.request(`${this.API_BASE}/users/${username}`, {
      method: "GET",
    })
  }

  static async addUser(data: any): Promise<AuthApiResponse> {
    return await this.request(`${this.API_BASE}/users/add`, {
      method: "POST",
      data: data
    })
  }

  static async updateUser(username: string, data: any): Promise<AuthApiResponse> {
    return await this.request(`${this.API_BASE}/users/update/${username}`, {
      method: "POST",
      data: data
    })
  }

  static async deleteUser(username: string): Promise<AuthApiResponse> {
    return await this.request(`${this.API_BASE}/users/delete/${username}`, {
      method: "POST"
    })
  }

  static async getRoles(params: any): Promise<RoleApiResponse> {
    return await this.request(`${this.API_BASE}/roles`, {
      method: "GET",
      params: params
    })
  }

  static async getRole(slug: any): Promise<RoleApiResponse> {
    return await this.request(`${this.API_BASE}/roles/${slug}`, {
      method: "GET",
    })
  }

  static async addRole(data: any): Promise<RoleApiResponse> {
    return await this.request(`${this.API_BASE}/roles/add`, {
      method: "POST",
      data: data
    })
  }

  static async updateRole(slug: any, data: any): Promise<RoleApiResponse> {
    return await this.request(`${this.API_BASE}/roles/update/${slug}`, {
      method: "POST",
      data: data
    })
  }

  static async deleteRole(slug: any): Promise<RoleApiResponse> {
    return await this.request(`${this.API_BASE}/roles/delete/${slug}`, {
      method: "POST"
    })
  }

  static async getTags(params: any): Promise<TagApiResponse> {
    return await this.request(`${this.API_BASE}/tags`, {
      method: "GET",
      params: params
    })
  }

  static async getTag(_id: any): Promise<TagApiResponse> {
    return await this.request(`${this.API_BASE}/tags/${_id}`, {
      method: "GET",
    })
  }

  static async addTag(data: any): Promise<TagApiResponse> {
    return await this.request(`${this.API_BASE}/tags/add`, {
      method: "POST",
      data: data
    })
  }

  static async deleteTag(_id: any): Promise<TagApiResponse> {
    return await this.request(`${this.API_BASE}/tags/delete/${_id}`, {
      method: "POST"
    })
  }

  static async getTopics(params: any): Promise<TopicApiResponse> {
    return await this.request(`${this.API_BASE}/topics`, {
      method: "GET",
      params: params
    })
  }

  static async getTopic(slug: any): Promise<TopicApiResponse> {
    return await this.request(`${this.API_BASE}/topics/${slug}`, {
      method: "GET",
    })
  }

  static async addTopic(data: any): Promise<TopicApiResponse> {
    return await this.request(`${this.API_BASE}/topics/add`, {
      method: "POST",
      data: data
    })
  }

  static async deleteTopic(slug: any): Promise<TopicApiResponse> {
    return await this.request(`${this.API_BASE}/topics/delete/${slug}`, {
      method: "POST"
    })
  }

  static async updateTopic(slug: any, data: any): Promise<TopicApiResponse> {
    return await this.request(`${this.API_BASE}/topics/update/${slug}`, {
      method: "POST",
      data: data
    })
  }

  static async addIdea(data: any): Promise<ApiResponse> {
    return await this.request(`${this.API_BASE}/ideas/add`, {
      method: "POST",
      data: data
    })
  }

  static async getIdeas(params: any): Promise<IdeaApiResponse> {
    return await this.request(`${this.API_BASE}/ideas`, {
      method: "GET",
      params: params
    })
  }

  static async getIdea(slug: any): Promise<IdeaApiResponse> {
    return await this.request(`${this.API_BASE}/ideas/${slug}`, {
      method: "GET",
    })
  }

  static async deleteIdea(slug: any): Promise<IdeaApiResponse> {
    return await this.request(`${this.API_BASE}/ideas/delete/${slug}`, {
      method: "POST"
    })
  }

  static async updateIdea(slug: any, data: any): Promise<IdeaApiResponse> {
    return await this.request(`${this.API_BASE}/ideas/update/${slug}`, {
      method: "POST",
      data: data
    })
  }

  static async getComments(params: any): Promise<CommentApiResponse> {
    return await this.request(`${this.API_BASE}/comments`, {
      method: "GET",
      params: params
    })
  }

  static async getComment(id: any): Promise<CommentApiResponse> {
    return await this.request(`${this.API_BASE}/comments/${id}`, {
      method: "GET",
    })
  }

  static async addComment(data: any): Promise<CommentApiResponse> {
    return await this.request(`${this.API_BASE}/comments/add`, {
      method: "POST",
      data: data
    })
  }

  static async deleteComment(id: any, data: any): Promise<CommentApiResponse> {
    return await this.request(`${this.API_BASE}/comments/delete/${id}`, {
      method: "POST",
      data: data
    })
  }

  static async updateComment(id: any, data: any): Promise<CommentApiResponse> {
    return await this.request(`${this.API_BASE}/comments/update/${id}`, {
      method: "POST",
      data: data
    })
  }

  static async reactIdea(id: any, data: any): Promise<any> {
    return await this.request(`${this.API_BASE}/reactions/update/${id}`, {
      method: "POST",
      data: data,
    })
  }

  static async voteIdea(slug: any, data: any): Promise<any> {
    return await this.request(`${this.API_BASE}/votes/update/${slug}`, {
      method: "POST",
      data: data,
    })
  }

  static async IdeaViews(params: any): Promise<any> {
    return await this.request(`${this.API_BASE}/statistics/idea-views`, {
      method: "GET",
      params: params
    })
  }

  static async ideaTopic(params: any): Promise<any> {
    return await this.request(`${this.API_BASE}/statistics/topic-ideas`, {
      method: "GET",
      params: params
    })
  }

  static async topTopicIdeas(params: any): Promise<any> {
    return await this.request(`${this.API_BASE}/statistics/top-topic-ideas`, {
      method: "GET",
      params: params
    })
  }

  static async topIdeasViews(params: any): Promise<any> {
    return await this.request(`${this.API_BASE}/statistics/top-views`, {
      method: "GET",
      params: params
    })
  }

  static async getIdeaReactions(slug: string) {
    return await this.request(`${this.API_BASE}/ideas/${slug}/reactions`, {

    })
  }

  static async addAttachment(data: any): Promise <any> {
    return await this.request(`${this.API_BASE}/attachments/add`, {
      method: "POST",
      data: data
    })
  }
  static async removeAttachment(data: any): Promise <any> {
    return await this.request(`${this.API_BASE}/attachments/remove`, {
      method: "POST",
      data: data
    })
  }
  static async removeAllAttachments(data: any): Promise <any> {
    return await this.request(`${this.API_BASE}/attachments/remove-all`, {
      method: "POST",
      data: data
    })
  }

  static async getDepartments(params: any): Promise <any> {
    return await this.request(`${this.API_BASE}/departments`, {
      method: "GET",
      params: params
    })
  }

  static async getDepartment(slug: any): Promise <any> {
    return await this.request(`${this.API_BASE}/departments/${slug}`, {
      method: "GET",
    })
  }
  static async addDepartment(data: any): Promise <any> {
    return await this.request(`${this.API_BASE}/departments/add`, {
      method: "POST",
      data: data
    })
  }
  static async updateDepartment(slug: string, data: any): Promise <any> {
    return await this.request(`${this.API_BASE}/departments/update/${slug}`, {
      method: "POST",
      data: data
    })

  }static async deleteDepartment(slug: any): Promise <any> {
    return await this.request(`${this.API_BASE}/departments/delete/${slug}`, {
      method: "POST",
    })
  }
}
