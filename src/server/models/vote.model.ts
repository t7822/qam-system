import {Schema, model, models} from 'mongoose';
import {paginate, toJSON} from "server/models/plugins";
import {IVote, VoteModel} from "./interfaces/vote.interface";

const voteSchema = new Schema<IVote>({
  user: {
    type: Schema.Types.ObjectId,
    ref: "User",
    required: true
  },
  idea: {
    type: Schema.Types.ObjectId,
    ref: "Idea",
    required: true
  },
  type: {
    type: String,
    enum: ['upvote', 'downvote'],
  },
  active: {
    type: Boolean,
    default: true,
  },
  activatedAt: {
    type: Number,
    default: Date.now()
  },
}, {
  collection: 'votes',
  timestamps: true
});

voteSchema.plugin(paginate);
voteSchema.plugin(toJSON);

/**
 * @typedef Vote
 */
const Vote = models.Vote || model<IVote, VoteModel>(
  "Vote",
  voteSchema
);

export default Vote;