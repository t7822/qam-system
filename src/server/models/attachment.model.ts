import {Schema, model, models} from 'mongoose';
import {toJSON, paginate} from 'server/models/plugins';
import {IAttachment, AttachmentModel} from "./interfaces/attachment.interface";

const attachmentSchema = new Schema<IAttachment>({
  idea: {
    type: Schema.Types.ObjectId,
    ref: "Idea",
    required: true
  },
  uploader: {
    type: Schema.Types.ObjectId,
    ref: "User",
    required: true
  },
  fileName: {
    type: String,
    required: true
  },
  originalName: {
    type: String,
    required: true
  },
  filePath: {
    type: String,
    required: true
  },
  fileSize: {
    type: Number,
    required: true
  },
  mimeType: {
    type: String,
  }
}, {
  collection: "attachments",
  timestamps: true,
});

// add plugin that converts mongoose to json
attachmentSchema.plugin(toJSON);
attachmentSchema.plugin(paginate);

const Attachment = models.Attachment || model<IAttachment, AttachmentModel>('Attachment', attachmentSchema);

export default Attachment;
