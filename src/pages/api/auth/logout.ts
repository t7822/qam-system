import {connectDB, errorHandler} from "server/middleware";
import middlewares from "server";
import {NextApiHandler, NextApiRequest, NextApiResponse} from "next";
import {signOut} from "server/controllers/auth.controller";
import {withIronSessionApiRoute} from "iron-session/next";
import {ironOptions} from "server/config";

const handler: NextApiHandler = async (req: NextApiRequest, res: NextApiResponse) => {
  await middlewares(req, res);

  if (req.method === "POST") {
    try {
      await signOut(req, res);
    } catch (err) {
      errorHandler(err, req, res);
    }
  }
};

export default connectDB(withIronSessionApiRoute(handler, ironOptions));
