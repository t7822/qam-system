import httpStatus from 'http-status';
import {authService, userService} from 'server/services';
import {ExtendedRequest, ExtendedResponse} from "server/interfaces/types"

export const register = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const user = await userService.createUser(req.body);
  res.status(httpStatus.CREATED).send({user});
};

export const login = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const user = await authService.login(req.body);
  // const tokens = await tokenService.generateAuthTokens(user._id, req.body.remember);

  if (user) {
    req.session.isLoggedIn = true;
    req.session.user = user;
    await req.session.save();
  }

  res.send({user});
};

export const getSessionData = async (req: ExtendedRequest, res: ExtendedResponse) => {
  if (req.session.isLoggedIn) {
    res.json({
      user: req.session.user,
      isLoggedIn: true
    })
  } else {
    res.json({
      isLoggedIn: false
    })
  }
};

export const signOut = async (req: ExtendedRequest, res: ExtendedResponse) => {
  await req.session.destroy();
  res.status(httpStatus.OK).send({
    status: 200,
    message: 'ok'
  });
};