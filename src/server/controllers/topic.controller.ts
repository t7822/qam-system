import {topicService} from 'server/services';
import pick from "server/utils/pick";
import {ExtendedRequest, ExtendedResponse} from "server/interfaces/types";

export const addTopic = async (req: ExtendedRequest, res: ExtendedResponse) => {
  Object.assign(req.body, {author: req.session.user?._id});
  const topic = await topicService.createTopic(req.body);
  res.json({
    message: "Created campaign successfully",
    topic: topic
  });
};

export const getTopics = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const filter = pick(req.query, ['slug', 'title', 'closureDate', "department"]);
  Object.assign(filter, {deleted: {$ne: true}});
  const options = pick(req.query, ['sortBy', 'limit', 'page']);
  const result = await topicService.queryTopics(filter, options);
  res.json(result);
};

export const getTopic = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const result = await topicService.getTopicByFilter({slug: req.query.slug});
  res.json(result);
};

export const updateTopic = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const topic = await topicService.updateTopic({slug: req.query.slug}, req.body);
  res.json({
    message: "Updated campaign successfully",
    topic: topic
  });
};

export const deleteTopic = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const topic = await topicService.deleteTopic({slug: req.query.slug});
  res.json({
    message: "Deleted campaign successfully",
    topic: topic
  });
};