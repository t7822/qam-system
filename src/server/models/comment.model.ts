import {Schema, model, models} from 'mongoose';
import {paginate, toJSON} from 'server/models/plugins';
import httpStatus from "http-status";
import Reaction from "server/models/reaction.model";
import ApiError from "server/utils/api-error";
import {IComment, CommentModel} from "./interfaces/comment.interface";

const commentSchema = new Schema<IComment>({
  author: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  content: {
    type: String,
    required: true
  },
  replyFor: {
    type: Schema.Types.ObjectId,
    ref: 'Comment',
  },
  level: {
    type: Number,
    default: 1
  },
  tag: {
    type: Schema.Types.ObjectId,
    ref: 'User',
  },
  category: {
    type: Schema.Types.ObjectId,
  },
  privacy: {
    type: String,
    enum: ["public", "anonymous"],
    default: "public"
  },
  editedAt: {
    type: String,
  },
  editedTimes: {
    type: Number,
  }
}, {
  collection: "comments",
  timestamps: true
});

commentSchema.plugin(paginate);
commentSchema.plugin(toJSON);

commentSchema.pre("save", async function (next) {
  const comment = this;

  if (comment.replyFor) {
    const replyFor = await Comment.findOne({_id: comment.replyFor})
    if (!replyFor) {
      throw new ApiError(httpStatus.NOT_FOUND, "Comment not found")
    }
    if (replyFor.level < 3) {
      comment.level = ++replyFor.level
    }
    if (replyFor.level === 3) {
      comment.level = 3
    }
  }

  next();
});

// will call before remove method => cascading delete all blogs model references
commentSchema.pre("deleteOne"/*, {document: true, query: false}*/, async function (next) {
  const comment = this;
  await Comment.deleteMany({replyFor: comment._id});
  await Reaction.deleteMany({target: comment._id});
  next();
});

/**
 * @typedef Comment
 */
const Comment = models.Comment || model<IComment, CommentModel>('Comment', commentSchema);

export default Comment;


