import Joi from 'joi';
import { ObjectId } from "server/validations/custom.validations";

export const getIdeaViewsStatistic: { query: object } = {
  query: Joi.object().keys({
    user: Joi.string(),
    idea: Joi.string(),
    date: Joi.string(),
  }),
};

export const getTopicIdeasStatistic: { query: object } = {
  query: Joi.object().keys({
    author: Joi.string(),
    topic: Joi.string(),
    date: Joi.string(),
  }),
};

export const getTopViewsStatistic: { query: object } = {
  query: Joi.object().keys({
    limit: Joi.number(),
    topic: Joi.string(),
    date: Joi.string(),
  }),
};

export const getTopTopicIdeasStatistic: { query: object } = {
  query: Joi.object().keys({
    limit: Joi.number(),
  }),
};