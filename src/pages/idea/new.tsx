import React, {createRef, useEffect, useState} from "react";
import {Button, Checkbox, Col, Card, Input, Row, Select, Upload, Form, notification} from "antd";
import Editor from "common/components/util-components/DynamicImport/dynamic-editor";
import {FolderOpenOutlined} from "@ant-design/icons";
import Link from "next/link";
import {withSessionSsr} from "server/utils/with-session";
import {auth} from "server/utils/auth";
import {SessionData} from "server/interfaces/types";
import {queryTopics} from "server/services/topic.service";
import {queryTags} from "server/services/tag.service";
import {ApiService} from "common/services/ApiService";
import axios from "axios";
import {useRouter} from "next/router";

const NewIdea = (props: any) => {
  const editorRef = createRef();
  const router = useRouter();
  const [form] = Form.useForm();
  const [agreed, setAgreed] = useState(false);
  const [checked, setChecked] = useState(false);
  const [type, setType] = useState("");
  const [loading, setLoading] = useState(false);
  const [content, setContent] = useState("");

  useEffect(() => {
    if (props?.topics?.length === 0) {
      notification.error({message: "No campaigns available right now. Please comeback later"});
      setTimeout(async () => {
        await router.replace("/");
      }, 2000)
    }
  }, [])

  const submitIdea = async () => {
    // @ts-ignore
    let newForm = new FormData();
    setLoading(true)
    let data = {
      title: form.getFieldValue("title"),
      content: content,
      topic: form.getFieldValue("topic"),
      tags: form.getFieldValue("tags"),
      attachments: form.getFieldValue("attachments")?.fileList || [],
    }

    if (type !== "") {
      data = Object.assign({privacy: "anonymous"}, data);
    }

    for (let item in data) {
      if (["attachments", "tags"].includes(item)) {
        data[item]?.forEach(x => {
          if (item === "tags") newForm.append(item, x)
          else newForm.append(item, x.originFileObj)
        })
      } else {
        newForm.append(item, data[item])
      }
    }

    if (form.getFieldValue("attachments") && form.getFieldValue("attachments")?.fileList.length > 0) {
      await axios.post('/api/ideas/add', newForm, {
        headers:
          {
            'Content-Type': 'multipart/form-data',
          }
      }).then(res => {
        console.log('res', res)
        if (res.status !== 200) {
          notification.error({
            message: res.data.message
          });
          setAgreed(!agreed)
          return setLoading(false)
        }
        setAgreed(!agreed);
        setContent("");
        notification.success({
          message: !checked ? res.data["message"] : "Create Anonymous Idea Successfully"
        });
        setChecked(false);
        form.resetFields();
        setLoading(false);
        router.push(`/idea/${res.data.idea.slug}`)
      }).catch(err => {
        console.log('err', err);
        setLoading(false);
      })
    } else {
      const res = await ApiService.addIdea(data);
      if (res.status) {
        notification.error({
          message: res.data.message
        });
        setAgreed(!agreed)
        return setLoading(false)
      }
      setAgreed(!agreed);
      setContent("");
      notification.success({
        message: !checked ? res.message : "Create Anonymous Idea Successfully"
      });
      setChecked(false);
      form.resetFields();
      setLoading(false)
    }
  }

  return (
    <Card>
      <h1>New Idea</h1>
      <Form form={form} layout={"vertical"} onFinish={submitIdea}>
        <Row gutter={32}>
          <Col xs={24} md={18}>
            <Form.Item
              name="title"
              label="Idea Title"
              rules={[{required: true, message: "Title is required!"}]}
            >
              <Input size={"small"} placeholder={"Idea title..."}/>
            </Form.Item>
            <Form.Item
              name="content"
              label="Idea Content"
              rules={[{required: true, message: "Content is required!"}]}
            >
              <Editor ref={editorRef} height="400px" onChange={() => {
                setContent(editorRef.current.getInstance().getMarkdown())
              }}/>
            </Form.Item>
          </Col>
          <Col xs={24} md={6}>
            <Form.Item
              name="topic"
              label="Campaign"
              rules={[{required: true, message: "Campaign is required!"}]}
            >
              <Select
                size={"small"}
                placeholder="Please select"
                style={{width: '100%'}}
              >
                {
                  props.topics?.map((topic: any, index: any) => {
                    return (
                      <Select.Option key={index} value={topic?._id}>
                        {topic?.title}
                      </Select.Option>
                    )
                  })
                }
              </Select>
            </Form.Item>
            <Form.Item name="tags" label="Tags" rules={[{required: true, message: "Tag is required!"}]}>
              <Select
                mode="tags"
                size={"small"}
                placeholder="Please select"
                style={{width: '100%'}}
              >
                {
                  props.tags?.map((tag: any, index: any) => {
                    return (
                      <Select.Option key={index} value={tag._id}>
                        {tag.title}
                      </Select.Option>
                    )
                  })
                }
              </Select>
            </Form.Item>
            <Form.Item name="attachments" label="Attachments">
              <Upload.Dragger multiple accept=".doc,.docx,.pdf">
                <p className="ant-upload-drag-icon">
                  <FolderOpenOutlined/>
                </p>
                <p className="ant-upload-text">Post File</p>
                <p className="ant-upload-hint">
                  Drag your file here
                </p>
              </Upload.Dragger>
            </Form.Item>
            <div className={"mb-2"}>
              <Checkbox
                checked={checked}
                onChange={(e) => {
                  setChecked(e.target.checked)
                  if (!checked) {
                    setType("privacy");
                  } else {
                    setType("");
                  }
                }}
              >
                Post with anonymous
              </Checkbox>
            </div>
            <div className={"mb-3"}>
              <Checkbox
                checked={agreed}
                onChange={e => setAgreed(e.target.checked)}
              >
                Agree with <Link href={"/terms-of-use"}><a href={"/terms-of-use"}>Terms of Use</a></Link>
              </Checkbox>
            </div>
            <Button disabled={!agreed} block htmlType="submit" loading={loading} type={"primary"} size={"small"}>
              Post
            </Button>
          </Col>
        </Row>
      </Form>
    </Card>
  );
};

export const getServerSideProps = withSessionSsr(
  async function getServerSideProps({req, res}): Promise<any> {
    try {
      const user = await auth(req.session as SessionData, []);
      if (user.role.name !== "staff") {
        return {
          redirect: {
            permanent: false,
            destination: '/',
          },
        }
      }
      const topics = await queryTopics({deleted: {$ne: true}, closureDate: "first", department: user.department.slug}, {
        page: 1,
        limit: 10,
        sortBy: "createdAt"
      });
      const tags = await queryTags({deleted: {$ne: true}}, {page: 1, limit: 10, sortBy: "createdAt"})
      return {
        props: {
          topics: JSON.parse(JSON.stringify(topics?.results)),
          tags: JSON.parse(JSON.stringify(tags?.results))
        }
      }
    } catch (err) {
      res.statusCode = err["statusCode"] || 500;
      return {
        redirect: {
          permanent: false,
          destination: (err.statusCode === 401) ? '/auth/login' : '/',
        },
      }
    }
  }
);


export default NewIdea;
