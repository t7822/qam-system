import httpStatus from 'http-status';
import {Reaction} from 'server/models';
import ApiError from 'server/utils/api-error';
import reactionConfig from "server/config/reaction.config";
import {Schema, models, Types} from "mongoose";

export const updateReaction = async (targetId: Types.ObjectId, userId: Schema.Types.ObjectId, body: any) => {
  let target = await models[body.model].findOne({_id: targetId}).populate({path: "author", model: "User"});
  if (!target || target.deleted) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Not found');
  }
  let reaction = await Reaction.findOne({user: userId, target: targetId});
  let message = `You have given a reaction to a ${(body.model === "Idea") ? "idea" : (body.model === "Comment") ? "comment" : `${body.model.toLowerCase()}`}`;

  if (!reaction) {
    reaction = await Reaction.create({
      user: userId,
      target: targetId,
      active: body.active ?? true,
      type: body.type ?? reactionConfig.default,
    });
  } else {
    reaction.active = body.active ?? reaction.type === body.type ? !reaction.active : true;
    reaction.type = body.type ?? reactionConfig.default;
    if (!reaction.active) message = `You have un-reacted an ${(body.model === "Idea") ? "idea" : (body.model === "Comment") ? "comment" : `${body.model.toLowerCase()}`}`;
    await reaction.save();
  }
  target = (body.model === "Comment" && body.reactTo) ? target.category : target._id;
  const reactions = await getReactions(targetId);
  return {
    message: message,
    active: reaction.active,
    reaction: reaction,
    reactions: reactions,
    target: target,
  }
}

export const getReactions = async (targetId: Types.ObjectId) => {
  let reactions = await Reaction.find({target: targetId, active: true}).populate({
    path: "user",
    model: "User",
    select: {avatar: 1, displayName: 1, fullName: 1, username: 1}
  }).lean();
  let result : any = {
    total: reactions.length
  };
  for (let reaction of reactions) {
    if (!result[reaction.type]) {
      result[reaction.type] = {
        users: [reaction.user],
        count: 1,
      }
    } else {
      result[reaction.type].users.push(reaction.user);
      result[reaction.type].count++;
    }
  }
  return result;
};
