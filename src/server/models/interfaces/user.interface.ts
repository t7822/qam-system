import {Document, Model, Schema} from 'mongoose';
import {IRole} from "server/models/interfaces/role.interface";
import {IDepartment} from "./department.interface";

export interface UserInterface extends Document {
  username: string,
  id: number,
  email: string,
  password: string,
  role: IRole["_id"],
  department: IDepartment["_id"],
  dob: string,
  gender: string,
  fullName: string,
  avatar: string,
  deleted: boolean,
  address: string,
  contact: string
}

// for methods
export interface IUser extends UserInterface {
  isPasswordMatch(password: string): Promise<boolean>,
}

// for static methods
export interface UserModel extends Model<IUser> {
  isFieldTaken(field:object, excludeId?: Schema.Types.ObjectId): Promise<boolean>,
  handleRole(data:any): Promise<IRole>,
  paginate(filter: object, options: object): any
}