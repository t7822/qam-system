import {auth, connectDB, errorHandler, validate} from "server/middleware";
import {userController} from "server/controllers";
import middlewares from "server";
import {runMiddleware} from "server/utils/middleware-handler";
import {getUser} from "server/validations/user.validation";
import {withIronSessionApiRoute} from "iron-session/next";
import {ironOptions} from "server/config";
import {NextApiHandler, NextApiRequest, NextApiResponse} from "next";

const handler: NextApiHandler = async (req: NextApiRequest, res: NextApiResponse) => {
  await middlewares(req, res);
  await runMiddleware(req, res, auth(req, res, "MANAGE_ALL_USER", "GET_ALL_USER"));
  await runMiddleware(req, res, validate(req, res, getUser));

  if (req.method === "GET") {
    try {
      await userController.getUser(req, res);
    } catch (err) {
      errorHandler(err, req, res);
    }
  }
};

export default connectDB(withIronSessionApiRoute(handler, ironOptions));
