import {Schema, model, models} from 'mongoose';
import {slugGenerator} from "server/utils/slugify";
import {paginate, toJSON} from "server/models/plugins";
import {IRole, RoleModel} from "./interfaces/role.interface";

const roleSchema = new Schema<IRole>({
  name: {
    type: String,
    required: true,
    trim: true,
    lowercase: true
  },
  slug: {
    type: String
  },
  permissions: [{
    type: String
  }],
  deleted: {
    type: Boolean,
    default: false,
    private: true
  },
});

roleSchema.pre('save', async function (next) {
  const role = this;
  if (role.isModified("name")) {
    const name: string = role.name;
    role.slug = await slugGenerator(name, "Role");
  }
  next();
});

roleSchema.plugin(paginate);
roleSchema.plugin(toJSON);

/**
 * @typedef Role
 */
const Role = models.Role as RoleModel|| model<IRole, RoleModel>("Role", roleSchema);

export default Role;