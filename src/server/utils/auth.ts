import httpStatus from "http-status";
import {connectDb} from "server/utils/connect-db";
import ApiError from "server/utils/api-error";
import {SessionData} from "server/interfaces/types";

export const auth = async (session: SessionData, permissions: string[] | null) => {
  // eslint-disable-next-line react-hooks/rules-of-hooks
  await connectDb();
  const {user} = session;

  if (!user) {
    throw new ApiError(httpStatus.UNAUTHORIZED, "Unauthorized");
  }

  if (permissions && permissions.length > 0) {
    const userPermission = user.role.permissions;
    const hasRequiredPermission = permissions.some(permission => userPermission.includes(permission));
    if (!hasRequiredPermission) {
      throw new ApiError(httpStatus.UNAUTHORIZED, "Access is not authorized.");
    }
  }
  return user;
}
