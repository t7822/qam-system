import React, { Component } from 'react';
import { Breadcrumb } from 'antd';
import navigationConfig from "common/configs/NavigationConfig";
import IntlMessage from 'common/components/util-components/IntlMessage';
import {useRouter} from "next/router";

let breadcrumbData = {
	'/app' : <IntlMessage id="home" />
};

navigationConfig.forEach((elm) => {
	const assignBreadcrumb = (obj) => breadcrumbData[obj.path] = <IntlMessage id={obj.title} />;
	assignBreadcrumb(elm);
	if (elm.submenu) {
		elm.submenu.forEach( elm => {
			assignBreadcrumb(elm)
			if(elm.submenu) {
				elm.submenu.forEach( elm => {
					assignBreadcrumb(elm)
				})
			}
		})
	}
})

const BreadcrumbRoute = props => {
	const router = useRouter();
	const pathSnippets = router.pathname.split('/').filter(i => i);
	const buildBreadcrumb = pathSnippets.map((_, index) => {
    const url = `/${pathSnippets.slice(0, index + 1).join('/')}`;
    return (
      <Breadcrumb.Item key={url}>
        <a href={url}>{breadcrumbData[url]}</a>
      </Breadcrumb.Item>
    );
	});

  return (
		<Breadcrumb>
			{buildBreadcrumb}
		</Breadcrumb>
  );
};

export class AppBreadcrumb extends Component {
	render() {
		return (
			<BreadcrumbRoute />
		)
	}
}

export default AppBreadcrumb
