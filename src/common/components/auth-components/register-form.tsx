import React, {useEffect, useState} from 'react'
import {useDispatch, useSelector} from 'react-redux'
import {LockOutlined, MailOutlined, UserAddOutlined, UserOutlined} from '@ant-design/icons';
import {Button, Form, Input, Alert, notification} from "antd";
import {motion} from "framer-motion"
import {ApiService} from "../../services/ApiService";
import {useRouter} from "next/router";
import {RootState} from "../../redux/store";
import {setShowAuthMessage} from "../../redux/slices/auth.slice";

const rules = {
  fullName: [{
    required: true,
    message: "Please input your full name"
  }],
  username: [{
    required: true,
    message: "Please input your username"
  }],
  email: [{
    required: true,
    message: 'Please input your email address'
  }],
  password: [{
    required: true,
    message: 'Please input your password'
  }],
  confirm: [{
    required: true,
    message: 'Please confirm your password!'
  }, ({getFieldValue} : {getFieldValue: any}) => ({
    //@ts-ignore
    validator(rule: any, value: any) {
      if (!value || getFieldValue('password') === value) {
        return Promise.resolve();
      }
      return Promise.reject('Passwords do not match!');
    },
  })]
}

export const RegisterForm = () => {
  const {authMessage, showAuthMessage} = useSelector((state: RootState) => state.auth);
  const [form] = Form.useForm();
  const router = useRouter();
  const [loading, setLoading] = useState(false);
  const dispatch = useDispatch();

  useEffect(() => {
    if (showAuthMessage) {
      setTimeout(() => {
        dispatch(setShowAuthMessage(false));
      }, 3000);
    }
  }, [showAuthMessage]);

  const onSignUp = (values: any) => {
    form.validateFields().then(async () => {
      setLoading(true);
      try {
        const res = await ApiService.register(values)
        if (res) {
          notification.success({
            message: "Register account successfully."
          })
          setTimeout(() => {
            router.replace("/auth/login");
          }, 400)
        }
      } catch (err) {
        console.log(err)
        notification.error({
          message: err.response.data.message
        })
        setLoading(false)
        console.log(err)
      } finally {
        setLoading(false)
      }
    }).catch(info => {
      console.log('Validate Failed:', info);
    });
  }

  return (
    <>
      <motion.div
        initial={{opacity: 0, marginBottom: 0}}
        animate={{
          opacity: showAuthMessage ? 1 : 0,
          marginBottom: showAuthMessage ? 20 : 0
        }}>
        <Alert type="error" showIcon message={authMessage}/>
      </motion.div>
      <Form form={form} layout="vertical" name="register-form" onFinish={onSignUp}>
        <Form.Item
          name="fullName"
          label="Full Name"
          rules={rules.fullName}
          hasFeedback
        >
          <Input prefix={<UserOutlined className="text-primary"/>}/>
        </Form.Item>
        <Form.Item
          name="email"
          label="Email"
          rules={rules.email}
          hasFeedback
        >
          <Input type={"email"} prefix={<MailOutlined className="text-primary"/>}/>
        </Form.Item>
        <Form.Item
          name="username"
          label="Username"
          rules={rules.password}
          hasFeedback
        >
          <Input prefix={<UserAddOutlined className="text-primary"/>}/>
        </Form.Item>
        <Form.Item
          name="password"
          label="Password"
          rules={rules.password}
          hasFeedback
        >
          <Input.Password prefix={<LockOutlined className="text-primary"/>}/>
        </Form.Item>
        <Form.Item
          name="confirm"
          label="Confirm Password"
          rules={rules.confirm}
          hasFeedback
        >
          <Input.Password prefix={<LockOutlined className="text-primary"/>}/>
        </Form.Item>
        <Form.Item>
          <Button type="primary" htmlType="submit" block loading={loading}>
            Sign Up
          </Button>
        </Form.Item>
      </Form>
    </>
  )
}

export default RegisterForm;
