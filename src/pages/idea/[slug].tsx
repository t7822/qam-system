import React, {createRef, useState, useEffect} from "react";
import {
  Avatar,
  Button,
  Card,
  Col,
  Divider,
  Row,
  Tag,
  Timeline,
  Typography,
  Dropdown,
  Menu,
  Spin,
  Modal,
  Switch,
  Skeleton,
  notification, Tooltip, Upload, Form,
} from "antd";
import Editor from "common/components/util-components/DynamicImport/dynamic-editor";
import {
  CaretUpOutlined,
  CaretDownOutlined,
  FormOutlined,
  DownloadOutlined,
  EllipsisOutlined,
  RightOutlined,
  FileAddOutlined,
  DeleteOutlined,
  EditOutlined,
  EyeOutlined,
  FileFilled, FolderOpenOutlined,
} from "@ant-design/icons";
import {withSessionSsr} from "server/utils/with-session";
import {auth} from "server/utils/auth";
import {SessionData} from "server/interfaces/types";
import {getIdeaByFilter} from "server/services/idea.service";
import moment from "moment";
import InfiniteScroll from "react-infinite-scroll-component"
import ReactMarkdown from "react-markdown";
import {ApiService} from "common/services/ApiService";
import {queryComments} from "server/services/comment.service";
import {useSelector} from "react-redux";
import {RootState} from "common/redux/store";
import {EmojiBox, VoteBox} from "common/components/shared-components/VoteBox";
import {useRouter} from "next/router";
import axios from "axios";

const {confirm} = Modal;

const SinglePost = (props: any) => {
  const router = useRouter();
  const {slug} = router.query;
  const {user} = useSelector((state: RootState) => state.auth);
  const [idea, setIdea] = useState<any>({});
  const editorRef = createRef();
  const [comments, setComments] = useState(props.comments || []);
  const [value, setValue] = useState('');
  const [editor, setEditor] = useState(false);
  const [attachments, setAttachments] = useState([]);
  const [loading, setLoading] = useState(false);
  const [nextPage, setNextPage] = useState(2);
  const [showSkeleton, setShowSkeleton] = useState(false);
  const [editorLoading, setEditorLoading] = useState(false);
  const [hasMore, setHasMore] = useState(true);
  const [react, setReact] = useState(0);
  const [content, setContent] = useState("");
  const [userReaction, setUserReaction] = useState("");
  const [showIdea, setShowIdea] = useState(false);
  const [type, setType] = useState("");
  const [anonymous, setAnonymous] = useState(false);

  useEffect(() => {
    getIdea().then((_: any) => {
    });
  }, [])

  const getIdea = async () => {
    setShowIdea(true)
    const res = await ApiService.getIdea(slug);
    setIdea(res);
    setAttachments(res.attachments)
    setContent(res.content);
    setShowIdea(false);
  }

  useEffect(() => {
    setUserReaction(idea?.userVote);
    const reaction = idea?.votes;
    const isLike = reaction?.hasOwnProperty('upvote');
    const isDislike = reaction?.hasOwnProperty('downvote');
    if (isLike && isDislike) {
      setReact(reaction['upvote'] - reaction['downvote']);
    } else if (!isLike && isDislike) {
      setReact(0 - reaction['downvote']);
    } else if (isLike && !isDislike) {
      setReact(reaction['upvote']);
    }
  }, [idea]);

  const fetchMoreComment = async () => {
    if (comments.length >= props.pageInfo.totalResults) {
      setHasMore(false)
      return;
    }
    if (comments.length > 0) {
      setShowSkeleton(true)
      const res = await ApiService.getComments({
        level: 1,
        category: idea?._id,
        page: nextPage,
        limit: 5,
        sortBy: "-createdAt"
      });
      const {results} = res;
      setNextPage(nextPage + 1);
      setComments((state: any) => [...state, ...results]);
      setShowSkeleton(false)
    }
  }

  const submitComment = async () => {
    setLoading(true)
    try {
      let data = {
        content: value,
        category: idea?._id,
        model: "Idea"
      }
      if (type !== "") {
        data = Object.assign({privacy: "anonymous"}, data);
      }
      const res = await ApiService.addComment(data);
      if (res.status) {
        notification.error({
          message: res.data.message
        })
        return setLoading(false)
      }
      setComments((state: any) => [{...res.comment}, ...state]);
      setValue("");
      setType("");
      setAnonymous(false);
      setEditor(false);
      setLoading(false);
    } catch (err) {
      console.log(err);
    }
  }


  const removeFile = async (attachmentId: any) => {
    return (
      confirm({
        title: 'Are you sure remove this attachment?',
        content: 'This action can not undo, so do you want to delete?',
        okText: 'Yes',
        okType: 'danger',
        okButtonProps: {
          disabled: false,
        },
        cancelText: 'No',
        onOk: async () => {
          try {
            const res = await ApiService.removeAttachment({attachment: attachmentId});
            if (res.status) {
              return notification.error({
                message: res.data.message
              })
            }
            const newLists = [...attachments].filter(attachment => attachment._id !== res._id);
            setAttachments(newLists);
            notification.success({
              message: "Remove Attachment Success"
            });
          } catch (err) {
            console.log(err);
          }
        },
      })
    )
  }

  const VoteIdea = ({userReaction, setUserReaction, setReact, react, idea}) => {
    const [sending, setSending] = useState(false);
    return (
      <div className="d-flex flex-column align-items-center justify-content-center">
        <div onClick={async () => {
          if (sending) return;
          try {
            setSending(true);
            if (userReaction === "upvote") {
              const res = await ApiService.voteIdea(idea.slug, {active: "false"});
              if (res.idea) {
                setUserReaction("");
                setReact((state: any) => state - 1);
                setSending(false);
              }
            } else {
              const res = await ApiService.voteIdea(idea.slug, {
                type: 'upvote'
              });
              if (res.idea) {
                setUserReaction('upvote');
                if (userReaction === 'downvote') {
                  setReact((state: any) => state + 2);
                } else setReact((state: any) => state + 1);
                setSending(false);
              }
            }
          } catch (err) {
            setSending(false);
          }
        }}>
          <Tooltip title="Upvote">
            <a>
              <Typography.Text>
                <CaretUpOutlined style={{fontSize: "200%", color: userReaction === "upvote" ? "#3E79F7" : "#455560"}}/>
              </Typography.Text>
            </a>
          </Tooltip>
        </div>
        {react}
        <div onClick={async () => {
          try {
            if (userReaction === 'downvote') {
              const res = await ApiService.voteIdea(idea.slug, {active: "false"});
              if (res.idea) {
                setUserReaction("");
                setReact((state: any) => state + 1);
              }
            } else {
              const res = await ApiService.voteIdea(idea.slug, {
                type: 'downvote'
              });
              if (res.idea) {
                setUserReaction('downvote');
                if (userReaction === 'upvote') {
                  setReact((state: any) => state - 2);
                } else setReact((state: any) => state - 1);
              }
            }
          } catch (err) {
          }
        }}>
          <Tooltip title="Downvote">
            <a>
              <Typography.Text>
                <CaretDownOutlined
                  style={{fontSize: "200%", color: userReaction === "downvote" ? "#3E79F7" : "#455560"}}/>
              </Typography.Text>
            </a>
          </Tooltip>
        </div>
      </div>
    )
  }

  const EllipsisDropDown = (props: any) => {
    const {setAttachments, content, setContent} = props;
    const [form] = Form.useForm();
    const [mode, setMode] = useState("");
    const [modalVisible, setModalVisible] = useState(false);
    const [modalLoading, setModalLoading] = useState(false);
    const [editorLoading, setEditorLoading] = useState(false)
    const [value, setValue] = useState(content)

    const onFinish = async () => {
      setModalLoading(true)
      if (mode === "add") {
        let newForm = new FormData();
        let files = form.getFieldValue("attachments")?.fileList || [];
        files.forEach(attachment => {
          newForm.append("attachments", attachment.originFileObj);
        })
        newForm.append("idea", idea.slug);
        if (form.getFieldValue("attachments") && form.getFieldValue("attachments")?.fileList.length > 0) {
          await axios.post('/api/attachments/add', newForm, {
            headers:
              {
                'Content-Type': 'multipart/form-data',
              }
          }).then(res => {
            if (res.status !== 200) {
              notification.error({
                message: res.data.message
              });
              return setModalLoading(false)
            }
            setAttachments((state: any) => {
              return [...state, ...res.data]
            });
            notification.success({
              message: res.data["message"] ?? "Add Attachment Successfully"
            });
            form.resetFields();
            setMode("");
            setModalLoading(false);
            newForm.delete("attachments");
            newForm.delete("idea");
          }).catch(err => {
            console.log('err', err);
            notification.error({
              message: err ?? "Add Attachment Failed"
            });
            setModalLoading(false);
          })
        }
      }
      if (mode === "update") {
        const res = await ApiService.updateIdea(slug, {content: value});
        if (res.status) {
          notification.error({
            message: res.data.message
          })
        }
        setContent(res.idea.content);
        setMode("");
        setModalLoading(false);
        form.resetFields();
        notification.success({
          message: res.message
        })
      }
    }

    const removeAllFile = async (slug: any) => {
      return (
        confirm({
          title: 'Are you sure remove all attachments?',
          content: 'This action can not undo, so do you want to delete?',
          okText: 'Yes',
          okType: 'danger',
          okButtonProps: {
            disabled: false,
          },
          cancelText: 'No',
          onOk: async () => {
            try {
              const res = await ApiService.removeAllAttachments({idea: slug});
              if (res.code === 400) {
                return notification.error({
                  message: "Can't Remove Attachments"
                })
              } else {
                setAttachments([]);
                return notification.success({
                  message: "Remove Attachment(s) Successfully"
                });
              }

            } catch (err) {
              console.log(err)
            }
          },
        })
      )
    }

    return (
      <div>
        <Dropdown
          placement="bottomRight"
          overlay={
            <Menu>
              {
                user._id === idea?.author?._id && (
                  <>
                    <Menu.Item key={"2"} icon={<FileAddOutlined/>} onClick={() => {
                      setModalVisible(true);
                      setMode("add");
                    }}>
                      Add Attachments
                    </Menu.Item>
                    <Menu.Item key={"4"} icon={<FormOutlined/>} onClick={() => {
                      setModalVisible(true);
                      setMode("update");
                      setEditorLoading(true);
                      setTimeout(() => {
                        setEditorLoading(false);
                      }, 1000)
                    }}>
                      Update Idea
                    </Menu.Item>
                  </>
                )
              }
              {
                attachments.length > 0 && (
                  <>
                    {
                      (user.role.name === "manager" || user._id === idea?.author?._id) && (
                        <Menu.Item key={"3"} icon={<DeleteOutlined/>} onClick={() => removeAllFile(idea.slug)}>
                          Remove All Attachments
                        </Menu.Item>
                      )
                    }
                    <Menu.Item key={"1"} icon={<DownloadOutlined/>}>
                      <a href={`/api/ideas/export/zip?slug=${idea.slug}`}>
                        Download Zip File
                      </a>
                    </Menu.Item>
                  </>
                )
              }
            </Menu>
          }
          trigger={['click']}
        >
          <div className="ellipsis-dropdown">
            <EllipsisOutlined/>
          </div>
        </Dropdown>
        <Modal
          okButtonProps={{form: 'form-submit', htmlType: 'submit'}}
          title={mode === "add" ? "Update Attachments" : "Update Idea"}
          visible={modalVisible}
          onCancel={() => setModalVisible(false)}
          okText={"Save"}
          confirmLoading={modalLoading}
        >
          <Form
            id="form-submit"
            form={form}
            layout={"vertical"}
            onFinish={onFinish}
          >
            {
              mode === "add" ? (
                <Form.Item
                  name="attachments"
                  label="Attachments"
                  rules={[{required: true, message: "Attachment(s) is required!"}]}
                >
                  <Upload.Dragger multiple accept=".doc,.docx,.pdf">
                    <p className="ant-upload-drag-icon">
                      <FolderOpenOutlined/>
                    </p>
                    <p className="ant-upload-text">Post File</p>
                    <p className="ant-upload-hint">
                      Drag your file here
                    </p>
                  </Upload.Dragger>
                </Form.Item>
              ) : (
                <Form.Item name="content" label="Content">
                  <Skeleton avatar paragraph={{rows: 4}} loading={editorLoading}>
                    <Editor ref={editorRef} initialValue={value} height="200px" onChange={() => {
                      setValue(editorRef.current.getInstance().getMarkdown())
                    }}/>
                  </Skeleton>
                </Form.Item>
              )
            }
          </Form>
        </Modal>
      </div>
    )
  }

  const CommentReply = (props: any) => {
    const {reply, commentReply, setCommentReply} = props;
    const [value, setValue] = useState("");
    const [loading, setLoading] = useState(false);
    const [saveLoading, setSaveLoading] = useState(false);
    const [editor, setEditor] = useState(false);

    const getReply = async (replyId: any) => {
      try {
        const res = await ApiService.getComment(replyId);
        if (res.status) {
          notification.error({
            message: res.data.message
          })
          return setEditor(false);
        }
        setValue(res.content)
      } catch (err) {
        console.log('error', err)
      }
    }

    const updateReply = async (replyId: any) => {
      setSaveLoading(true);
      try {
        const res = await ApiService.updateComment(replyId, {
          content: value
        });
        if (res.status) {
          notification.error({
            message: res.data.message
          })
          return setSaveLoading(false)
        }
        const newComments = commentReply.map((reply: any) => {
          if (res.comment._id === reply._id) {
            return res.comment
          } else {
            return reply
          }
        });
        setCommentReply(newComments);
        setValue("");
        setEditor(false);
        setSaveLoading(false);
      } catch (err) {
        console.log(err);
        setSaveLoading(false);
      }
    }

    const showDeleteConfirm = (replyId: any) => {
      return (
        confirm({
          title: 'Are you sure delete this comment?',
          content: 'This action can not undo, so do you want to delete?',
          okText: 'Yes',
          okType: 'danger',
          okButtonProps: {
            disabled: false,
          },
          cancelText: 'No',
          onOk: async () => {
            try {
              const res = await ApiService.deleteComment(replyId, {model: "Comment"});
              if (res.status) {
                return notification.error({
                  message: res.data.message
                })
              }
              const newLists = [...commentReply].filter(reply => reply._id !== res.comment._id);
              setCommentReply(newLists);
              notification.success({
                message: res.message
              });
            } catch (err) {
              notification.error({message: err ? err : "Some error"})
            }
          },
        })
      )
    };
    return (
      <Card className={"mt-4"}>
        <Row gutter={16} className="d-flex flex-row flex-grow-1 justify-content-between pr-2 py-2">
          <div className="ml-3" style={{width: "75%"}}>
            <div style={{
              whiteSpace: 'nowrap',
              overflow: 'hidden',
              textOverflow: 'ellipsis'
            }}>
              <Avatar src={reply.author.avatar}/> &nbsp;
              <strong>{reply.author.fullName}</strong> &nbsp;&nbsp;
              <span>{`${moment(reply.createdAt).fromNow()}`}</span>
            </div>
          </div>
          <div className="ml-3">
            {
              (reply.author._id === user._id || user.role.permissions.includes("MANAGE_ALL_COMMENT")) && (
                <Dropdown
                  overlay={
                    <Menu>
                      <div key={reply._id}>
                        {
                          reply.author._id === user._id && (
                            <Menu.Item
                              key={"2"}
                              icon={<EditOutlined/>}
                              onClick={() => {
                                setEditor(true);
                                setLoading(true);
                                setTimeout(() => {
                                  setLoading(false)
                                }, 2000);
                                getReply(reply._id).then((_: any) => {
                                })
                              }}>
                              Update Reply
                            </Menu.Item>
                          )
                        }
                        <Menu.Item onClick={() => showDeleteConfirm(reply._id)} key={"3"} icon={<DeleteOutlined/>}>
                          Delete Reply
                        </Menu.Item>
                      </div>

                    </Menu>
                  }
                  trigger={['click']}
                >

                  <div className="ellipsis-dropdown">
                    <EllipsisOutlined/>
                  </div>
                </Dropdown>
              )
            }
          </div>
        </Row>
        <Row className="ml-1 mt-3">
          <ReactMarkdown>
            {
              reply.content
            }
          </ReactMarkdown>
        </Row>
        {
          editor && (
            <div className="mb-2 mt-2">
              <Skeleton avatar paragraph={{rows: 4}} loading={loading}>
                <Editor ref={editorRef} initialValue={value} height="200px" onChange={() => {
                  setValue(editorRef.current.getInstance().getMarkdown())
                }}/>
                <Row style={{
                  display: "flex",
                  justifyContent: "flex-end",
                  alignItems: "center"
                }} className="mb-4">
                  <Col>
                    <Button
                      type="default"
                      className="mt-3 mr-2"
                      onClick={() => {
                        setEditor(false);
                        setValue("");
                      }}
                    >
                      Cancel
                    </Button>
                  </Col>
                  <Col>
                    <Button
                      type="primary"
                      className="mt-3"
                      htmlType="submit"
                      loading={saveLoading}
                      style={{float: "right"}}
                      disabled={value === "" ? true : false}
                      onClick={() => {
                        updateReply(reply._id).then((_: any) => {
                        });
                      }}
                    >
                      Save
                    </Button>
                  </Col>
                </Row>
              </Skeleton>
            </div>
          )
        }
      </Card>
    )
  }

  const CommentMain = ({comment, idea}: any) => {
    const [commentReply, setCommentReply] = useState([]);
    const [hasNextPage, setHasNextPage] = useState(true);
    const [page, setPage] = useState(1);
    const [replyValue, setReplyValue] = useState("");
    const [replyEditor, setReplyEditor] = useState(false);
    const [replyLoading, setReplyLoading] = useState(false);
    const [submitLoading, setSubmitLoading] = useState(false);
    const [mode, setMode] = useState("");
    const [type, setType] = useState("");
    const [anonymous, setAnonymous] = useState(false);

    const getComment = async (commentId: any) => {
      try {
        const res = await ApiService.getComment(commentId);
        if (res.status) {
          notification.error({
            message: res.data.message
          })
          return setReplyEditor(false);
        }
        setReplyValue(res.content)
      } catch (err) {
        console.log('error', err)
      }
    }

    const updateComment = async (commentId: any) => {
      setSubmitLoading(true);
      try {
        const res = await ApiService.updateComment(commentId, {
          content: replyValue
        });
        if (res.status) {
          notification.error({
            message: res.data.message
          })
          return setSubmitLoading(false)
        }
        const newComments = comments.map((comment: any) => {
          if (res.comment._id === comment._id) {
            return res.comment
          } else {
            return comment
          }
        });
        setComments(newComments);
        setMode("");
        setReplyValue("");
        setReplyEditor(false);
        setSubmitLoading(false);
      } catch (err) {
        console.log(err);
        setSubmitLoading(false);
      }
    }

    const submitReply = async () => {
      setSubmitLoading(true)
      let data = {
        content: replyValue,
        category: idea?._id,
        replyFor: comment._id,
        model: "Idea"
      }
      if (type !== "") {
        data = Object.assign({privacy: "anonymous"}, data)
      }
      const res = await ApiService.addComment(data);
      if (res.status) {
        notification.error({
          message: res.data.message
        })
        return setSubmitLoading(false)
      }
      if (commentReply.length === 0) {
        await loadMoreReply()
      } else {
        setCommentReply((state: any) => [{...res.comment}, ...state]);
      }
      setType("");
      setAnonymous(false);
      setReplyValue("");
      setReplyEditor(false);
      setSubmitLoading(false);
    }

    const showDeleteConfirm = (commentId: any) => {
      return (
        confirm({
          title: 'Are you sure delete this comment?',
          content: 'This action can not undo, so do you want to delete?',
          okText: 'Yes',
          okType: 'danger',
          okButtonProps: {
            disabled: false,
          },
          cancelText: 'No',
          onOk: async () => {
            try {
              const res = await ApiService.deleteComment(commentId, {model: "Comment"});
              if (res.status) {
                return notification.error({
                  message: res.data.message
                })
              }
              const newLists = [...comments].filter(comment => comment._id !== res.comment._id);
              setComments(newLists);
              notification.success({
                message: res.message
              });
            } catch (err) {
              notification.error({message: err ? err : "Some error"})
            }
          },
        })
      )
    };

    const loadMoreReply = async () => {
      try {
        const res = await ApiService.getComments({
          level: 2,
          replyFor: comment._id,
          page,
          limit: 5,
          sortBy: "-createdAt"
        });
        const {results, totalPages} = res;
        setHasNextPage(page < totalPages);
        if (res.page < totalPages) {
          setPage(res.page + 1);
        }
        setCommentReply((state: any) => {
          return [...state, ...results]
        });
      } catch (err) {
        console.log(err)
      }
    }
    return (
      <Card className={"mt-3"} bordered={false}>
        <Row gutter={16} className="d-flex flex-row flex-grow-1 justify-content-between pr-2 py-2">
          <div className="mt-2 ml-3" style={{width: "80%"}}>
            <div style={{
              whiteSpace: 'nowrap',
              overflow: 'hidden',
              textOverflow: 'ellipsis'
            }}>
              <Avatar src={comment.author.avatar}/> &nbsp;
              <strong>{comment.author.fullName}</strong> &nbsp;&nbsp;
              <span>{`${moment(comment.createdAt).fromNow()}`}</span>
            </div>
          </div>
          {
            (comment.author._id === user._id || user.role.permissions.includes("MANAGE_ALL_COMMENT")) && (
              <div className="ml-3">
                <Dropdown
                  placement="bottomRight"
                  overlay={
                    <Menu>
                      <div key={comment._id}>
                        {
                          comment.author._id === user._id && (
                            <Menu.Item
                              key={"2"}
                              icon={<EditOutlined/>}
                              onClick={() => {
                                setMode("update")
                                setReplyEditor(true)
                                setReplyLoading(true)
                                setTimeout(() => {
                                  setReplyLoading(false);
                                }, 2000)
                                getComment(comment._id).then((_: any) => {
                                })
                              }}
                            >
                              Update Comment
                            </Menu.Item>
                          )
                        }
                        <Menu.Item
                          key={"3"}
                          icon={<DeleteOutlined/>}
                          onClick={() => showDeleteConfirm(comment._id)}
                        >
                          Delete Comment
                        </Menu.Item>
                      </div>
                    </Menu>
                  }
                  trigger={['click']}
                >
                  <div className="ellipsis-dropdown">
                    <EllipsisOutlined/>
                  </div>
                </Dropdown>
              </div>
            )
          }
        </Row>
        <Row className="ml-1 mt-3">
          <ReactMarkdown>
            {
              comment.content
            }
          </ReactMarkdown>
        </Row>

        <div
          className='d-flex align-items-center my-1 position-relative'
          style={{fontSize: 13, fontWeight: 500}}
        >
          {
            (idea?.topic?.finalClosureDate > new Date().getTime() && user.role.name === "staff") && (
              <a>
                <p
                  className='mb-0 button-reply'
                  style={{transform: 'translateY(-1px)', fontWeight: "bold"}}
                  onClick={() => {
                    setMode("add");
                    setReplyEditor(!replyEditor);
                    setReplyLoading(true);
                    setTimeout(() => {
                      setReplyLoading(false);
                    }, 2000)
                  }}
                >
                  <RightOutlined style={{fontWeight: "bold"}} className="mt-2"/> Reply
                </p>
              </a>
            )
          }

        </div>
        <div>
          {
            replyEditor && (
              <div className="mb-2 mt-2">
                {
                  idea?.topic?.finalClosureDate > new Date().getTime() && (
                    <Skeleton avatar paragraph={{rows: 4}} loading={replyLoading}>
                      <Editor ref={editorRef} initialValue={replyValue} height="200px" onChange={() => {
                        setReplyValue(editorRef.current.getInstance().getMarkdown())
                      }}/>
                      <Row style={{
                        display: "flex",
                        justifyContent: "flex-end",
                        alignItems: "center"
                      }}>
                        <Col className={"mr-2 mt-2"}>
                          {
                            mode !== "update" && (
                              <Switch
                                checked={anonymous}
                                checkedChildren="Anonymous"
                                unCheckedChildren="Normal"
                                onChange={(checked) => {
                                  setAnonymous(checked)
                                  if (!anonymous) {
                                    setType("privacy");
                                  } else {
                                    setType("");
                                  }
                                }}
                              />
                            )
                          }
                        </Col>
                        <Col>
                          <Button
                            type="default"
                            className="mt-3 mr-2"
                            onClick={() => {
                              setReplyEditor(false);
                              setMode("");
                              setReplyValue("");
                            }}
                          >
                            Cancel
                          </Button>
                        </Col>
                        <Col>
                          <Button
                            type="primary"
                            className="mt-3"
                            htmlType="submit"
                            disabled={replyValue === "" ? true : false}
                            loading={submitLoading}
                            style={{float: "right"}}
                            onClick={() => {
                              if (mode === "add") {
                                submitReply().then((_: any) => {
                                });
                              } else {
                                updateComment(comment._id).then((_: any) => {
                                });
                              }
                            }}
                          >
                            {mode === "add" ? "Submit" : "Save"}
                          </Button>
                        </Col>
                      </Row>
                    </Skeleton>
                  )
                }
              </div>
            )
          }
        </div>
        <div>
          {commentReply.length > 0 && commentReply.map(reply => {
            return (
              <CommentReply
                key={reply._id}
                reply={reply}
                commentReply={commentReply}
                setCommentReply={setCommentReply}
              />
            )
          })
          }
        </div>
        {
          (comment.subCommentCount > 0 && hasNextPage) && (
            <a
              onClick={() => {
                loadMoreReply().then(_ => {
                })
              }}
            >Views More Comment(s)</a>
          )
        }
      </Card>
    )
  }

  return (
    <div>
      <Spin spinning={showIdea} tip={"Loading...."}>
        <h1 className={"mt-2"}>{idea?.title}</h1>
        <div className={"d-flex align-items-center"} style={{width: "100%"}}>
          <VoteBox emoji={EyeOutlined} viewCount={idea?.viewCount}/>
          <Typography.Text style={{
            whiteSpace: 'nowrap',
            overflow: 'hidden',
            textOverflow: 'ellipsis'
          }}>
            <a className={"mr-1"}>
              <strong>
                {idea?.author?.fullName}
              </strong>
            </a>
            started this idea in topic <strong>{idea?.topic?.title}</strong>.
          </Typography.Text>
        </div>
        <Row className={"mt-4"} gutter={16}>
          <Col xs={24} md={16}>
            <Card>
              <div className="d-flex flex-row">
                {
                  user.role.name === "staff" && (
                    <VoteIdea
                      idea={idea}
                      react={react}
                      setReact={setReact}
                      setUserReaction={setUserReaction}
                      userReaction={userReaction}
                    />
                  )
                }
                <div>
                </div>
                <div className="flex-grow-1 pl-4">
                  <Row
                    style={{
                      display: "flex",
                      alignItems: "center",
                      justifyContent: "space-between"
                    }}
                    className={"idea-header mb-3"}
                    gutter={16}
                  >
                    <Col xs={20}>
                      <Avatar src={idea?.author?.avatar} size={28} className={"mr-2"}/>
                      <strong className={"mr-2"}>{idea.author?.fullName}</strong>
                      <span>{`${moment(idea?.createdAt).fromNow()}`}</span>
                    </Col>
                    <Col xs={4} style={{display: "flex", justifyContent: "flex-end"}}>
                      {
                        (attachments.length > 0 || user._id === idea?.author?._id) && (
                          <EllipsisDropDown content={content} setContent={setContent} setAttachments={setAttachments}/>
                        )
                      }
                    </Col>
                  </Row>
                  <Typography.Text>
                    <ReactMarkdown>
                      {content}
                    </ReactMarkdown>
                  </Typography.Text>
                  {
                    attachments?.length > 0 && (
                      <div style={{height: "38%", overflowX: "hidden", overflowY: "scroll"}}>
                        <Row gutter={10}>
                          {
                            attachments.map((item: any) =>
                              (
                                <Col key={item._id} xl={5} lg={7} md={9} sm={7} xs={15}>
                                  <Tooltip title={item?.originalName}>
                                    <Card bodyStyle={{display: "flex"}}>
                                      <div style={{display: "flex", alignItems: "center", width: "85%"}}>
                                        <FileFilled style={{fontSize: '200%'}}/>
                                        <span
                                          style={{
                                            fontWeight: "bold",
                                            whiteSpace: "nowrap",
                                            overflow: "hidden",
                                            textOverflow: "ellipsis",
                                            width: "50px"
                                          }}
                                          className="ml-2"
                                        >
                                          {item?.originalName}
                                        </span>
                                      </div>
                                      {
                                        (user.role.name === "manager" || user._id === idea?.author?._id) && (
                                          <div className="ml-3 mt-1">
                                            <Tooltip title="Remove">
                                              <DeleteOutlined onClick={async () => {
                                                await removeFile(item._id)
                                              }}/>
                                            </Tooltip>
                                          </div>
                                        )
                                      }
                                    </Card>
                                  </Tooltip>
                                </Col>
                              ))
                          }
                        </Row>
                      </div>
                    )
                  }
                  <div className="d-flex mt-1">
                    <EmojiBox idea={idea}/>
                  </div>
                </div>
              </div>
            </Card>

            <Divider dashed/>
            <div className="d-flex justify-content-between align-items-center mb-4">
              <div className="d-flex flex-grow-1 align-items-center">
                <h2>{idea?.commentCount} Comment(s)</h2>
              </div>
              {
                (!editor && user.role.name === "staff" && idea?.topic?.finalClosureDate > new Date().getTime()) && (
                  <Button
                    type="primary"
                    className="d-flex justify-content-end align-items-end"
                    onClick={() => {
                      setEditor(true);
                      setEditorLoading(true)
                      if (value === "") {
                        setTimeout(() => {
                          setEditorLoading(false);
                        }, 2500);
                      }
                    }}
                  >
                    Add Comment
                  </Button>
                )
              }
            </div>
            {
              editor && (
                <Skeleton avatar paragraph={{rows: 4}} loading={editorLoading}>
                  <Editor ref={editorRef} initialValue={value} height="200px" onChange={() => {
                    setValue(editorRef.current.getInstance().getMarkdown())
                  }}/>
                  <Row style={{
                    display: "flex",
                    justifyContent: "flex-end",
                    alignItems: "center"
                  }}>
                    <Col className={"mr-4 mt-3"}>
                      <Switch
                        checked={anonymous}
                        checkedChildren="Anonymous"
                        unCheckedChildren="Normal"
                        onChange={(checked) => {
                          setAnonymous(checked)
                          if (!anonymous) {
                            setType("privacy");
                          } else {
                            setType("");
                          }
                        }}
                      />
                    </Col>
                    <Col>
                      <Button
                        type="default"
                        className="mt-3 mr-2"
                        onClick={() => {
                          setEditor(false);
                          setType("");
                        }}
                      >
                        Cancel
                      </Button>
                    </Col>
                    <Col>
                      <Button
                        type="primary"
                        className="d-flex justify-content-end mt-3 mr-2"
                        loading={loading}
                        disabled={value === "" ? true : false}
                        onClick={() => {
                          if (editor && value !== '') {
                            submitComment().then(_ => {
                              setValue("");
                              setType("")
                              setEditor(false);
                            });
                          }
                        }}
                      >
                        Submit
                      </Button>
                    </Col>
                  </Row>
                </Skeleton>
              )
            }
            <InfiniteScroll
              dataLength={comments.length}
              next={fetchMoreComment}
              hasMore={comments.length > 0 ? hasMore : !hasMore}
              loader={<Skeleton avatar paragraph={{rows: 4}} active={showSkeleton}/>}
            >
              {
                comments?.map((comment: any) => {
                  return (
                    <CommentMain
                      idea={idea}
                      key={comment._id}
                      comment={comment}
                    />
                  )
                })
              }
            </InfiniteScroll>
          </Col>
          <Col md={8} xs={24}>
            <h5>Campaign</h5>
            <div className={"mt-1"}>
              <Tag
                className="edit-tag"
                closable={false}
                style={{
                  borderRadius: 50
                }}
                color={idea?.topic?.finalClosureDate < new Date().getTime() ? "#ed5e4e" : "#71eba1"}
              >
                {idea?.topic?.title}
              </Tag>
            </div>
            <Divider/>
            <h5>Tags</h5>
            {
              idea?.tags?.map((tag: any, index: any) => {
                return (
                  <Tag
                    key={tag._id + index}
                    className="edit-tag"
                    closable={false}
                    style={{
                      borderRadius: 50
                    }}
                  >
                    {tag.title}
                  </Tag>
                )
              })
            }
            <Divider/>
            {
              user.role.permissions.includes("EXPORT_IDEA") && (
                <div>
                  <h5>Download CSV File</h5>
                  <a href={`/api/ideas/export/csv?slug=${slug}`}>
                    <Button className={"mb-2"} size={"small"} icon={<DownloadOutlined/>} block>Download</Button>
                  </a>
                  <Divider/>
                </div>
              )
            }
            <h5>Events</h5>
            <Timeline>
              <Timeline.Item>{`Idea created on ${moment(idea?.createdAt).format("DD-MM-YYYY")}`}</Timeline.Item>
              <Timeline.Item>
                {
                  idea?.topic?.firstClosureDate > new Date().getTime()
                    ? `Campaign will close posting idea on ${moment(idea?.topic?.firstClosureDate).format("DD-MM-YYYY")}`
                    : `Campaign closed posting idea on ${moment(idea?.topic?.firstClosureDate).format("DD-MM-YYYY")}`
                }
              </Timeline.Item>
              <Timeline.Item>
                {
                  idea?.topic?.finalClosureDate > new Date().getTime()
                    ? `Idea will close posting comment on ${moment(idea?.topic?.finalClosureDate).format("DD-MM-YYYY")}`
                    : `Idea closed posting comment on ${moment(idea?.topic?.finalClosureDate).format("DD-MM-YYYY")}`
                }
              </Timeline.Item>
            </Timeline>
          </Col>
        </Row>
      </Spin>
    </div>
  );
};

export const getServerSideProps = withSessionSsr(
  async function getServerSideProps(context): Promise<any> {
    const {req, res} = context;
    const {slug} = context.query;
    try {
      const user = await auth(req.session as SessionData, []);
      const idea = await getIdeaByFilter({slug: slug}, user._id);
      const comments = await queryComments({category: idea._id, level: 1}, {
        page: 1,
        limit: 5,
        sortBy: "-createdAt"
      }, user);
      return {
        props: {
          idea: JSON.parse(JSON.stringify(idea)),
          comments: JSON.parse(JSON.stringify(comments?.results)),
          pageInfo: {
            totalResults: comments?.totalResults,
            hasNextPage: comments?.page < comments?.totalPages
          }
        }
      }
    } catch (err) {
      console.log('err', err)
      res.statusCode = err["statusCode"] || 500;
      return {
        redirect: {
          permanent: false,
          destination: (err.statusCode === 401) ? '/auth/login' : '/',
        },
      }
    }
  }
)

export default SinglePost;
