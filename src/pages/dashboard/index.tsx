import {Badge, Card, Col, notification, Row, Spin} from "antd";
import {useSelector} from "react-redux";
import Flex from "common/components/shared-components/Flex";
import {FileDoneOutlined, EyeOutlined, UserSwitchOutlined, BookOutlined} from "@ant-design/icons";
import {RootState} from "common/redux/store";
import AvatarStatus from 'common/components/shared-components/AvatarStatus';
import {COLORS} from "common/constants/ChartConstant";
import {ApiService} from "common/services/ApiService";
import {useEffect, useState} from "react";
import moment from "moment";
import dynamic from "next/dynamic";
import {withSessionSsr} from "server/utils/with-session";
import {auth} from "server/utils/auth";
import {SessionData} from "server/interfaces/types";
import {queryTopics} from "server/services/topic.service";
import {queryUsers} from "server/services/user.service";
import {ideaViews} from "server/services/statistic.service";
import {queryIdeas} from "server/services/idea.service";

const DonutChartWidget = dynamic(() => import('common/components/shared-components/DonutChartWidget'), {
  ssr: false
});
const ChartWidget = dynamic(() => import('common/components/shared-components/ChartWidget'), {
  ssr: false
});
const DataDisplayWidget = dynamic(() => import('common/components/shared-components/DataDisplayWidget'), {
  ssr: false
});

const WeeklyRevenue = () => {
  const [ideas, setIdeas] = useState([]);
  const [authors, setAuthors] = useState([]);
  const [loading, setLoading] = useState(false);
  const startDate = moment().startOf('week').format('DD/MM/YYYY');
  const endDate = moment().endOf('week').format('DD/MM/YYYY');
  const idea = [{
    name: "Created",
    data: ideas
  }, {
    name: "Author",
    data: authors
  }];
  const getWeeklyViews = async () => {
    setLoading(true);
    try {
      const res = await ApiService.ideaTopic({});
      let newArrIdeas = res?.week?.days.map(t => t.created);
      let newArrAuthors = res?.week?.days.map(t => t.authors);
      setIdeas(newArrIdeas);
      setAuthors(newArrAuthors);
      setLoading(false)
    } catch (err) {
      notification.error({
        message: err ?? "Something error!"
      })
      setLoading(false)
    }
  }

  useEffect(() => {
    getWeeklyViews().then((_: any) => {
    });
  }, [])

  const {direction} = useSelector((state: RootState) => state.theme);
  return (
    <Spin spinning={loading} tip={"Loading..."}>
      <Card>
        <Col xs={24} sm={24} md={24} lg={8}>
          <Flex className="h-100" flexDirection="column" justifyContent="between">
            <div>
              <h4 className="mb-0">Weekly Idea Created</h4>
              <span className="text-muted">{`${startDate} - ${endDate}`}</span>
            </div>
          </Flex>
        </Col>
        <ChartWidget
          card={false}
          series={idea}
          xAxis={['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']}
          title="New Ideas"
          height={260}
          width={'100%'}
          type="line"
          customOptions={{colors: COLORS}}
          direction={direction} extra={undefined} bodyClass={undefined}/>
      </Card>
    </Spin>
  )
}

const DisplayDataSet = ({viewTotal, topicTotal, ideaTotal, userTotal}) => {
  return (
    <Row gutter={16}>
      <Col xs={24} sm={24} md={24} lg={12} xl={12} xxl={12}>
        <DataDisplayWidget
          icon={<FileDoneOutlined/>}
          value={ideaTotal}
          title="Total Ideas"
          color="cyan"
          vertical={true}
          avatarSize={55}
        />
        <DataDisplayWidget
          icon={<UserSwitchOutlined/>}
          value={userTotal}
          title="Total Users"
          color="gold"
          vertical={true}
          avatarSize={55}
        />
      </Col>
      <Col xs={24} sm={24} md={24} lg={12} xl={12} xxl={12}>
        <DataDisplayWidget
          icon={<BookOutlined/>}
          value={topicTotal}
          title="Total Campaign"
          color="volcano"
          vertical={true}
          avatarSize={55}
        />
        <DataDisplayWidget
          icon={<EyeOutlined/>}
          value={viewTotal}
          title="Total Views"
          color="blue"
          vertical={true}
          avatarSize={55}
        />
      </Col>
    </Row>
  )
}

const NumbersOfIdea = () => {
  const [ideaNumber, setIdeaNumber] = useState([]);
  const [topics, setTopics] = useState([]);
  const [loading, setLoading] = useState(false)
  const sessionColor = [COLORS[0], COLORS[1], COLORS[3], COLORS[4], COLORS[5]]

  const getTopics = async () => {
    setLoading(true)
    try {
      const res = await ApiService.topTopicIdeas({});
      let newArrTopic = res.map(t => t.title);
      let newArrIdea = res.map(t => t.ideaCount);
      setTopics(newArrTopic);
      setIdeaNumber(newArrIdea);
      setLoading(false)
    } catch (err) {
      notification.error({
        message: err ?? "Something error!"
      })
      setLoading(false)
    }
  }

  useEffect(() => {
    getTopics().then((_: any) => {
    })
  }, []);

  const jointSessionData = () => {
    let arr = []
    if (topics?.length > 0) {
      for (let i = 0; i < ideaNumber.length; i++) {
        const data = ideaNumber[i];
        const label = topics[i];
        const color = sessionColor[i]
        arr = [...arr, {
          data: data,
          label: label,
          color: color
        }]
      }
    }
    return arr
  }

  const conbinedSessionData = jointSessionData();

  return (
    <Spin spinning={loading} tip={"Loading...."}>
      <DonutChartWidget
        series={ideaNumber}
        labels={topics}
        title="Number of Idea"
        customOptions={{colors: sessionColor}}
        bodyClass="mb-2 mt-3"
        extra={
          <Row justify="center">
            <Col xs={20} sm={20} md={20} lg={24}>
              <div className="mt-4 mx-auto" style={{maxWidth: 200}}>
                {conbinedSessionData.map(elm => (
                  <Flex alignItems="center" justifyContent="between" className="mb-3" key={elm.label}>
                    <div>
                      <Badge color={elm.color}/>
                      <span className="text-gray-light">{elm.label}</span>
                    </div>
                    <span className="font-weight-bold text-dark">{elm.data}</span>
                  </Flex>
                ))}
              </div>
            </Col>
          </Row>
        }
      />
    </Spin>
  )
}

const TopIdeaView = () => {
  const [ideas, setIdeas] = useState<any>([]);
  const [loading, setLoading] = useState(false);
  const getTopIdeaViews = async () => {
    setLoading(true)
    try {
      const res = await ApiService.topIdeasViews({limit: 5});
      setIdeas(res.total)
      setLoading(false)
    } catch (err) {
      notification.error({
        message: err ?? "Something error!"
      });
      setLoading(false)
    }
  }

  useEffect(() => {
    getTopIdeaViews().then((_: any) => {
    })
  }, []);

  return (
    <Spin spinning={loading} tip={"Loading...."}>
      <Card
        title="Top Idea's View"
      >
        {ideas?.ideas?.map(elm => (
          <Flex className="w-100 py-3" justifyContent="between" alignItems="center" key={elm.name}>
            <AvatarStatus shape="square" src={elm.author.avatar} name={elm.title} subTitle={elm.topic.title}/>
            <Flex>
              <div className="mr-3 text-right">
                <span className="text-muted">Views</span>
                <div className="mb-0 h5 font-weight-bold">
                  {elm.viewCount}
                </div>
              </div>
            </Flex>
          </Flex>
        ))}
      </Card>
    </Spin>
  )
}

const Dashboards = ({viewTotal, topicTotal, ideaTotal, userTotal}) => {
  return (
    <>
      <Row gutter={16}>
        <Col xs={24} sm={24} md={24} lg={16} xl={15} xxl={14}>
          <WeeklyRevenue/>
        </Col>
        <Col xs={24} sm={24} md={24} lg={8} xl={9} xxl={10}>
          <DisplayDataSet viewTotal={viewTotal} topicTotal={topicTotal} ideaTotal={ideaTotal} userTotal={userTotal}/>
        </Col>
      </Row>
      <Row gutter={16}>
        <Col xs={24} sm={24} md={24} lg={16} xl={15} xxl={14}>
          <TopIdeaView/>
        </Col>
        <Col xs={24} sm={24} md={24} lg={8} xl={9} xxl={10}>
          <NumbersOfIdea/>
        </Col>
      </Row>
    </>
  )
}

export const getServerSideProps = withSessionSsr(
  async function getServerSideProps({req, res}): Promise<any> {
    try {
      const user = await auth(req.session as SessionData, ["GET_STATISTIC"]);
      const topics = await queryTopics({deleted: {$ne: true}}, {
        page: 1,
        limit: 10,
        sortBy: "createdAt"
      });
      const users = await queryUsers({deleted: {$ne: true}}, {
        page: 1,
        limit: 10,
        sortBy: "createdAt"
      });
      const ideas = await queryIdeas({deleted: {$ne: true}}, {page: 1, limit: 10, sortBy: "createdAt"}, user);
      const views = await ideaViews({});
      return {
        props: {
          viewTotal: JSON.parse(JSON.stringify(views?.total?.views)),
          topicTotal: JSON.parse(JSON.stringify(topics?.totalResults)),
          ideaTotal: JSON.parse(JSON.stringify(ideas?.totalResults)),
          userTotal: JSON.parse(JSON.stringify(users?.totalResults)),
        }
      }
    } catch (err) {
      res.statusCode = err["statusCode"] || 500;
      return {
        redirect: {
          permanent: false,
          destination: (err.statusCode === 401) ? '/auth/login' : '/',
        },
      }
    }
  }
);


export default Dashboards