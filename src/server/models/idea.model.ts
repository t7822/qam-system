import {Schema, model, models} from 'mongoose';
import {toJSON, paginate} from 'server/models/plugins';
import ideaConfig from 'server/config/idea.config';
import {slugify} from "server/utils/slugify";
import Comment from "server/models/comment.model";
import UserActivity from "server/models/user-activity.model";
import Reaction from "server/models/reaction.model";
import {IIdea, IdeaModel} from "./interfaces/idea.interface";

const ideaSchema = new Schema<IIdea>({
  author: {
    type: Schema.Types.ObjectId,
    ref: "User"
  },
  title: {
    type: String,
    required: true,
  },
  slug: {
    type: String,
  },
  content: {
    type: String,
    required: true,
  },
  topic: {
    type: Schema.Types.ObjectId,
    ref: "Topic",
  },
  privacy: {
    type: String,
    enum: ideaConfig.privacy.values,
    default: ideaConfig.privacy.default,
  },
  editedAt: {
    type: String,
  },
  editedTimes: {
    type: Number,
  },
  isAttachmentsChanged: {
    type: Boolean,
    default: true,
    private: true
  },
  deleted: {
    type: Boolean,
    default: false,
    private: true
  },
  createdAt: {
    type: Number,
    default: Date.now(),
  }
}, {
  collection: "ideas",
  toJSON: {virtuals: true},
  toObject: {virtuals: true},
});

// add plugin that converts mongoose to json
ideaSchema.plugin(toJSON);
ideaSchema.plugin(paginate);

ideaSchema.virtual("tags", {
  ref: "IdeaTag",
  localField: "_id",
  foreignField: "idea"
});

ideaSchema.virtual("comments", {
  ref: "Comment",
  localField: "_id",
  foreignField: "category"
});

ideaSchema.statics.slugGenerator = async function (ideaTitle: string): Promise<string> {
  let newSlug = slugify(ideaTitle);
  let count = 0;
  while (await this.exists({slug: newSlug})) {
    newSlug = `${slugify(ideaTitle)}_${++count}`;
  }
  return newSlug;
};

ideaSchema.pre('save', async function (next) {
  const idea = this;
  if (idea.isModified("title")) {
    idea.slug = await Idea.slugGenerator(idea.title);
  }
  next();
});

// will call before remove method => cascading delete all blogs model references
ideaSchema.pre("deleteOne", async function (next) {
  const idea = this;
  await Comment.deleteMany({category: idea._id});
  await UserActivity.deleteMany({target: idea._id});
  await Reaction.deleteMany({target: idea._id});
  next();
});

const Idea = models.Idea as IdeaModel || model<IIdea, IdeaModel>('Idea', ideaSchema);

export default Idea;
