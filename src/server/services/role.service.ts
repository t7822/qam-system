import httpStatus from "http-status";
import {User, Role} from "server/models"
import ApiError from "server/utils/api-error";
import config from "server/config/role.config";
import {IRole} from "server/models/interfaces/role.interface";

/**
 * Create new Role
 * @param {Object} roleBody - Role's object body
 * @return {Promise}
 */
export const createRole = async (roleBody: any) => {
  return Role.create(roleBody);
}

export const getUsers = async (permission: string, filter: any, options: any) => {
  const roles = (await Role.find({permissions: `${permission.toUpperCase()}`})).map(role => role._id);

  if (roles.length === 0) {
    throw new ApiError(httpStatus.NOT_FOUND, `Cannot find role with permission ${permission}`);
  }
  Object.assign(options, {populate: 'role', filter: filter ?? {}});
  if (!options.limit) options.limit = await User.countDocuments({role: {$in: roles}});
  return User.paginate({role: {$in: roles}}, options);
}

export const queryRoles = async (filter: any, options: any) => {
  return Role.paginate(filter, options);
}

export const getRoleByFilter = async (filter: any): Promise<IRole> => {
  let role = await Role.findOne(filter);
  if (!role) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Role not found');
  }
  return role;
}

export const updateRole = async (filter: any, updateBody: any): Promise<IRole> => {
  const role = await getRoleByFilter(filter);
  Object.assign(role, updateBody);
  await role.save();
  return role;
};

export const deleteRole = async (filter: any,): Promise<IRole> => {
  const role = await Role.findOne(filter);
  if (!role) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Role not found');
  }

  await role.updateOne({deleted: true});
  return role;
}

export const getPermissions = () => {
  return config.PERMISSIONS;
}
