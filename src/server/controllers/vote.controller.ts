import { voteService, userActivityService } from 'server/services';
import { ExtendedRequest, ExtendedResponse } from "server/interfaces/types";
import { Types } from "mongoose";
// import { sendNotificationEmail } from 'server/services/email.service';
// import { notificationEmailContent } from 'server/config/email.config';

export const getVotes = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const result = await voteService.getVotes(req.query.idea as string);
  res.json(result);
};

export const updateVote = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const result = await voteService.updateVote(req.query.idea as string, req.session.user?._id, req.body);
  if (result.message) {
    // if (req.session.user?._id.toString() !== result.target.author.toString()) await sendNotificationEmail(notificationEmailContent("test", `${req.session.user?.fullName} added a new comment on your idea!`), { _id: result.target.author });
    await userActivityService.createUserActivity({
      message: "has" + result.message.slice("You have".length),
      detail: result.type,
      target: result.idea._id,
      model: "Idea",
      user: req.session.user?._id
    });
  }
  res.json(result);
};