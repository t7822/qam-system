import React, {useEffect, useState} from "react";
import {SmileOutlined} from "@ant-design/icons";
import {Popover, Typography} from "antd";
import {ApiService} from "../../../services/ApiService";

export const VoteBox = (props: any) => {
  let Emoji;
  if (props.emoji) {
    Emoji = props.emoji;
  }
  return (
    <a className={"vote-box"} style={{
      display: "flex",
      flexDirection: "row",
      borderRadius: 50,
      border: "1px solid #00000011",
      padding: "2px 8px",
      textAlign: "center",
      alignItems: "center",
      justifyContent: "center",
      minWidth: 52,
      fontSize: 14,
      marginRight: 8,
    }}>
      <Typography.Text>
        <Emoji style={props?.style}/> {props?.viewCount}
      </Typography.Text>
    </a>
  );
}

export const EmojiBox = ({idea}) => {
  const [reaction, setReaction] = useState();
  useEffect(() => {
    if (idea && idea.userReaction) {
      setReaction(idea?.userReaction);
    }
  }, [idea]);

  const addReaction = async (type: any) => {
    try {
      const res = await ApiService.reactIdea(idea._id, {type: type, model: "Idea", active: "true", reactTo: "Idea"});
      setReaction(res.reaction.type)
    } catch (err) {
      console.log('err');
    }
  }
  // TODO: customize emoji box
  return (
    <div className="d-flex flex-row">
      <Popover placement={"topLeft"} overlayClassName={"emoji-picker"} content={<div className={"slot-container"}>
        <a onClick={async () => {
          await addReaction("%F0%9F%98%82")
        }} className={"slot"}>
          😂
        </a>
        <a onClick={async () => {
          await addReaction('%F0%9F%98%9E')
        }} className={"slot"}>
          😞
        </a>
        <a onClick={async () => {
          await addReaction('%F0%9F%8E%89');
        }} className={"slot"}>
          🎉
        </a>
        <a onClick={async () => {
          await addReaction('%F0%9F%A4%94');
        }} className={"slot"}>
          🤔
        </a>
        <a onClick={async () => {
          await addReaction('%F0%9F%A4%AA');
        }} className={"slot"}>
          🤪
        </a>
      </div>}
      >
        <a className={"emoji-box"} style={{
          display: "flex",
          flexDirection: "row",
          borderRadius: 50,
          border: "1px solid #00000011",
          padding: "2px 4px",
          textAlign: "center",
          alignItems: "center",
          justifyContent: "center",
          width: 32,
          fontSize: 12,
        }}>
          <Typography.Text>
            {reaction === undefined ? <SmileOutlined/> : (
              decodeURIComponent(reaction)
            )}
          </Typography.Text>
        </a>
      </Popover>
    </div>
  )
}
