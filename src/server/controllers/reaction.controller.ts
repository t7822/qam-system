import { reactionService, userActivityService } from 'server/services';
import { ExtendedRequest, ExtendedResponse } from "server/interfaces/types";
import { Types } from "mongoose";
// import { sendNotificationEmail } from 'server/services/email.service';
// import { notificationEmailContent } from 'server/config/email.config';

export const getReactions = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const result = await reactionService.getReactions(new Types.ObjectId(req.query.targetId as string));
  res.json(result);
};

export const updateReaction = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const result = await reactionService.updateReaction(new Types.ObjectId(req.query.targetId as string), req.session.user?._id, req.body);
  if (result.message) {
    // if (req.session.user?._id.toString() !== result.target.author.toString()) await sendNotificationEmail(notificationEmailContent("test", `${req.session.user?.fullName} added a new comment on your idea!`), { _id: result.target.author });
    await userActivityService.createUserActivity({
      message: "has" + result.message.slice("You have".length),
      detail: result.reaction.type,
      target: result.target,
      model: (!req.body.reactTo) ? req.body.model : req.body.reactTo,
      user: req.session.user?._id
    });
  res.json(result);
  }
};