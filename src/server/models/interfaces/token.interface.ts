import {Model} from "mongoose";
import {IUser} from "./user.interface";

export interface TokenInterface extends Document{
  token: string,
  user: IUser["_id"],
  type: string,
  expires: Date,
  blacklisted: boolean
}

export interface IToken extends TokenInterface {
}

export interface TokenModel extends Model<IToken> {
}
