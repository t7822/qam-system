import {Schema, model, models} from 'mongoose';
import {paginate, toJSON} from "server/models/plugins";
import reactionConfig from "server/config/reaction.config";
import {IReaction, ReactionModel} from "./interfaces/reaction.interface";

const reactionSchema = new Schema<IReaction>({
  user: {
    type: Schema.Types.ObjectId,
    ref: "User",
    required: true
  },
  target: {
    type: Schema.Types.ObjectId,
    required: true
  },
  type: {
    type: String,
    enum: reactionConfig.type,
    default: reactionConfig.default,
  },
  active: {
    type: Boolean,
    default: true,
  },
  activatedAt: {
    type: Number,
    default: Date.now()
  }
}, {
  collection: 'reactions',
  timestamps: true
});

reactionSchema.plugin(paginate);
reactionSchema.plugin(toJSON);

/**
 * @typedef Reaction
 */
const Reaction = models.Reaction || model<IReaction, ReactionModel>(
  "Reaction",
  reactionSchema
);

export default Reaction;