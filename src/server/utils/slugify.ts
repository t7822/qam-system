const {models} = require("mongoose");

export const slugify = (str: string) => {
  return str.normalize('NFD').replace(/[\u0300-\u036f]/g, '').replace(/đ/g, 'd').replace(/Đ/g, 'D').replace(/ /g, '_').toLowerCase().replace(/[^\w-]+/g, '');
}

export const slugGenerator = async (name: string, model: string): Promise<string> => {
  let newSlug = slugify(name);
  let count = 0;
  while (await models[`${model}`].exists({slug: newSlug})) {
    newSlug = `${slugify(name)}_${++count}`;
  }
  return newSlug;
};