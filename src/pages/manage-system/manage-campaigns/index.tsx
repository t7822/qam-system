import {
  Button,
  Tooltip,
  Modal,
  Card,
  Table,
  Spin,
  Tag,
  Form,
  DatePicker,
  Input,
  notification,
  Pagination,
  Select
} from "antd";
import {DeleteOutlined, EditOutlined, PlusCircleOutlined, DownloadOutlined} from "@ant-design/icons";
import React, {useEffect, useState} from "react";
import ManageSystem from "../index";
import {ApiService} from "common/services/ApiService";
import moment from "moment";
import {withSessionSsr} from "server/utils/with-session";
import {auth} from "server/utils/auth";
import {SessionData} from "server/interfaces/types";
import {queryTopics} from "server/services/topic.service";
import {queryDepartments} from "server/services/department.service";

const {confirm} = Modal;
const {RangePicker} = DatePicker;

const ManageTopics = (props: any) => {
  const [form] = Form.useForm();
  const [visible, setVisible] = useState(false);
  const [mode, setMode] = useState('');
  const [topics, setTopics] = useState(props.topics || []);
  const [pageInfo, setPageInfo] = useState(props.pageInfo || {});
  const [slug, setSlug] = useState("");
  const [submitLoading, setSubmitLoading] = useState(false);
  const [loading, setLoading] = useState(false);
  const [modalLoading, setModalLoading] = useState(false);

  useEffect(() => {
    if (mode === "edit") {
      if (slug !== "") {
        (async () => {
          await getTopic()
        })()
      }
    } else {
      form.resetFields
    }
  }, [mode, slug]);

  const handleChange = async (value: any) => {
    setLoading(true)
    const res = await ApiService.getTopics({
      page: value,
      limit: 10,
      sortBy: "-createdAt"
    });
    setTopics(res.results)
    setPageInfo({
      totalResults: res.totalResults,
      hasNextPage: res?.page < res?.totalPages
    })
    setLoading(false)
  }

  const getTopic = async () => {
    setModalLoading(true)
    const res = await ApiService.getTopic(slug);
    form.setFieldsValue({
      title: res?.title,
      closureDate: [moment(res.firstClosureDate, "x"), moment(res.finalClosureDate, "x")],
      department: res?.department?._id
    })
    setModalLoading(false)
  }

  const handleTopic = async () => {
    setSubmitLoading(true);
    // @ts-ignore
    const values = form?.getFieldsValue("closureDate");
    let data = {
      title: form?.getFieldValue("title"),
      firstClosureDate: values?.closureDate[0].format("x"),
      finalClosureDate: values?.closureDate[1].format("x"),
      department: form?.getFieldValue("department")
    }

    if (mode === "add") {
      const res = await ApiService.addTopic(data);
      if (res.status) {
        notification.error({
          message: res.data.message
        })
      }
      const newTopics = [...topics, res.topic];
      setTopics(newTopics);
      setVisible(false);
      setMode('');
      form.resetFields();
      notification.success({
        message: res.message
      })
      setSubmitLoading(false)
    } else {
      const res = await ApiService.updateTopic(slug, data);
      if (res.status) {
        notification.error({
          message: res.data.message
        })
      }
      const newTopics = topics.map((topic: any) => {
        if (res?.topic?._id === topic._id) {
          return res.topic
        } else {
          return topic
        }
      });
      setTopics(newTopics);
      setVisible(false);
      setMode('');
      setSlug("");
      form.resetFields();
      notification.success({
        message: res.message
      })
      setSubmitLoading(false)
    }
  }

  const columns: any = [
    {
      title: "Title",
      align: "center",
      dataIndex: "title",
      key: "title",
      render: (record: any) => {
        return (
          <span>{record}</span>
        )
      }
    }, {
      title: "Author",
      align: "center",
      dataIndex: "author",
      key: "author",
      render: (record: any) => {
        return (
          <Tag>{record.fullName}</Tag>
        )
      }
    }, {
      title: "Department",
      align: "center",
      dataIndex: "department",
      key: "department",
      render: (record: any) => {
        return (
          <Tag>{record.name}</Tag>
        )
      }
    }, {
      title: "First Closure Date",
      align: "center",
      dataIndex: "firstClosureDate",
      key: "firstClosureDate",
      render: (record: any) => {
        return (
          <span>{moment(record, "x").format("dddd, DD/MM/YYYY, h:mm:ss a")}</span>
        )
      }
    }, {
      title: "Final Closure Date",
      align: "center",
      dataIndex: "finalClosureDate",
      key: "finalClosureDate",
      render: (record: any) => {
        return (
          <span>{moment(record, "x").format("dddd, DD/MM/YYYY, h:mm:ss a")}</span>
        )
      }
    }, {
      title: "Slug",
      align: "center",
      dataIndex: "slug",
      key: "slug",
    }, {
      title: "Action",
      dataIndex: "action",
      align: "center",
      key: "action",
      render: (_: any, record: any) => (
        <div className="text-right d-flex justify-content-center">
          <Tooltip title="Download CSV File">
            <a href={`/api/ideas/export/csv?topic=${record.slug}`}>
              <Button type="default" className="mr-2" icon={<DownloadOutlined/>} size="small"/>
            </a>
          </Tooltip>
          <Tooltip title="Update">
            <Button type="default" className="mr-2" icon={<EditOutlined/>} size="small"
                    onClick={() => {
                      setVisible(true)
                      setMode("edit")
                      setSlug(record.slug)
                    }}/>
          </Tooltip>
          <Tooltip title="Delete">
            <Button type="primary" danger icon={<DeleteOutlined/>} onClick={() => showDeleteConfirm(record.slug)}
                    size="small"/>
          </Tooltip>
        </div>
      )
    }
  ];

  const showDeleteConfirm = (slug: any) => {
    return (
      confirm({
        title: 'Are you sure delete this Campaign?',
        content: 'This action can not undo, so do you want to delete?',
        okText: 'Yes',
        okType: 'danger',
        cancelText: 'No',
        onOk: async () => {
          const res = await ApiService.deleteTopic(slug);
          if (res.status) {
            return notification.error({
              message: res.data.message
            })
          }
          const newTopics = [...topics].filter(topic => topic._id !== res.topic._id)
          setTopics(newTopics)
          notification.success({
            message: res.message
          })
        },
      })
    )
  }

  return (
    <ManageSystem>
      <div>
        <div className="mb-3" style={{
          display: "flex",
          justifyContent: "space-between"
        }}>
          <Button type="primary" icon={<PlusCircleOutlined/>} onClick={() => {
            setMode("add");
            setVisible(true);
          }}>
            Add Campaign
          </Button>
        </div>
        <Card>
          <div className="table-responsive">
            <Spin spinning={loading} tip="Loading....">
              <Table
                rowKey={(record) => record.slug}
                columns={columns}
                dataSource={topics}
                pagination={false}
                footer={() => {
                  return (
                    <Pagination
                      showQuickJumper
                      defaultCurrent={1}
                      total={pageInfo.totalResults}
                      onChange={handleChange}
                    />
                  )
                }}
              />
            </Spin>
          </div>
        </Card>
      </div>
      <Modal
        okButtonProps={{form: 'campaign-form-submit', htmlType: 'submit'}}
        visible={visible}
        onCancel={() => {
          setVisible(false);
          setMode("");
          setSlug("");
          form.resetFields();
        }}
        okText={mode === "edit" ? "Save" : "Submit"}
        title={mode === "edit" ? "Edit Campaign" : "Add Campaign"}
        confirmLoading={submitLoading}
        width={500}
        bodyStyle={{
          paddingTop: "8px"
        }}
      >
        <Spin spinning={modalLoading} tip={"Loading...."}>
          <Form
            id="campaign-form-submit"
            form={form}
            preserve={false}
            layout="vertical"
            onFinish={handleTopic}
          >
            <Form.Item
              name="title"
              label="Title"
              rules={[{
                required: true,
                message: "Title is required"
              }]}
            >
              <Input placeholder="Enter Campaign Title..."/>
            </Form.Item>
            <Form.Item
              name="department"
              label="Department"
              rules={[{required: true, message: 'Please select department!'}]}
            >
              <Select placeholder="Please select department">
                {props?.departments.map((department: any, index: number) => {
                  return (
                    <Select.Option style={{textTransform: "capitalize"}} value={department._id}
                                   key={department._id + index}><span
                      style={{textTransform: "capitalize"}}>{department.name}</span></Select.Option>
                  )
                })}
              </Select>
            </Form.Item>
            <Form.Item name="closureDate" label="Closure Date"
                       rules={[{required: true, message: 'Please select Closure Date!'}]}
            >
              <RangePicker
                className="w-100"
                placeholder={["First Closure Date", "Final Closure Date"]}
                ranges={{
                  Today: [moment().startOf('day'), moment().endOf('day')],
                  'This Month': [moment().startOf('month'), moment().endOf('month')],
                }}
                showTime
                format="YYYY/MM/DD HH:mm:ss"
              />
            </Form.Item>
          </Form>
        </Spin>
      </Modal>
    </ManageSystem>
  )
}

export const getServerSideProps = withSessionSsr(
  async function getServerSideProps({req, res}): Promise<any> {
    try {
      await auth(req.session as SessionData, ["MANAGE_ALL_TOPIC"]);
      const topics = await queryTopics({deleted: {$ne: true}}, {page: 1, limit: 10, sortBy: "-createdAt"});
      const departments = await queryDepartments({deleted: {$ne: true}}, {page: 1, limit: 10, sortBy: "-createdAt"});
      return {
        props: {
          topics: JSON.parse(JSON.stringify(topics?.results)),
          departments: JSON.parse(JSON.stringify(departments?.results)),
          pageInfo: {
            totalResults: topics?.totalResults,
            hasNextPage: topics?.page < topics?.totalPages
          }
        }
      }
    } catch (err) {
      res.statusCode = err["statusCode"] || 500;
      return {
        redirect: {
          permanent: false,
          destination: (err.statusCode === 401) ? '/auth/login' : '/',
        },
      }
    }
  }
);

export default ManageTopics
