import User from 'server/models/user.model';
import Role from 'server/models/role.model';
import Comment from 'server/models/comment.model';
import Reaction from 'server/models/reaction.model';
import UserActivity from 'server/models/user-activity.model';
import Idea from "server/models/idea.model";
import Tag from 'server/models/tag.model';
import IdeaTag from 'server/models/idea-tag.model';
import UserIdea from 'server/models/user-idea.model';
import Topic from "server/models/topic.model";
import View from "server/models/view.model";
import Attachment from "server/models/attachment.model";
import Vote from "server/models/vote.model";
import Department from "server/models/department.model";

export {
  User,
  Role,
  Comment,
  Reaction,
  UserActivity,
  IdeaTag,
  Tag,
  Idea,
  UserIdea,
  Topic,
  View,
  Attachment,
  Vote,
  Department,
};
