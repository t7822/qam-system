import Joi from 'joi';
import {password, ObjectId} from "server/validations/custom.validations";

export const addUser: {body: object} = {
  body: Joi.object().keys({
    username: Joi.string().required(),
    email: Joi.string().email().required(),
    password: Joi.string().custom(password).required(),
    fullName: Joi.string(),
    role: Joi.string(),
    gender: Joi.string(),
    phoneNumber: Joi.string(),
    dob: Joi.alternatives().try(Joi.number(), Joi.string()),
    bio: Joi.string(),
    displayName: Joi.string(),
    avatar: Joi.any(),
    department: Joi.string().custom(ObjectId),
  })
};

export const getUsers = {
  query: Joi.object().keys({
    username: Joi.string(),
    displayName: Joi.string(),
    fullName: Joi.string(),
    role: Joi.string(),
    department: Joi.string().custom(ObjectId),
    id: Joi.number(),
    sortBy: Joi.string(),
    limit: Joi.number().integer(),
    page: Joi.number().integer()
  })
};

export const getUser = {
  params: Joi.object().keys({
    username: Joi.string().required()
  })
};

export const updateUser = {
  params: Joi.object().keys({
    username: Joi.string().required()
  }),
  body: Joi.object().keys({
    username: Joi.string(),
    email: Joi.string().email(),
    role: Joi.string(),
    fullName: Joi.string(),
    id: Joi.number(),
    password: Joi.string().custom(password),
    currentPassword: Joi.string().custom(password),
    phoneNumber: Joi.string(),
    dob: Joi.alternatives().try(Joi.number(), Joi.string()),
    gender: Joi.string(),
    bio: Joi.string(),
    displayName: Joi.string(),
    avatar: Joi.any(),
  })
};

// extends current Joi object
export const updateSelfProfileBody = updateUser.body.keys({currentPassword: Joi.string()})
export const updateSelfProfile = {
  body: updateSelfProfileBody
};

export const deleteUser = {
  params: getUser.params
}