import React, {useEffect, useState} from "react";
import {Button, Col, DatePicker, Form, Input, notification, Row, Select} from "antd";
import {ApiService} from "common/services/ApiService";
import moment from "moment";


const dateFormat = "DD-MM-YYYY";
const {Option} = Select;

const ROW_GUTTER = 16;
const EDIT = "EDIT";

const UserForm = (props: any) => {
  const {mode, user} = props;
  const [form] = Form.useForm();
  const [loading, setLoading] = useState(false)


  useEffect(() => {
    if (mode === EDIT && JSON.stringify(user) !== "{}") {
      form.setFieldsValue({
        ...user,
        dob: user.dob ? moment(parseInt(user.dob)) : undefined,
      })
    }
  }, [user])


  const onSubmit = async (values: any) => {
    setLoading(true)
    if (mode === EDIT) {
      try {
        let res = await ApiService.updateSelfInfo({
          ...values,
          dob: values.dob.format("x")
        });
        if (res.status) {
          notification.error({
            message: res.data.message
          })
          return setLoading(false);

        } else {
          notification.success({message: res.message});
        }
      } catch (err) {
        console.log(err)
      }
    }
    setLoading(false)
  }

  return (
    <Form
      className="user-form"
      layout="vertical"
      onFinish={onSubmit}
      form={form}
    >
      <Row gutter={ROW_GUTTER}>
        <Col xs={24} sm={24} md={12}>
          <Form.Item
            label="Username"
            name="username"
            rules={[
              {
                required: true,
                message: 'Please input your username!'
              },
            ]}
          >
            <Input disabled={mode === EDIT} placeholder="Enter username..."/>
          </Form.Item>
        </Col>
        <Col xs={24} sm={24} md={12}>
          <Form.Item
            label="Email"
            name="email"
            rules={[{
              required: true,
              type: 'email',
              message: 'Please enter a valid email!'
            }]}
          >
            <Input disabled={mode === EDIT} placeholder="Enter email..."/>
          </Form.Item>
        </Col>
      </Row>
      <Row gutter={ROW_GUTTER}>
        <Col xs={24} sm={24} md={12}>
          <Form.Item
            label="Full Name"
            name="fullName"
            rules={[
              {
                required: true,
                message: 'Please input your name!',
              },
            ]}
          >
            <Input placeholder="Enter full name..."/>
          </Form.Item>
        </Col>
        <Col xs={24} sm={24} md={12}>
          <Form.Item
            name="gender"
            label="Gender"
            rules={[
              {
                required: true,
                message: 'Please choose your gender!',
              },
            ]}
          >
            <Select placeholder="Choose gender">
              <Option value="0">Male</Option>
              <Option value="1">Female</Option>
              <Option value="2">Other</Option>
            </Select>
          </Form.Item>
        </Col>
      </Row>
      <Row gutter={ROW_GUTTER}>
        <Col xs={24} sm={24} md={12}>
          <Form.Item
            name="dob"
            label="Date Of Birth"
            rules={[
              {
                required: true,
                message: 'Please input your birthday!',
              },
            ]}
          >
            <DatePicker style={{width: "100%"}} format={dateFormat}/>
          </Form.Item>
        </Col>
        <Col xs={24} sm={24} md={12}>
          <Form.Item
            name="contact"
            label="Contact"
            rules={[
              {
                required: true,
                message: 'Please choose your contact!',
              },
            ]}
          >
            <Input placeholder="Enter contact.."/>
          </Form.Item>
        </Col>
      </Row>
      <Row gutter={ROW_GUTTER}>
        <Col xs={24} sm={24} md={24}>
          <Form.Item
            label="Address"
            name="address"
          >
            <Input placeholder="Enter address.."/>
          </Form.Item>
        </Col>
      </Row>
      <Button type="primary" htmlType="submit" style={{float: "right"}} loading={loading}>
        Save Change
      </Button>
    </Form>
  )
}


export default UserForm;
