import httpStatus from "http-status";
import {Department, Topic} from "server/models"
import ApiError from "server/utils/api-error";
import {ITopic} from "server/models/interfaces/topic.interface";

export const createTopic = async (topicBody: any): Promise<ITopic> => {
  const topic = await Topic.create(topicBody);
  await topic.populate([
      {path: "author", model: "User", select: "fullName username avatar"},
      {path: "department", model: "Department", select: "name slug manager"}
    ])
  return topic;
}

export const queryTopics = async (filter: any, options: any) => {
  if (filter.closureDate === "first") {
    Object.assign(filter, {firstClosureDate: {$gt: new Date().getTime()}});
    delete filter.closureDate
  }

  if (filter.closureDate === "final") {
    Object.assign(filter, {finalClosureDate: {$gt: new Date().getTime()}});
    delete filter.closureDate
  }

  if (filter.closureDate === "closed") {
    Object.assign(filter, {finalClosureDate: {$lt: new Date().getTime()}});
    delete filter.closureDate
  }

  if (filter.department) {
    const department = await Department.findOne({slug: filter.department});
    if (!department) throw new ApiError(httpStatus.NOT_FOUND, 'Department not found');
    filter.department = department._id;
  }

  Object.assign(options, {populate: 'author,department', filter: {author: {model: "User", select: "username fullName avatar"}}});
  return Topic.paginate(filter, options);
}

export const getTopicByFilter = async (filter: any): Promise<ITopic> => {
  if (filter.department) {
    const department = await Department.findOne({slug: filter.department});
    if (!department) throw new ApiError(httpStatus.NOT_FOUND, 'Department not found');
    filter.department = department._id;
  }

  let topic = await Topic.findOne(filter)
    .populate([
      {path: "author", model: "User", select: "fullName username avatar"},
      {path: "department", model: "Department", select: "name slug manager"}
    ])

  if (!topic || topic.deleted) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Campaign not found');
  }
  return topic;
}

export const updateTopic = async (filter: any, updateBody: any): Promise<ITopic> => {
  const topic = await getTopicByFilter(filter);
  Object.assign(topic, updateBody);
  await topic.save();
  await topic.populate([
    {path: "author", model: "User", select: "fullName username avatar"},
    {path: "department", model: "Department", select: "name slug manager"}
  ])
  return topic;
};

export const deleteTopic = async (filter: any): Promise<ITopic> => {
  const topic = await getTopicByFilter(filter);
  await topic.updateOne({deleted: true});
  return topic;
}

export const deleteTopicPermanently = async (filter: any,): Promise<ITopic> => {
  const topic = await getTopicByFilter(filter);
  await topic.deleteOne();
  return topic;
}