import {createAsyncThunk, createSlice, PayloadAction} from '@reduxjs/toolkit';
import {ApiService} from "../../services/ApiService";
import {Schema} from "mongoose"

export interface UserData {
  _id?: Schema.Types.ObjectId,
  username: string,
  fullName?: string,
  dob?: string,
  email?: string,
  contact?: string,
  address?: string,
  avatar?: string,
  createdAt?: string,
  department?: any,
  id?: number,
  role: {
    name?: string,
    permissions: [string]
  }
}

export interface TypeAuthState {
  isLoggedIn: boolean,
  authLoading: boolean,
  user: UserData,
  accessToken: '',
  showAuthMessage: boolean,
  authMessage: string | undefined,
}

const initialState: TypeAuthState = {
  isLoggedIn: false,
  authLoading: false,
  showAuthMessage: false,
  authMessage: '',
  user: {
    username: '',
    fullName: '',
    avatar: '',
    createdAt: '',
    id: 0,
    _id: undefined,
    role: {
      name: '',
      permissions: ['']
    }
  },
  accessToken: ''
};

export interface ILoginData {
  username: string,
  password: string
}

export const doLogin = createAsyncThunk('auth/doLogin', async (loginData: ILoginData) => {
  try {
    const data = await ApiService.login(loginData.username, loginData.password);
    if (data.user) {
      return {
        isLoggedIn: true,
        user: data.user,
        response: {
          message: 'Login success.'
        }
      };
    } else {
      return {
        isLoggedIn: false,
        response: data.data,
      }
    }
  } catch (e) {
    return {
      isLoggedIn: false,
      response: {
        message: 'Unknown error.'
      }
    }
  }
});
export const doSignOut = createAsyncThunk('auth/doSignOut', async () => {
  return await ApiService.signOut();
});

export const authSlice = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    setSessionData(state, action: PayloadAction<any>) {
      state.isLoggedIn = action.payload.isLoggedIn;
      state.user = action.payload.user;
    },
    setShowAuthMessage(state, action: PayloadAction<boolean>) {
      state.showAuthMessage = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(doSignOut.fulfilled, () => {
      location.replace("/auth/login");
    });
    builder.addCase(doLogin.pending, state => {
      state.authLoading = true;
    });
    builder.addCase(doLogin.fulfilled, (state, action) => {
      // Add user to the state array
      state.isLoggedIn = action.payload.isLoggedIn;
      state.authLoading = false;
      if (state.isLoggedIn) {
        state.user = action.payload.user;
        // TODO: redirect

      } else {
        state.showAuthMessage = true;
        state.authMessage = action.payload.response.message;
      }
    })
  },
});

export const {
  setSessionData,
  setShowAuthMessage
} = authSlice.actions

export default authSlice.reducer
