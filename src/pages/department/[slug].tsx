import ForumLayout from "common/layouts/forum-layout";
import ForumHeader from "common/components/forum-components/forum-header";
import {Typography, Row, Col, Input, Card, List, Tag, Pagination, Select, Spin, Button} from "antd";
import {
  SearchOutlined,
  PlayCircleFilled, PlusCircleOutlined, DownloadOutlined
} from '@ant-design/icons';
import Link from "next/link"
import {withSessionSsr} from "server/utils/with-session";
import {auth} from "server/utils/auth";
import {SessionData} from "server/interfaces/types";
import {queryTopics} from "server/services/topic.service";
import {useEffect, useState} from "react";
import moment from "moment";
import {ApiService} from "common/services/ApiService";
import {queryIdeas} from "server/services/idea.service";
import {useRouter} from "next/router";
import {useSelector} from "react-redux";
import {RootState} from "common/redux/store";
import {getDepartmentByFilter} from "server/services/department.service";


const Home = (props: any) => {
  const router = useRouter();
  const {slug} = router.query;
  const {user} = useSelector((state: RootState) => state.auth);
  const {recentIdeas, recentTopics, department} = props;
  const [topics, setTopics] = useState(props.topics || []);
  const [pageInfo, setPageInfo] = useState(props.pageInfo || {});
  const [loading, setLoading] = useState(false);
  const [content, setContent] = useState("");
  const [page, setPage] = useState(1);
  const [filter, setFilter] = useState("");

  const getTopics = async () => {
    setLoading(true)
    if (content !== "") {
      const res = await ApiService.getTopics({
        department: slug,
        title: content,
        closureDate: filter,
        page: page,
        limit: 5,
        sortBy: "-createdAt"
      });
      setTopics(res.results)
      setPageInfo({
        totalResults: res.totalResults,
        hasNextPage: res?.page < res?.totalPages
      })
    } else {
      const res = await ApiService.getTopics({
        department: slug,
        closureDate: filter,
        page: page,
        limit: 5,
        sortBy: "-createdAt"
      });
      setTopics(res.results)
      setPageInfo({
        totalResults: res.totalResults,
        hasNextPage: res?.page < res?.totalPages
      })
    }
    setLoading(false)
  }

  useEffect(() => {
    getTopics().then((_: any) => {
    })
  }, [content, page, filter])

  return (
    <ForumLayout>
      <ForumHeader department={department}/>
      <Row gutter={16}>
        <Col lg={8} md={10} sm={16} xs={15} className={"mb-2"}>
          <Input.Search onSearch={(value) => {
            setContent(value);
            setPage(1);
          }} prefix={<SearchOutlined/>} allowClear size={"small"}
                        placeholder={"Search for topic..."}/>
        </Col>
        <Col lg={3} md={4} sm={8} xs={9}>
          <Select
            size={"small"}
            style={{width: "100%"}}
            defaultValue={""}
            onChange={(value => {
              setFilter(value);
              setPage(1)
            })}
          >
            <Select.Option value="">All</Select.Option>
            <Select.Option value="first">First Closure</Select.Option>
            <Select.Option value="final">Final Closure</Select.Option>
            <Select.Option value="closed">Closed</Select.Option>
          </Select>
        </Col>
        <Col md={0} xs={16} className={'mt-2'}>
          <h5>Campaign</h5>
        </Col>
        <Col lg={13} md={10} sm={8} xs={8}
             style={{display: "flex", justifyContent: "flex-end"}}
             className="mb-2"
        >
          {
            (user?.department?._id === department?._id && user?.role.name === "user") && (
              <Button href="/idea/new" icon={<PlusCircleOutlined/>} size={"small"} type={"primary"}>
                New ideas
              </Button>
            )
          }
        </Col>
      </Row>

      <Row gutter={32}>
        <Col xs={24} md={18}>
          <Spin spinning={loading} tip={"Loading...."}>
            <List
              size="large"
              split={false}
              dataSource={topics}
              renderItem={(item: any) => <Card bordered={false}>
                <div className={"forum-list-item"}>
                  <div className={"item-main-content"}>
                    <div className={"list-item-topic"}>
                      <div className={"list-item-topic-outer"}>
                        💬
                      </div>
                    </div>
                    <div className={"list-item-meta"}>
                      <Link href={`/topic/${item.slug}`}>
                        <a className={"title"}>
                          <strong>
                            <Typography.Text>
                              {item.title}
                            </Typography.Text>
                          </strong>
                        </a>
                      </Link>
                      <div className={"description"}>
                        First closure: {moment(item.firstClosureDate, "x").format('LL')},
                        Final closure: {moment(item.finalClosureDate, "x").format('LL')}
                      </div>
                    </div>
                  </div>
                  <div className={"item-extra-content"}>
                    <div className="ml-4">
                      <Tag
                        className="edit-tag mb-2"
                        closable={false}
                        style={{
                          borderRadius: 50
                        }}
                      >
                        {moment(item.finalClosureDate, "x").fromNow()}
                      </Tag>
                      <Tag className="edit-tag ml-2"
                           closable={false}
                           style={{
                             borderRadius: 50
                           }}
                           color={item.finalClosureDate < new Date().getTime() ? "#ed5e4e" : "#71eba1"}
                      >
                        {item.finalClosureDate < new Date().getTime() ? "CLOSE" : "OPEN"}
                      </Tag>
                    </div>
                    <div>

                    </div>
                  </div>
                </div>
              </Card>}
            />
          </Spin>
          {
            topics.length > 0 && (
              <Pagination
                current={page}
                defaultCurrent={1}
                total={pageInfo.totalResults}
                pageSize={5}
                onChange={(page) => setPage(page)}
              />
            )
          }
        </Col>
        <Col md={6} xs={0}>
          <Card
            title={
              <div style={{textAlign: "center"}}>Recent Campaigns</div>
            }
            size="small"
            bordered={false}
          >
            {
              recentTopics?.map((topic: any) => {
                return (
                  <div key={topic.slug} className="ml-4">
                    <Link href={`/topic/${topic.slug}`}>
                      <a>
                        <p style={{fontWeight: "bold"}}><PlayCircleFilled/> {topic.title}</p>
                      </a>
                    </Link>
                  </div>
                )
              })
            }
          </Card>
          <Card
            className={"mt-2"}
            size="small"
            title={
              <div style={{textAlign: "center"}}>Recent Ideas</div>
            }
            bordered={false}>
            {
              recentIdeas?.map((idea: any) => {
                return (
                  <div key={idea.slug} className="ml-4">
                    <Link href={`/idea/${idea.slug}`}>
                      <a>
                        <p style={{fontWeight: "bold"}}><PlayCircleFilled/> {idea.title}</p>
                      </a>
                    </Link>
                  </div>
                )
              })
            }
          </Card>
        </Col>
      </Row>
    </ForumLayout>
  )
}

export const getServerSideProps = withSessionSsr(
  async function getServerSideProps({req, res, query}): Promise<any> {
    try {
      const user = await auth(req.session as SessionData, []);
      const department = await getDepartmentByFilter({slug: query.slug})
      console.log('department', department);
      const topics = await queryTopics({deleted: {$ne: true}, department: query.slug}, {
        page: 1,
        limit: 5,
        sortBy: "-createdAt"
      });
      const recentIdeas = await queryIdeas({deleted: {$ne: true}}, {page: 1, limit: 3, sortBy: "-createdAt"}, user);
      const recentTopics = await queryTopics({deleted: {$ne: true}}, {page: 1, limit: 3, sortBy: "-createdAt"});
      return {
        props: {
          department: JSON.parse(JSON.stringify(department)),
          topics: JSON.parse(JSON.stringify(topics?.results)),
          recentIdeas: JSON.parse(JSON.stringify(recentIdeas?.results)),
          recentTopics: JSON.parse(JSON.stringify(recentTopics?.results)),
          pageInfo: {
            totalResults: topics?.totalResults,
            hasNextPage: topics?.page < topics?.totalPages
          },
        }
      }
    } catch (err) {
      console.log("error", err);
      res.statusCode = err["statusCode"] || 500;
      return {
        redirect: {
          permanent: false,
          destination: (err.statusCode === 401) ? '/auth/login' : '/',
        },
      }
    }
  }
);

export default Home;
