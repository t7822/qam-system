import React from 'react'
import {connect, useDispatch, useSelector} from 'react-redux'
import {Radio, Switch, Button, message} from 'antd';
import {CopyOutlined} from '@ant-design/icons';
import ColorPicker from 'common/components/shared-components/ColorPicker';
import CopyToClipboard from 'react-copy-to-clipboard';
import NavLanguage from './NavLanguage';
import {
  SIDE_NAV_LIGHT,
  NAV_TYPE_SIDE,
  NAV_TYPE_TOP,
  SIDE_NAV_DARK,
  DIR_RTL,
  DIR_LTR
} from 'common/constants/ThemeConstant';
import {useThemeSwitcher} from "react-css-theme-switcher";
import utils from 'common/utils';
import {RootState} from "../../redux/store";
import {
  changeDirection,
  changeHeaderNavColor,
  changeNavStyle, changeNavType,
  changeTopNavColor,
  switchTheme,
  toggleCollapsedNav
} from "../../redux/slices/theme.slice";

const colorOptions = [
  '#3e82f7',
  '#24a772',
  '#de4436',
  '#924aca',
  '#193550'
]

const ListOption = ({name, selector, disabled, vertical}) => {
  return (
    <div className={`my-4 ${vertical ? '' : 'd-flex align-items-center justify-content-between'}`}>
      <div className={`${disabled ? 'opacity-0-3' : ''} ${vertical ? 'mb-3' : ''}`}>{name}</div>
      <div>{selector}</div>
    </div>
  );
}

export const ThemeConfigurator = () => {
  const {
    navType,
    sideNavTheme,
    navCollapsed,
    topNavColor,
    headerNavColor,
    locale,
    currentTheme,
    direction,
  } = useSelector((state: RootState) => state.theme);
  const isNavTop = navType === NAV_TYPE_TOP;
  const isCollapse = navCollapsed;

  const dispatch = useDispatch();

  const {switcher, themes} = useThemeSwitcher();

  const toggleTheme = (isChecked) => {
    dispatch(changeHeaderNavColor(''));
    const changedTheme = isChecked ? 'dark' : 'light'
    dispatch(switchTheme(changedTheme));
    switcher({theme: themes[changedTheme]});
  };

  const onTopNavColorClick = (value) => {
    dispatch(changeHeaderNavColor(''));
    const {rgb} = value
    const rgba = `rgba(${rgb.r}, ${rgb.g}, ${rgb.b}, ${rgb.a})`
    const hex = utils.rgbaToHex(rgba)
    dispatch(changeTopNavColor(hex));
  }
  const onHeaderNavColorClick = (value) => {
    const {rgb} = value
    const rgba = `rgba(${rgb.r}, ${rgb.g}, ${rgb.b}, ${rgb.a})`
    const hex = utils.rgbaToHex(rgba)
    dispatch(changeHeaderNavColor(hex));
  }

  const onNavTypeClick = (value) => {
    dispatch(changeHeaderNavColor(''));
    if (value === NAV_TYPE_TOP) {
      dispatch(changeTopNavColor(colorOptions[0]));
      dispatch(toggleCollapsedNav(false));
    }
    dispatch(changeNavType(value));
  }

  const genCopySettingJson = (configState) => JSON.stringify(configState, null, 2);

  return (
    <>
      <div className="mb-5">
        <h4 className="mb-3 font-weight-bold">Navigation</h4>
        {
          isNavTop ?
            <ListOption
              name="Top Nav Color:"
              vertical
              selector={<ColorPicker color={topNavColor} colorChange={onTopNavColorClick}/>} disabled={false}
            />
            :
            <ListOption
              name="Header Nav Color:"
              vertical
              selector={
                <ColorPicker color={headerNavColor} colorChange={onHeaderNavColorClick}/>
              }
              disabled={false}
            />
        }

        <ListOption
          name="Navigation Type:"
          selector={
            <Radio.Group
              size="small"
              onChange={e => onNavTypeClick(e.target.value)}
              value={navType}
            >
              <Radio.Button value={NAV_TYPE_SIDE}>Side</Radio.Button>
              <Radio.Button value={NAV_TYPE_TOP}>Top</Radio.Button>
            </Radio.Group>
          }
          disabled={false}
          vertical={false}
        />
        <ListOption
          name="Side Nav Color:"
          selector={
            <Radio.Group
              disabled={isNavTop}
              size="small"
              onChange={e => dispatch(changeNavStyle(e.target.value))}
              value={sideNavTheme}
            >
              <Radio.Button value={SIDE_NAV_LIGHT}>Light</Radio.Button>
              <Radio.Button value={SIDE_NAV_DARK}>Dark</Radio.Button>
            </Radio.Group>
          }
          disabled={isNavTop}
          vertical={false}
        />
        <ListOption
          name="Side Nav Collapse:"
          selector={
            <Switch
              disabled={isNavTop}
              checked={isCollapse}
              onChange={() => toggleCollapsedNav(!navCollapsed)}
            />
          }
          vertical={false}
          disabled={isNavTop}
        />
        <ListOption
          name="Dark Theme:"
          selector={
            <Switch checked={currentTheme === 'dark'} onChange={toggleTheme}/>
          }
          disabled={false}
          vertical={false}
        />
        <ListOption
          name="Direction:"
          selector={
            <Radio.Group
              size="small"
              onChange={e => dispatch(changeDirection(e.target.value))}
              value={direction}
            >
              <Radio.Button value={DIR_LTR}>LTR</Radio.Button>
              <Radio.Button value={DIR_RTL}>RTL</Radio.Button>
            </Radio.Group>
          }
          disabled={false}
          vertical={false}
        />
      </div>
      <div className="mb-5">
        <h4 className="mb-3 font-weight-bold">Locale</h4>
        <ListOption
          name="Language:"
          selector={
            <NavLanguage configDisplay/>
          }
          disabled={false}
          vertical={false}
        />
      </div>
      <div>
        <CopyToClipboard
          text={genCopySettingJson({
            navType,
            sideNavTheme,
            navCollapsed,
            topNavColor,
            headerNavColor,
            locale,
            currentTheme,
            direction
          })}
          onCopy={() => message.success('Copy Success, please paste it to src/configs/AppConfig.js THEME_CONFIG variable.')}
        >
          <Button icon={<CopyOutlined/>} block>
            <span>Copy Setting</span>
          </Button>
        </CopyToClipboard>
      </div>
    </>
  )
};

export default ThemeConfigurator;
