import httpStatus from 'http-status';
import {Department, User} from 'server/models';
import ApiError from 'server/utils/api-error';
import {deleteFile} from "server/utils/google-storage";
import {IUser} from "server/models/interfaces/user.interface";
import logger from "server/config/logger.config"

export const createUser = async (userBody: any): Promise<IUser> => {
  if (await User.isFieldTaken({username: userBody.username})) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Username is already taken');
  }

  if (await User.isFieldTaken({email: userBody.email})) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Email is already taken');
  }

  if (userBody.role) {
    userBody.role = await User.handleRole(userBody.role);
  }

  return User.create(userBody);
};

export const queryUsers = async (filter: any, options: any) => {
  if (filter.department) {
    const department = await Department.findOne({slug: filter.department});
    if (!department) throw new ApiError(httpStatus.NOT_FOUND, 'Department not found');
    filter.department = department._id;
  }
  Object.assign(filter, {deleted: {$ne: true}});
  Object.assign(options, {populate: 'role,department', filter: {role: {model: "Role"}}});
  const users = await User.paginate(filter, options);
  if (!users || users.results.length === 0) {
    throw new ApiError(httpStatus.NOT_FOUND, "No user found");
  }
  return users;
};

export const getListUsers = async (filter: any) => {
  // if (filter.role) {
  //   filter.role = await User.handleRole(filter.role)
  // }

  const users = await User.find(filter).lean();
  if (!users || users.length === 0) {
    throw new ApiError(httpStatus.NOT_FOUND, "User not found");
  }
  return users;
}

export const getUserByFilter = async (filter: any, publicFields = ""): Promise<IUser> => {
  if (filter.department) {
    const department = await Department.findOne({slug: filter.department});
    if (!department) throw new ApiError(httpStatus.NOT_FOUND, 'Department not found');
    filter.department = department._id;
  }
  const user = await User.findOne({...filter, deleted: {$ne: true}}).populate({
    path: "role",
    model: "Role"
  }).populate({
    path: "department",
    model: "Department"
  }).select(publicFields);
  if (!user) {
    throw new ApiError(httpStatus.NOT_FOUND, 'User not found');
  }
  return user;
};

export const updateUser = async (filter: any, updateBody: any = {}, file: any): Promise<IUser> => {
  const user = await getUserByFilter(filter);
  if (updateBody.username && await User.isFieldTaken({username: updateBody.username}, user._id)) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Username is already taken');
  }

  if (updateBody.email && await User.isFieldTaken({email: updateBody.email}, user._id)) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Email is already taken');
  }

  if (file) {
    updateBody.avatar = file.linkUrl;
    try {
      if (user.avatar && user.avatar.startsWith("https://storage.googleapis.com/")) await deleteFile(user.avatar);
    } catch (e) {
      logger.error("Failed to delete previous avatar");
    }
  }

  if (updateBody.role) {
    updateBody.role = await User.handleRole(updateBody.role);
  }

  if (updateBody.currentPassword && (updateBody.currentPassword && !(await user.isPasswordMatch(updateBody.currentPassword)))) {
    throw new ApiError(httpStatus.BAD_REQUEST, "Current password is not valid");
  }

  Object.assign(user, updateBody);
  await user.save();
  return user;
};

export const deleteUser = async (filter: any) => {
  const user = await getUserByFilter(filter);
  //await user.deleteOne();
  Object.assign(user, {deleted: true});
  await user.save();
  return user;
}
