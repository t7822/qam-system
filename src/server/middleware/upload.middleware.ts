import {IncomingForm} from 'formidable'
import httpStatus from "http-status";
import ApiError from "server/utils/api-error";
import {errorHandler} from "server/middleware/index";
import path from 'path';
import fs from 'fs';

const options = {
  multiples: true,
  uploadDir: './public/attachments'
};

export default (req, res, extFilter = ['.doc', '.docx', '.pdf'], limitedSize = 10 * 1024 * 1024) => {
  return new Promise(async (resolve, reject) => {
    const form = new IncomingForm(options);
    form.parse(req, async (err, fields, files) => {
      try {
        if (err) {
          throw new ApiError(httpStatus.BAD_REQUEST, "Failed to upload file");
        }
        req.body = fields;

        if (Object.keys(files).length <= 0) return resolve('');
        const fileInfos = [];
        if (files.attachments && !Array.isArray(files.attachments)) {
          files.attachments = [files.attachments]
        }
        for (const file of files.attachments) {
          const {newFilename, originalFilename, filepath, mimetype, size} = file;
          const ext = path.extname(originalFilename).toLowerCase();
          const filename = `${newFilename}_${Date.now()}${ext}`
          if (extFilter.length > 0 && !extFilter.includes(ext)) {
            throw new ApiError(httpStatus.BAD_REQUEST, "Invalid file type");
          }
          if (size > limitedSize) {
            throw new ApiError(httpStatus.BAD_REQUEST, "This file was to large to upload");
          }
          fs.renameSync(filepath, path.join(options.uploadDir, filename));
          fileInfos.push({
            originalName: originalFilename,
            fileName: filename,
            filePath: options.uploadDir + '/' + filename,
            size: size,
            mimeType: mimetype
          })
        }
        req.files = fileInfos;

        resolve('');
      } catch (err) {
        errorHandler(err, req, res);
      }
    });
  });
}