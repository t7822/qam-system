import {ExtendedRequest, ExtendedResponse} from "server/interfaces/types";
import httpStatus from "http-status";
import ApiError from "server/utils/api-error";
import {errorHandler} from "server/middleware/index"
import {IUser} from "../models/interfaces/user.interface";

declare module "iron-session" {
  interface IronSessionData {
    isLoggedIn?: boolean,
    user?: IUser,
    views?: Array<string>,
  }
}

const authMiddleware = (req: ExtendedRequest, res: ExtendedResponse, ...permissions: string[]) => {
  return new Promise<void>((resolve) => {
    try {
      if (!req.session.isLoggedIn || !req.session.user) {
        throw new ApiError(httpStatus.UNAUTHORIZED, 'Not authenticated');
      }

      if (permissions.length > 0) {
        const userPermission = req.session.user?.role.permissions;
        const hasRequiredPermission = permissions.some(permission => userPermission.includes(permission));
        if (!hasRequiredPermission) {
          throw new ApiError(httpStatus.FORBIDDEN, 'Forbidden');
        }
      }
      resolve();
    } catch (err) {
      errorHandler(err, req, res);
    }
  });
}

export default authMiddleware;
