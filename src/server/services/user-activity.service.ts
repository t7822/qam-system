import httpStatus from 'http-status';
import {UserActivity} from 'server/models';
import ApiError from 'server/utils/api-error';
import {IUserActivity} from "server/models/interfaces/user-activity.interface";

export const createUserActivity = async (userActivityBody: any): Promise<IUserActivity> => {
  return UserActivity.create(userActivityBody);
};

export const queryUserActivities = async (user: any, filter: any, options: any) => {
  Object.assign(filter, {user: user._id});
  const userActivities = await UserActivity.paginate(filter, options);
  for (const activity of userActivities.results) {
    await activity.populate({path: "target", model: activity.model, select: "slug username code id"});
    activity.target = activity.target.slug;
    activity.message = user.displayName + " " + activity.message;
  }
  return userActivities;
};

export const getUserActivityByFilter = async (filter: any): Promise<IUserActivity> => {
  const userActivity = await UserActivity.findOne({...filter, deleted: {$ne: true}}).populate({path: "user", model: "User"});
  if (!userActivity) {
    throw new ApiError(httpStatus.NOT_FOUND, 'UserActivity not found');
  }
  return userActivity;
};

export const deleteUserActivity = async (filter: any): Promise<IUserActivity> => {
  const userActivity = await getUserActivityByFilter(filter);
  await userActivity.deleteOne();
  return userActivity;
}
