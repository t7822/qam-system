import {auth, connectDB, validate, errorHandler} from "server/middleware";
import {attachmentController} from "server/controllers";
import middlewares from "server";
import {runMiddleware} from "server/utils/middleware-handler";
import {removeAttachment} from "server/validations/attachment.validation";
import {withIronSessionApiRoute} from "iron-session/next";
import {ironOptions} from "server/config";
import {NextApiHandler, NextApiRequest, NextApiResponse} from "next";

const handler: NextApiHandler = async (req: NextApiRequest, res: NextApiResponse) => {
  await middlewares(req, res);
  await runMiddleware(req, res, auth(req, res));
  await runMiddleware(req, res, validate(req, res, removeAttachment));
  if (req.method === "POST") {
    try {
      await attachmentController.removeAttachment(req, res);
    } catch (err) {
      errorHandler(err, req, res);
    }
  }
};
export default connectDB(withIronSessionApiRoute(handler, ironOptions));
