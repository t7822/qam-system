import {userService} from 'server/services';
import pick from "server/utils/pick";
import {default as defaultURL} from 'server/config/upload.config';
import {Role} from "server/models";
import {ExtendedRequest, ExtendedResponse} from "server/interfaces/types";

export const addUser = async (req: ExtendedRequest, res: ExtendedResponse) => {
  if (!req.file) {
    req.body.avatar = defaultURL.avatar.url;
  } else {
    req.body.avatar = req.file.linkUrl;
  }
  const user = await userService.createUser(req.body);
  res.json({
    message: "Created user successfully",
    user
  });
};

export const getUsers = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const filter: any = pick(req.query, ['displayName', 'fullName', 'username', 'role', 'id']);
  if (filter.role) filter.role = await convertRole(filter.role);
  const options = pick(req.query, ['sortBy', 'limit', 'page']);
  const result = await userService.queryUsers(filter, options);
  res.json(result);
};

export const getPublicUsers = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const filter: any = pick(req.query, ['displayName', 'fullName', 'username', 'role', 'id']);
  if (filter.role) filter.role = await convertRole(filter.role);

  const options = pick(req.query, ['sortBy', 'limit', 'page']);
  Object.assign(options, {select: defaultURL.publicFields});
  const result = await userService.queryUsers(filter, options);
  res.json(result);
};

export const getUser = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const result = await userService.getUserByFilter({username: req.query.username});
  res.json(result);
};

export const getPublicUser = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const result = await userService.getUserByFilter({username: req.query.username}, defaultURL.publicFields);
  res.json(result);
};

export const getSelfInfo = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const result = await userService.getUserByFilter({_id: req.session.user?._id});
  res.json(result);
};

export const updateUser = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const user = await userService.updateUser({username: req.query.username}, req.body, req.file);
  res.json({
    message: "Updated user successfully",
    user
  });
};

export const updateSelfProfile = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const user = await userService.updateUser({username: req.session.user?.username}, req.body, req.file);
  res.json({
    message: "Updated user successfully",
    user
  });
};

export const deleteUser = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const user = await userService.deleteUser({username: req.query.username});
  res.json({
    message: "Deleted user successfully",
    user
  });
};

async function convertRole(name: string) {
  const role = await Role.findOne({name: {$regex: name, $options: 'i'}});
  return (role) ? role._id : null;
}
