import mongoose, {ConnectOptions} from "mongoose";

interface Options extends ConnectOptions {
  useUnifiedTopology: boolean,
  useNewUrlParser: boolean,
}

export const connectDb = () => {
  return new Promise(async (resolve) => {
    if (!mongoose.connection.readyState) {
      console.log("Starting new Mongo DB connection...");
      const options: Options = {
        useUnifiedTopology: true,
        useNewUrlParser: true
      }
      await mongoose.connect(process.env.MONGO_URL, options);
    }
    resolve('');
  });
}
