import dotenv from 'dotenv';
import path from 'path';

dotenv.config({path: path.join(__dirname, '../../../.env.local')});

export const ironOptions = {
  cookieName: "myapp_cookiename",
  password: "complex_password_at_least_32_characters_long",
  // secure: true should be used in production (HTTPS) but can't be used in development (HTTP)
  cookieOptions: {
    secure: process.env.NODE_ENV === "production",
  },
};

export const mongooseConfig = {
  url: process.env.MONGO_URL,
  options: {
    useCreateIndex: true,
    useNewUrlParser: true,
    useUnifiedTopology: true,
  },
};

export default {
  env: process.env.NODE_ENV,
  port: process.env.PORT,
  jwt: {
    secret: process.env.JWT_SECRET,
    accessExpirationMinutes: process.env.JWT_ACCESS_EXPIRATION_MINUTES,
    refreshExpirationDays: process.env.JWT_REFRESH_EXPIRATION_DAYS,
    resetPasswordExpirationMinutes: process.env.JWT_RESET_PASSWORD_EXPIRATION_MINUTES,
    verifyEmailExpirationMinutes: process.env.JWT_VERIFY_EMAIL_EXPIRATION_MINUTES,
  },
  email: {
    smtp: {
      host: process.env.SMTP_HOST,
      port: process.env.SMTP_PORT,
      auth: {
        user: process.env.SMTP_USERNAME,
        pass: process.env.SMTP_PASSWORD,
      },
    },
    url: process.env.EMAIL_SERVER_URL,
    secret: process.env.EMAIL_SERVER_SECRET,
    from: `\"EOF\'s mailer\" ${process.env.EMAIL_SERVER_SENDER}`,
    verifyUrl: (process.env.NODE_ENV === "production") ? "https:eof.vn/api/email/verify/" : "http:localhost:3000/api/email/verify/"
  },
  mailgun: {
    apiKey: process.env.MAILGUN_API_KEY || "",
    domain: process.env.MAILGUN_DOMAIN || "eof.vn"
  },
  gCloud: {
    projectId: process.env.GCLOUD_PROJECT_ID,
    clientEmail: process.env.GCLOUD_CLIENT_EMAIL,
    privateKey: process.env.GCLOUD_PRIVATE_KEY,
    baseUrl: "https://storage.googleapis.com/"
  }
};
