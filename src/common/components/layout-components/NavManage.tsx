import React, {useState} from 'react';
import {FolderOpenOutlined, InfoCircleOutlined, DashboardOutlined} from '@ant-design/icons';
import {connect, useSelector} from "react-redux";
import Link from "next/link";
import {RootState} from "../../redux/store";
import {Dropdown, Menu} from "antd";

const NavManage = () => {
  const {user} = useSelector((state: RootState) => state.auth);
  const [visible, setVisible] = useState(false);

  const handleOnClick = () => {
    setVisible(false)
  }

  const menu = (
    <Menu
      className={"mt-4"}
      mode={"vertical"}
      theme={"light"}
      onClick={handleOnClick}
    >
      <Menu.Item key={'/manage-system/manage-users'}>
        <Link href={user.role.name === "admin" ? `/manage-system/manage-users` : `/manage-system/manage-tags`}>
          <a>
            <InfoCircleOutlined className="mr-3"/>
            <strong>Manage System</strong>
          </a>
        </Link>
      </Menu.Item>
      <Menu.Item key={'/dashboard'}>
        <Link href={`/dashboard`}>
          <a>
            <DashboardOutlined className="mr-3"/>
            <strong>Dashboard</strong>
          </a>
        </Link>
      </Menu.Item>
    </Menu>
  )

  const handleVisibleChange = (value: any) => {
    setVisible(value)
  }

  return (
    <div style={{display: (user.role.name === "staff" || user.role.name === "coordinator") ? "none" : "block"}} key="panel">
      <Dropdown
        placement="bottomRight"
        overlay={menu}
        onVisibleChange={handleVisibleChange}
        visible={visible}
        trigger={['click']}
      >
        <FolderOpenOutlined className="nav-icon mr-0"/>
      </Dropdown>
    </div>
  );
}

const mapStateToProps = ({theme}: { theme: any }) => {
  const {locale} = theme;
  return {locale}
};

export default connect(mapStateToProps)(NavManage);
