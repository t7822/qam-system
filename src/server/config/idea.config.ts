export default {
  privacy: {
    values: ["public", "anonymous"],
    default: "public"
  }
}