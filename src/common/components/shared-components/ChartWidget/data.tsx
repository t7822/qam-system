export const weeklyRevenueData: any = {
  series: [
    {
      name: 'Earning',
      data: [45, 52, 38, 24, 33, 26, 21]
    }
  ],
  categories:[
    '08 Jul',
    '09 Jul',
    '10 Jul',
    '11 Jul',
    '12 Jul',
    '13 Jul',
    '14 Jul'
  ]
}
