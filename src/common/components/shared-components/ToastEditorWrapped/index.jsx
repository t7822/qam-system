import dynamic from 'next/dynamic';
import React, { useRef } from 'react';

const Editor = dynamic(() => import('@toast-ui/react-editor')
  .then(m => m.Editor), { ssr: false });

const ToastEditorWrapped = (props) => {
  const editorRef = useRef(null);
  return (
    <Editor
      initialValue="hello react editor world!"
      previewStyle="vertical"
      height={props.height ? props.height : "200px"}
      initialEditType="wysiwyg"
      useCommandShortcut={true}
      toolbarItems={[
        ['heading', 'bold', 'italic', 'strike'],
        ['hr', 'quote'],
        ['ul', 'ol', 'task', 'indent', 'outdent'],
        ['table', 'image', 'link'],
        ['code', 'codeblock'],
      ]}
      ref={editorRef}
    />
  )
}

export default ToastEditorWrapped;
