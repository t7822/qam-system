import React from 'react'
import { Card } from 'antd';
import { apexPieChartDefaultOption } from 'common/constants/ChartConstant';
import PropTypes from 'prop-types';
import dynamic from "next/dynamic";

const ApexChart = dynamic(() => import('react-apexcharts'), {
	ssr: false
});

const defaultOption = apexPieChartDefaultOption;

const Chart = (props: any) => {
	return (
		<ApexChart {...props} />
	)
}

const DonutChartWidget = (props: any) => {
	const { series, customOptions, labels, width, height, title, extra, bodyClass } = props
	let options = defaultOption
	options.labels = labels
	options.plotOptions.pie.donut.labels.total.label = title
	if(!title) {
		options.plotOptions.pie.donut.labels.show = false
	}
	if(customOptions) {
		options = {...options, ...customOptions }
	}
	return (
		<Card>
			<div className={`text-center ${bodyClass}`}>
				<Chart type="donut" options={options} series={series} width={width} height={height} />
				{extra}
			</div>
		</Card>
	)
}

DonutChartWidget.propTypes = {
	series: PropTypes.array.isRequired,
	labels: PropTypes.array,
	title: PropTypes.string,
	extra: PropTypes.element,
	bodyClass: PropTypes.string
}

DonutChartWidget.defaultProps = {
	series: [],
	labels: [],
	title: '',
	height: 250,
	width: '100%'
};

export default DonutChartWidget
