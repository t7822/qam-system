import {commentService, userActivityService} from 'server/services';
import pick from "server/utils/pick";
import {ExtendedRequest, ExtendedResponse} from "server/interfaces/types";
import {Types} from "mongoose";
import {sendNotificationEmail} from 'server/services/email.service';
import {notificationEmailContent, urls} from 'server/config/email.config';

export const addComment = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const comment = await commentService.addComment(req.body, req.session.user?._id);
  res.json({
    message: "Created Comment successfully",
    comment
  });
  if (req.session.user?._id.toString() !== comment.category.author.toString()) await sendNotificationEmail(notificationEmailContent(urls.redirectUrl + comment.category.slug, `${req.session.user?.fullName} added a new comment on your idea!`), {_id: comment.category.author});
  await userActivityService.createUserActivity({
    message: `has added a new comment`,
    target: comment.category._id,
    detail: comment.content,
    model: req.body.model,
    user: req.session.user?._id
  });
};

export const queryComments = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const filter = pick(req.query, ['category', 'author', 'replyFor', 'level']);
  const options = pick(req.query, ['sortBy', 'limit', 'page']);
  const result = await commentService.queryComments(filter, options, req.session.user);
  res.json(result);
};

export const getComment = async (req: ExtendedRequest, res: ExtendedResponse) => {
  let userId = req.session.user ? req.session.user?._id : null;
  const result = await commentService.getCommentByFilter({_id: req.query.commentId}, userId);
  res.json(result);
};

export const updateComment = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const hasAuth = req.session.user?.role.permissions.includes('MANAGE_ALL_COMMENT') || req.session.user?.role.permissions.includes('UPDATE_ALL_COMMENT');
  req.body.author = req.session.user?._id;
  const comment = await commentService.updateComment(new Types.ObjectId(req.query.commentId as string), req.body, hasAuth);
  res.json({
    message: "Updated Comment successfully",
    comment
  });
  await userActivityService.createUserActivity({
    message: `has updated a comment`,
    detail: comment.content,
    target: comment.category._id,
    model: req.body.model,
    user: req.session.user?._id
  });
};

export const deleteComment = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const deleteData = {commentId: req.query.commentId, author: req.session.user?._id, model: req.body.model}
  const hasAuth = req.session.user?.role.permissions.includes('MANAGE_ALL_COMMENT') || req.session.user?.role.permissions.includes('DELETE_ALL_COMMENT');
  const comment = await commentService.deleteComment(deleteData, hasAuth);
  res.json({
    message: "Deleted Comment successfully",
    comment
  });
  await userActivityService.createUserActivity({
    message: `has delete a comment`,
    detail: comment.content,
    target: comment.category._id,
    model: req.body.model,
    user: req.session.user?._id
  });
};