import {Button, Tooltip, Modal, Card, Table, Row, Col, notification, Pagination, Tag, Form, Input} from "antd";
import {DeleteOutlined, EditOutlined, EyeOutlined, PlusCircleOutlined} from "@ant-design/icons";
import React, {useState} from "react";
import {useRouter} from "next/router";
import ManageSystem from "../index";
import {withSessionSsr} from "server/utils/with-session";
import {SessionData} from "server/interfaces/types";
import {auth} from "server/utils/auth";
import {queryRoles} from "server/services/role.service";
import {IRole} from "server/models/interfaces/role.interface";
import {ApiService} from "common/services/ApiService";

const {confirm} = Modal;

const RoleList = (props: any) => {
  const router = useRouter();
  const [form] = Form.useForm();
  const [visible, setVisible] = useState(false);
  const [loading, setLoading] = useState(false);
  const [data, setData] = useState<IRole>(props.initRole);
  const [roles, setRoles] = useState(props.roles || []);
  const [pageInfo, setPageInfo] = useState(props.pageInfo || {});
  const [mode, setMode] = useState("");


  const getRoles = async (value: any) => {
    const res = await ApiService.getRoles({
      page: value,
      limit: 10,
      sortBy: "createdAt"
    });
    setRoles(res.results)
    setPageInfo({
      totalResults: res.totalResults,
      hasNextPage: res?.page < res?.totalPages
    })
  }

  const onOK = async () => {
    const data = form.getFieldsValue();
    if (mode === "add") {
      const res = await ApiService.addRole(data);
      if (res.status) {
        notification.error({
          message: res.data.message
        })
        return setLoading(false)
      }
      setLoading(false)
      setMode("");
      setVisible(false);
      form.resetFields();
      const newList = [...roles, res.role]
      setRoles(newList);
      notification.success({
        message: res.message
      });
    }
  }

  const columns: any = [
    {
      title: "Name",
      align: "center",
      dataIndex: "name",
      key: "name",
    },
    {
      title: "Slug",
      align: "center",
      dataIndex: "slug",
      key: "slug",
    }, {
      title: "Permission",
      dataIndex: "permissions",
      align: "center",
      render: (record: any) => {
        return (
          <div>
            <Tag>{record?.length} permission(s)</Tag>
          </div>
        )
      },
    }, {
      title: "Action",
      dataIndex: "action",
      align: "center",
      key: "action",
      render: (_: any, record: any) => (
        <div className="text-right d-flex justify-content-center">
          <Tooltip title="View">
            <Button type="primary" className="mr-2" icon={<EyeOutlined/>} size="small" onClick={() => {
              setVisible(true);
              setMode("view")
              setData(record);
            }}/>
          </Tooltip>
          <Tooltip title="Update">
            <Button type="default" className="mr-2" icon={<EditOutlined/>} size="small" onClick={async () => {
              await router.push(`/manage-system/manage-roles/edit-role/${record.slug}`)
            }}/>
          </Tooltip>
          <Tooltip title="Delete">
            <Button type="default" danger icon={<DeleteOutlined/>} onClick={() => showDeleteConfirm(record.slug)}
                    size="small"/>
          </Tooltip>
        </div>
      )
    }
  ];

  const showDeleteConfirm = (slug: any) => {
    return (
      confirm({
        title: 'Are you sure delete this role?',
        content: 'This action can not undo, so do you want to delete?',
        okText: 'Yes',
        okType: 'danger',
        cancelText: 'No',
        onOk: async () => {
          const res = await ApiService.deleteRole(slug);
          if (res.status) {
            return notification.error({
              message: res.data.message
            })
          }
          const newRoles = [...roles].filter(role => role._id !== res.role._id)
          setRoles(newRoles)
          notification.success({
            message: res.message
          })
        },
      })
    )
  }

  return (
    <ManageSystem>
      <div>
        <div className="mb-3" style={{
          display: "flex",
          justifyContent: "space-between"
        }}>
          <Button type="primary" icon={<PlusCircleOutlined/>} onClick={() => {
            setMode("add");
            setVisible(true)
          }}>
            Add Role
          </Button>
        </div>
        <Card>
          <div className="table-responsive">
            <Table
              rowKey={(record) => record.slug}
              columns={columns}
              dataSource={roles}
              pagination={false}
              footer={() => {
                return (
                  <Pagination
                    showQuickJumper
                    defaultCurrent={1}
                    total={pageInfo.totalResults}
                    onChange={getRoles}
                  />
                )
              }}
            />
          </div>
          <Modal
            title="Role information"
            visible={visible}
            onCancel={() => {
              setVisible(false)
              setMode("")
            }}
            confirmLoading={loading}
            okButtonProps={{
              style: {
                display: mode === "view" ? "none" : "inline-block"
              },
              form: 'role-form-submit',
              htmlType: 'submit',
            }}
            okText={"Submit"}
          >
            {
              mode === "view"
                ? (
                  <div>
                    <Row>
                      <Col>
                        <h4>Name</h4>
                        <p>{data.name}</p>
                      </Col>
                    </Row>
                    <Row>
                      <Col>
                        <h4>Permissions</h4>
                        <p>
                          {
                            (data.permissions && data.permissions.length > 0)
                              ?
                              (data.permissions ? data.permissions : []).join(", ")
                              :
                              'No permissions'
                          }
                        </p>
                      </Col>
                    </Row>
                  </div>
                ) : (
                  <Form id="role-form-submit" layout="vertical" form={form} onFinish={onOK}>
                    <Form.Item
                      label="Name" name="name"
                      rules={[{required: true, message: 'Please input role name!'}]}
                    >
                      <Input placeholder="Input role name"/>
                    </Form.Item>
                  </Form>
                )
            }
          </Modal>
        </Card>
      </div>
    </ManageSystem>
  )
}

export const getServerSideProps = withSessionSsr(
  async function getServerSideProps({req, res}): Promise<any> {
    try {
      await auth(req.session as SessionData, ["MANAGE_ALL_ROLE"]);
      const roles = await queryRoles({deleted: {$ne: true}}, {page: 1, limit: 10, sortBy: "createdAt"});
      return {
        props: {
          initRole: {} as IRole,
          roles: JSON.parse(JSON.stringify(roles?.results)),
          pageInfo: {
            totalResults: roles?.totalResults,
            hasNextPage: roles?.page < roles?.totalPages
          }
        }
      }
    } catch (err) {
      res.statusCode = err["statusCode"] || 500;
      return {
        redirect: {
          permanent: false,
          destination: (err.statusCode === 401) ? '/auth/login' : '/',
        },
      }
    }
  }
);

export default RoleList
