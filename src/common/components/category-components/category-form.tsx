import {Card, Form, Col, Input, Button} from "antd";
import {PageHeaderAlt} from "../layout-components/PageHeaderAlt";
import Flex from "../shared-components/Flex";

const {TextArea} = Input

const RoleForm = (props: any) => {
  const {mode} = props;
  const [form] = Form.useForm();
  return (
    <div>
      <Form
        form={form}
        layout="vertical"
      >
        <PageHeaderAlt className="border-bottom" navType={undefined}>
          <div className="container">
            <Flex className="py-2" mobileFlex={false} justifyContent="between" alignItems="center">
              <h2>{mode === 'ADD' ? 'Add Category' : 'Edit Category Information'}</h2>
              <div className="mb-3">
                <Button type="primary" htmlType="submit">
                  {mode === 'ADD' ? 'Submit' : 'Save'}
                </Button>
              </div>
            </Flex>
          </div>
        </PageHeaderAlt>

        <div className="container mt-3" style={{display: "flex", justifyContent: "center"}}>
          <Col xs={24} sm={24} md={24} lg={12} xl={12} xxl={12}>
            <Card title="Category Information">
              <Form.Item
                label="Title"
                name="title"
                rules={[{required: true, message: 'Please input category title'}]}
              >
                <Input placeholder="Enter category title..."/>
              </Form.Item>
              <Form.Item
                label="Description"
                name="description"
                rules={[{required: true, message: 'Please input description'}]}
              >
                <TextArea placeholder="Enter description..."/>
              </Form.Item>
            </Card>
          </Col>
        </div>
      </Form>
    </div>
  )
}

export default RoleForm
