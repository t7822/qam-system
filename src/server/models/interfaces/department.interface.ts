import {Document, Model} from 'mongoose';
import {IUser} from "./user.interface";

export interface DepartmentInterface extends Document {
  name: string,
  slug: string,
  manager: IUser["_id"],
  deleted: boolean,
  createdAt: number,
}

// for methods
export interface IDepartment extends DepartmentInterface {
}

// for static methods
export interface DepartmentModel extends Model<IDepartment> {
  paginate(filter: object, options: object): any,
  slugGenerator(name: string): Promise<string>,
}