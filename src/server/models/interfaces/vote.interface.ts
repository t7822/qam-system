import {Document, Model} from 'mongoose';
import {IUser} from "./user.interface";
import {IIdea} from "./idea.interface";

export interface VoteInterface extends Document {
  user: IUser["_id"],
  idea: IIdea["_id"],
  type: string,
  active: boolean,
  activatedAt: number,
}

export interface IVote extends VoteInterface {
  // for methods
}

export interface VoteModel extends Model<IVote> {
  // for static functions
}