import {Form, Card, Input, Button, DatePicker, Select, Row, Col, notification} from 'antd';
import moment from 'moment';
import {PageHeaderAlt} from "../layout-components/PageHeaderAlt";
import Flex from "../shared-components/Flex";
import {useEffect, useState} from "react";
import {ApiService} from "../../services/ApiService";
import {useRouter} from "next/router";

const UserForm = (props: any) => {
  const router = useRouter();
  const {mode, user} = props || null;
  const [form] = Form.useForm();
  const [loading, setLoading] = useState(false)

  useEffect(() => {
    if (user) {
      form.setFieldsValue({
        username: user?.username,
        email: user?.email,
        role: user?.role?.name,
        department: user?.department?._id,
        password: user?.password,
        fullName: user?.fullName,

        //personal information
        contact: user?.contact === "unassigned" ? null : user.contact,
        dob: user?.dob === "unassigned" ? null : moment(parseInt(user.dob)),
        gender: user?.gender === "unassigned" ? null : user?.gender,
        address: user?.address === "unassigned" ? null : user?.address,
      })
    } else {
      form.resetFields()
    }
  }, [form, mode, user])

  const datePickerStyle = {
    width: '100%',
  }

  const onFinish = async (values: any) => {
    setLoading(true)
    let data = {
      //required
      username: values?.username,
      email: values?.email,
      fullName: values?.fullName,
      role: values?.role,
      department: values?.department,

      //personal information
      dob: values?.dob !== undefined ? new Date(values?.dob).getTime() : "unassigned",
      gender: values?.gender ?? "unassigned",
      address: values?.address ?? "unassigned",
      contact: values?.contact ?? "unassigned",
    }
    try {
      if (mode === "ADD") {
        const res = await ApiService.addUser({...data, password: values?.password})
        if (res.status) {
          notification.error({
            message: res.data.message
          })
          return setLoading(false)
        }
        notification.success({
          message: res.message
        })
        setLoading(false)
      } else {
        const res = await ApiService.updateUser(user.username, values?.password === undefined ? data : {
          ...data,
          password: values?.password
        })
        if (res.status) {
          notification.error({
            message: res.data.message
          })
          return setLoading(false)
        }
        notification.success({
          message: res.message
        })
        setLoading(false)
      }
    } catch (err) {
      notification.error({
        message: err
      })
      setLoading(false)

    }
  }

  return (
    <div>
      <Form
        form={form}
        name="dynamic_rule"
        layout="vertical"
        onFinish={onFinish}
      >
        <PageHeaderAlt className="border-bottom" navType={undefined}>
          <div className="container">
            <Flex className="py-2" mobileFlex={false} justifyContent="between" alignItems="center">
              <h2 className="mb-3">{mode === "ADD" ? "Add User" : "Edit User Information"}</h2>
              <div className="mb-3">
                <Button className="mr-2" onClick={async () => {
                  await router.back()
                }}>Cancel</Button>
                <Button type="primary" htmlType="submit" loading={loading}>
                  {mode === "ADD" ? "Submit" : "Save"}
                </Button>
              </div>
            </Flex>
          </div>
        </PageHeaderAlt>
        <div className="container">
          <Row gutter={32} className="mt-3">
            <Col xs={24} sm={24} md={24} lg={12} xl={12} xxl={12}>
              <Card title="Required Information">
                <Form.Item
                  label="Full Name"
                  name="fullName"
                  rules={[{required: true, message: 'Please input your username!'}]}
                >
                  <Input placeholder="Enter full name..."/>
                </Form.Item>
                <Form.Item
                  label="UserName"
                  name="username"
                  rules={[{
                    required: true,
                    message: "Please provide username!"
                  }, ({getFieldValue}: { getFieldValue: any }) => ({
                    //@ts-ignore
                    validator() {
                      if (getFieldValue('username') === "" || getFieldValue('username').length > 5) {
                        return Promise.resolve();
                      }
                      return Promise.reject('Username must contain more than 6 digits');
                    },
                  })]}
                >
                  <Input placeholder="Enter username..."/>
                </Form.Item>
                <Form.Item
                  label="Email"
                  name="email"
                  rules={[{required: true, message: 'Please input your email!', type: "email"}]}
                >
                  <Input placeholder="Enter personal email..."/>
                </Form.Item>
                <Form.Item
                  name="role"
                  label="Role"
                  rules={[{required: true, message: 'Please select role!'}]}
                >
                  <Select placeholder="Please select role">
                    {props.roles.map((role: any, index: number) => {
                      return (
                        <Select.Option style={{textTransform: "capitalize"}} value={role.name}
                                       key={role._id + index}><span
                          style={{textTransform: "capitalize"}}>{role.name}</span></Select.Option>
                      )
                    })}
                  </Select>
                </Form.Item>
                <Form.Item
                  name="department"
                  label="Department"
                  rules={[{required: true, message: 'Please select department!'}]}
                >
                  <Select placeholder="Please select department">
                    {props?.departments.map((department: any, index: number) => {
                      return (
                        <Select.Option style={{textTransform: "capitalize"}} value={department._id}
                                       key={department._id + index}><span
                          style={{textTransform: "capitalize"}}>{department.name}</span></Select.Option>
                      )
                    })}
                  </Select>
                </Form.Item>
                <Form.Item
                  label={mode === "ADD" ? "Password" : "Password (Optional)"}
                  name="password"
                  rules={mode === "ADD" ? [
                    {
                      required: true,
                      message: "Please input password"
                    },
                  ] : []}
                >
                  <Input placeholder="Enter password..."/>
                </Form.Item>
                {mode === "ADD" && (
                  <Form.Item
                    label="Confirm Password"
                    name="confirmPassword"
                    rules={[{
                      required: true,
                      message: 'Please confirm your password!'
                    }, ({getFieldValue}: { getFieldValue: any }) => ({
                      //@ts-ignore
                      validator(rule: any, value: any) {
                        if (!value || getFieldValue('password') === value) {
                          return Promise.resolve();
                        }
                        return Promise.reject('Passwords do not match!');
                      },
                    })]}
                  >
                    <Input placeholder="Enter confirm password..."/>
                  </Form.Item>
                )}
              </Card>
            </Col>
            <Col xs={24} sm={24} md={24} lg={12} xl={12} xxl={12}>
              <Card title="Optional Information">
                <Form.Item
                  label="Date of Birth"
                  name="dob"
                >
                  <DatePicker style={datePickerStyle}/>
                </Form.Item>
                <Form.Item
                  name="gender"
                  label="Gender"
                >
                  <Select placeholder="Select gender...">
                    <Select.Option value="0">Male</Select.Option>
                    <Select.Option value="1">Female</Select.Option>
                    <Select.Option value="2">Other</Select.Option>
                  </Select>
                </Form.Item>
                <Form.Item
                  label="Contact"
                  name="contact"
                >
                  <Input placeholder="Enter phone number..."/>
                </Form.Item>
                <Form.Item
                  label="Address"
                  name="address"
                >
                  <Input placeholder="Enter address..."/>
                </Form.Item>
              </Card>
            </Col>
          </Row>
        </div>
      </Form>
    </div>
  )
}

export default UserForm
