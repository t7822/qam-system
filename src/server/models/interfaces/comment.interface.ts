import {Document, Model} from 'mongoose';
import {IUser} from "./user.interface";
import {IIdea} from "./idea.interface";

export interface CommentInterface extends Document {
  author: IUser["_id"],
  content: string,
  replyFor: this["_id"],
  level: number,
  tag: IUser["_id"],
  category: IIdea["_id"],
  privacy: string,
  editedAt: string,
  editedTimes: number,
  deleted: boolean,
}

export interface IComment extends CommentInterface {
  // for methods
}

export interface CommentModel extends Model<IComment> {
  // for static functions
}