import Joi from 'joi';
import {ObjectId} from "server/validations/custom.validations";

export const createTag: {body: object} = {
  body: Joi.object().keys({
    title: Joi.string().required(),
  }),
};

export const getTags: {query: object} = {
  query: Joi.object().keys({
    title: Joi.string(),
    slug: Joi.string(),
    sortBy: Joi.string(),
    limit: Joi.number().integer(),
    page: Joi.number().integer(),
  }),
};

export const getTag: {params: object} = {
  params: Joi.object().keys({
    tagId: Joi.string().custom(ObjectId),
  }),
};

export const deleteTag: {params: object} = {
  params: Joi.object().keys({
    tagId: Joi.string().custom(ObjectId),
  }),
};