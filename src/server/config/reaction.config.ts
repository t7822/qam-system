export default {
	type: [
		"%F0%9F%98%82", //haha
		"%F0%9F%98%9E", //sad
		"%F0%9F%8E%89", //congrats
		"%F0%9F%A4%94", //thinking
		"%F0%9F%A4%AA", //zany
	],
	models: [
		"Blog",
		"Comment",
		"Idea",
		"ProfilePost"
	],
	default: "%F0%9F%98%82"
}