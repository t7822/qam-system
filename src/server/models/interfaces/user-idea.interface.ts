import {Document, Model} from 'mongoose';
import {IIdea} from "./idea.interface";
import {IUser} from "./user.interface";

export interface UserIdeaInterface extends Document {
  idea: IIdea["_id"],
  user: IUser["_id"],
}

// for methods
export interface IUserIdea extends UserIdeaInterface {

}

// for static methods
export interface UserIdeaModel extends Model<IUserIdea> {
  paginate(filter: object, options: object): any
}