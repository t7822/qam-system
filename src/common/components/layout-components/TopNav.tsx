import React from 'react'
import {useSelector} from 'react-redux';
import {NAV_TYPE_TOP} from 'common/constants/ThemeConstant';
import utils from 'common/utils'
import MenuContent from 'common/components/layout-components/MenuContent'
import {RootState} from "../../redux/store";

interface TopNavProps {
  localization?: boolean,
  routeInfo?: any
}

export const TopNav = ({localization = true} : TopNavProps) => {
  const {topNavColor} = useSelector((state: RootState) => state.theme);
  return (
    <div className={`top-nav ${utils.getColorContrast(topNavColor)}`} style={{backgroundColor: topNavColor}}>
      <div className="top-nav-wrapper">
        <MenuContent
          type={NAV_TYPE_TOP}
          localization={localization}
        />
      </div>
    </div>
  )
}

export default TopNav;
