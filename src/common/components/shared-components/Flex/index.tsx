import React from 'react';

interface IFlexProps {
  children?: any,
  className?: string,
  alignItems?: string,
  flexDirection?: string,
  justifyContent?: string,
  mobileFlex?: boolean
}

const Flex = (props: IFlexProps) => {
  let {children, className, alignItems, justifyContent, mobileFlex, flexDirection} = props
  if (!flexDirection) flexDirection = "row";
  if (typeof mobileFlex !== "boolean") mobileFlex = true;
  if (typeof className !== "string") className = "";
  const getFlexResponsive = () => mobileFlex ? 'd-flex' : 'd-md-flex'
  return (
    <div
      className={`${getFlexResponsive()} ${className} ${flexDirection ? ('flex-' + flexDirection) : ''} ${alignItems ? ('align-items-' + alignItems) : ''} ${justifyContent ? ('justify-content-' + justifyContent) : ''}`}>
      {children}
    </div>
  )
}

export default Flex;
