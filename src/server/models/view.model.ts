import {Schema, model, models} from 'mongoose';
import {paginate, toJSON} from "server/models/plugins";
import {IView, ViewModel} from "./interfaces/view.interface";

const viewSchema = new Schema<IView>({
  user: {
    type: Schema.Types.ObjectId,
    ref: "User",
    default: null,
  },
  idea: {
    type: Schema.Types.ObjectId,
    ref: "Idea",
    required: true
  },
  viewedAt: {
    type: String,
    required: true,
  }
}, {
  collection: 'views',
  timestamps: true
});

viewSchema.plugin(paginate);
viewSchema.plugin(toJSON);

/**
 * @typedef View
 */
const View = models.View || model<IView, ViewModel>(
  "View",
  viewSchema
);

export default View;